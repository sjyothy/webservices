package com.emitac.Service;

import com.avanza.pims.entity.Locations;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ServiceTest {
    
    
    public static void main(String[] args) throws Exception {
       try
       {
        String json = "{\"createdBy\":\"pims_admin\",\"socialResearchId\":\"-1\",\"inheritanceFileId\":\"79\",\"beneficiaryId\":\"\",\"title\":\"Test from code\",\"longitude\":\"50.0004\",\"latitude\":\"25.0004\" }";
        Gson jsonObj = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
        Locations input = jsonObj.fromJson(json,Locations.class);
        System.out.println("Longitude:"+input.getLongitude());
       }
       catch(Exception e)
       {
           
           System.out.println("Exception occured :"+e.getStackTrace());
       }
        
    }

}
