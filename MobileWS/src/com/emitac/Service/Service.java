
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.security.AuthenticationStatus;
import com.avanza.core.security.db.UserDbImpl;
import com.avanza.pims.Utils.SocialResearchSearchCriteria;
import com.avanza.pims.constant.Constant;
import com.avanza.pims.dao.UtilityManager;
import com.avanza.pims.entity.AmafContactDetails;
import com.avanza.pims.entity.AppointmentDetails;
import com.avanza.pims.entity.AssetDetailsForMobile;
import com.avanza.pims.entity.BeneficiaryDetailsForMobile;
import com.avanza.pims.entity.ChatMessages;
import com.avanza.pims.entity.Contract;
import com.avanza.pims.entity.DisbursementDetails;
import com.avanza.pims.entity.DisbursementReason;
import com.avanza.pims.entity.DomainData;
import com.avanza.pims.entity.EndProgObjective;
import com.avanza.pims.entity.EntityManager;
import com.avanza.pims.entity.FileDetailsByPersonId;
import com.avanza.pims.entity.InheritanceFile;
import com.avanza.pims.entity.ItemType;
import com.avanza.pims.entity.Locations;
import com.avanza.pims.entity.MemsNolTypes;
import com.avanza.pims.entity.MobileServiceCategory;
import com.avanza.pims.entity.MobileServiceProcedure;
import com.avanza.pims.entity.MobileServiceProcedureType;
import com.avanza.pims.entity.News;
import com.avanza.pims.entity.Person;
import com.avanza.pims.entity.Request;
import com.avanza.pims.entity.RequestType;
import com.avanza.pims.entity.ResearchRecommendation;
import com.avanza.pims.entity.SocialResearch;
import com.avanza.pims.entity.UserHappinessIndex;
import com.avanza.pims.ws.vo.UserFeedbackView;
import com.avanza.pims.ws.RegisterExternalUserService;
import com.avanza.pims.ws.finance.EPayTransactionService;
import com.avanza.pims.ws.mems.DisbursementService;
import com.avanza.pims.ws.mems.InheritanceFileService;
import com.avanza.pims.ws.mems.MinorTransformUtil;
import com.avanza.pims.ws.mems.SocialResearchService;
import com.avanza.pims.ws.mems.endowment.EndowmentProgramService;
import com.avanza.pims.ws.property.EvacuationService;
import com.avanza.pims.ws.property.PropertyService;
import com.avanza.pims.ws.request.RequestService;
import com.avanza.pims.ws.services.AppointmentService;
import com.avanza.pims.ws.services.ChatService;
import com.avanza.pims.ws.services.LocationsService;
import com.avanza.pims.ws.services.UserFeedbackService;
import com.avanza.pims.ws.utility.CallWebservice;
import com.avanza.pims.ws.utility.UtilityService;
import com.avanza.pims.ws.vo.AppointmentInputParameter;
import com.avanza.pims.ws.vo.AspectsCompareInputParameters;
import com.avanza.pims.ws.vo.CancelRequestInputParameters;
import com.avanza.pims.ws.vo.ContractSearchCriteria;
import com.avanza.pims.ws.vo.DocumentView;
import com.avanza.pims.ws.vo.EPayTokenDetails;
import com.avanza.pims.ws.vo.MemsNolTypeView;
import com.avanza.pims.ws.vo.NolInputParameter;
import com.avanza.pims.ws.vo.NormalDisbursementInputParameter;
import com.avanza.pims.ws.vo.PaginationVO;
import com.avanza.pims.ws.vo.PersistBeneficiaryInputParameters;
import com.avanza.pims.ws.vo.PersistChatInputParameters;
import com.avanza.pims.ws.vo.PersistDonationsInputParameters;
import com.avanza.pims.ws.vo.PersistMinorMaintenanceInputParameters;
import com.avanza.pims.ws.vo.PersistPropertyMaintenanceInputParameters;
import com.avanza.pims.ws.vo.PersistRecommendationsInputParameters;
import com.avanza.pims.ws.vo.PersistSocialResearchInputParameters;
import com.avanza.pims.ws.vo.PersonView;
import com.avanza.pims.ws.vo.RegisterUser;
import com.avanza.pims.ws.vo.RelationshipView;
import com.avanza.pims.ws.vo.ReplaceChequeInputParameters;

import com.avanza.pims.ws.vo.RequestTypeView;
import com.avanza.pims.ws.vo.RequestView;
import com.avanza.pims.ws.vo.TransferRequestInputParameters;
import com.avanza.pims.ws.vo.UserDetails;

import com.avanza.pims.ws.vo.UserView;
import com.avanza.pims.ws.vo.mems.DisbursementDetailsView;

import com.avanza.pims.ws.vo.mems.EndowmentProgramView;
import com.avanza.pims.ws.vo.mems.MinorBalanceSummaryView;
import com.avanza.pims.ws.vo.mems.SocialResearchMobileView;

import com.emitac.Utils.Cryptography;
import com.emitac.Utils.ResourceWrapper;

import com.google.gson.Gson;

import com.google.gson.GsonBuilder;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import java.io.InputStream;

import java.text.DateFormat;

import java.text.MessageFormat;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.beanutils.BeanUtils;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;


//import com.emitac.Utils;
//import com.avanza.core.security;


@Path("/")
public class Service {

    public static void main(String[] arr) {
        try
        {
        }
        catch(Exception e){
            logger.LogException("Error:",e);
        }
    }
   private static String message = "{\"message\":" ;
    private static String msgCode = " {\"msgCode\":";
    private static String msgEn = " \"msgEn\":\"";
    private static String msgAr = " \"msgAr\":\"";
    private static String errorCode = " {\"errorCode\":";
    private static String errorMsgEn = " \"errorMsgEn\":\"";
    private static String errorMsgAr = " \"errorMsgAr\":\"";
    private static final String allowedToMakeRequestSuccess= "{\"allowed\": \"true\"}";
    private static String downloadURL ="https://services.amaf.ae/AMAFMobileWSAppClientTwo/downloadFile?documentId=";    
//    private static String downloadURL ="https://services.amaf.ae/AMAFMobileWSAppClientOne/downloadFile?documentId=";    
//    private static String downloadURL ="https://services.amaf.ae/AMAFMobileWSAppClientTest/downloadFile?documentId=";    

    private static UtilityService utilityServiceObj = new UtilityService();
    private static PropertyService propertyServiceObj = new PropertyService();
    private static RequestService requestServiceObj = new RequestService();
    private static EvacuationService evacuationServiceObj = new EvacuationService();
    final static com.avanza.core.util.Logger logger = com.avanza.core.util.Logger.getLogger(Service.class);
    // This main function is added for testing purpose. It will be removed later
     
    
    @GET
    @Path("/authenticateFromEServices")
    @Produces("application/json;charset=utf-8")
    public String authenticateFromEServices( @QueryParam("inputData") String inputData) throws Exception {
        
        String json="";
        StringBuilder errorJson=null;
        try
        {
            
            //<User Id>;<Password>
            String[] arr = inputData.split(";");
            String loginId= arr[0];
            String password =arr[1];
            logger.logDebug("|authenticateFromEServices|loginId:%s",loginId);
        //            logger.logDebug("password:"+password);
            if( loginId == null || loginId.trim().length() <=0 )
            {
                errorJson =  buildErrorJson(errorJson, false,String.valueOf(  Constant.MobileWebServiceErrorCodes.LOGIN_ID_NOT_SPECIFIED),String.valueOf(  Constant.MobileWebServiceErrorCodes.LOGIN_ID_NOT_SPECIFIED_MSG ));
                return errorJson.toString();
            }
            else if( password == null || password.trim().length()<= 0 )
            {
                errorJson =  buildErrorJson(errorJson, false,String.valueOf(  Constant.MobileWebServiceErrorCodes.PASSWORD_REQUIRED) ,String.valueOf(  Constant.MobileWebServiceErrorCodes.PASSWORD_REQUIRED_MSG)
                                            );
                return errorJson.toString();
            }
            UserDetails userDetail = UtilityService.getUserDetailsFromLoginId(loginId);
        
            if( userDetail == null )
            {
                errorJson =  buildErrorJson(errorJson, false,String.valueOf(  Constant.MobileWebServiceErrorCodes.USER_NOT_PRESENT),String.valueOf(  Constant.MobileWebServiceErrorCodes.USER_NOT_PRESENT_MSG) );
                return errorJson.toString();
            }
            if( userDetail.getStatusId()!= null && userDetail.getStatusId().equals("0") )
            {
                errorJson =  buildErrorJson(errorJson, false,String.valueOf(  Constant.MobileWebServiceErrorCodes.USER_IS_NOT_ACTIVATED),String.valueOf(  Constant.MobileWebServiceErrorCodes.USER_IS_NOT_ACTIVATED_MSG) );
                return errorJson.toString();
                       
            }
            else if( userDetail.getIsSystem().equals("1")  )
            {
                 json = authenticateSystemUser(json, errorJson, password, userDetail,false);
            }
            else
            {
                json = authenticateLDAPUser(json, errorJson, userDetail, password);   
            }
            if( errorJson != null  ) 
            {
                    json = errorJson.toString();
            }
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
        //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                 ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        
        return json;
    }
    
    @GET
    @Path("/authenticateFromPortal")
    @Produces("application/json;charset=utf-8")
    public String authenticateFromPortal( @QueryParam("inputData") String inputData) throws Exception {
        
        String json="";
        StringBuilder errorJson=null;
        try
        {
            Cryptography crypto = new com.emitac.Utils.Cryptography();
            inputData = crypto.decrypt(inputData);
            //<User Id>;<Password>
            String[] arr = inputData.split(";");
            String loginId= arr[0];
            String password =arr[1];
            logger.logDebug("loginId:"+loginId);
        //            logger.logDebug("password:"+password);
            if( loginId == null || loginId.trim().length() <=0 )
            {
                if (errorJson ==null ) errorJson=new StringBuilder(message);
                errorJson=errorJson.append( msgCode ).append( Constant.MobileWebServiceErrorCodes.LOGIN_ID_NOT_SPECIFIED ).append(",");
                errorJson=errorJson.append( msgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.LOGIN_ID_NOT_SPECIFIED_MSG,"en")
                                                                ).
                                                                append("\",");
                errorJson=errorJson.append( msgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.LOGIN_ID_NOT_SPECIFIED_MSG,"ar")
                                                               ).
                                                               append("\"");
                
                errorJson = errorJson.append("}}");
                return errorJson.toString();
            }
            else if( password == null || password.trim().length()<= 0 )
            {
                if (errorJson ==null ) errorJson=new StringBuilder(message);
                errorJson=errorJson.append( msgCode ).append( Constant.MobileWebServiceErrorCodes.PASSWORD_REQUIRED ).append(",");
                errorJson=errorJson.append( msgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.PASSWORD_REQUIRED_MSG,"en")
                                                                ).
                                                                append("\",");
                errorJson=errorJson.append( msgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.PASSWORD_REQUIRED_MSG,"ar")
                                                               ).
                                                               append("\"");
                
                errorJson = errorJson.append("}}");
                return errorJson.toString();
            }
           
            UserDetails userDetail = UtilityService.getUserDetailsFromLoginId(loginId);
        
            if( userDetail == null )
            {
                if (errorJson ==null ) errorJson=new StringBuilder("{\"message\": {");
                errorJson=errorJson.append( msgCode ).append( Constant.MobileWebServiceErrorCodes.USER_NOT_PRESENT ).append(",");
                errorJson=errorJson.append( msgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.USER_NOT_PRESENT_MSG,"en")
                                                                ).
                                                                append("\",");
                errorJson=errorJson.append( msgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.USER_NOT_PRESENT_MSG,"ar")
                                                               ).
                                                               append("\"");
                
                errorJson = errorJson.append("}}");
                return errorJson.toString();
            }
        
            else if( userDetail.getStatusId()!= null && userDetail.getStatusId().equals("0") )
            {
                  if (errorJson ==null ) errorJson=new StringBuilder(message);
                   errorJson=errorJson.append( msgCode ).append( Constant.MobileWebServiceErrorCodes.USER_IS_NOT_ACTIVATED ).append(",");
                   errorJson=errorJson.append( msgEn ).append(
                                                                    ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.USER_IS_NOT_ACTIVATED_MSG,"en")
                                                                   ).
                                                                   append("\",");
                   errorJson=errorJson.append( msgAr ).append(
                                                                    ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.USER_IS_NOT_ACTIVATED_MSG,"ar")
                                                                  ).
                                                                  append("\"");
                   
                errorJson = errorJson.append("}}");
            }
            else if( userDetail.getIsSystem().equals("1")  )
            {
                 json = authenticateSystemUser(json, errorJson, password, userDetail, true);
            }
            
            if( errorJson != null  ) 
            {
                    json = errorJson.toString();
            }
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"message\": {\"msgCode\":\"000\",\"msgEn\":\"An error has occured please try again later\""+
                                                             ",\"msgAr\":\"An error has occured please try again later\"}}";
        }
        
        return json;
    }
    @GET
    @Path("/authenticate")
    @Produces("application/json;charset=utf-8")
    public String authenticate(  @QueryParam("inputData") String credentials )
    {
        String json="";
        StringBuilder errorJson=null;
        try
        {
            
            Cryptography crypto = new com.emitac.Utils.Cryptography();
            String inputData = crypto.decrypt(credentials);
            logger.logDebug("inputData:"+inputData);
            //<User Id>;<Password>
            String[] arr = inputData.split(";");
            String loginId= arr[0];
            String password =arr[1];
            logger.logDebug("loginId:"+loginId);
//            logger.logDebug("password:"+password);
            if( loginId == null || loginId.trim().length() <=0 )
            {
                errorJson =  buildErrorJson(errorJson, false,String.valueOf(  Constant.MobileWebServiceErrorCodes.LOGIN_ID_NOT_SPECIFIED) ,Constant.MobileWebServiceErrorCodes.LOGIN_ID_NOT_SPECIFIED_MSG);
               return errorJson.toString();
            }
            else if( password == null || password.trim().length()<= 0 )
            {
                errorJson = buildErrorJson(errorJson, false,String.valueOf(  Constant.MobileWebServiceErrorCodes.PASSWORD_REQUIRED) ,Constant.MobileWebServiceErrorCodes.PASSWORD_REQUIRED_MSG);
                    
                return errorJson.toString();
            }
            
            
//            UserDbImpl user  = (UserDbImpl) com.avanza.core.security.SecurityManager.getUser(loginId);
            UserDetails userDetail = UtilityService.getUserDetailsFromLoginId(loginId);
//            if( user == null )
            if( userDetail == null )
            {
                errorJson =  buildErrorJson(errorJson, false,String.valueOf(  Constant.MobileWebServiceErrorCodes.USER_NOT_PRESENT),Constant.MobileWebServiceErrorCodes.USER_NOT_PRESENT_MSG );
                return errorJson.toString();
                
            }
//            if( user.getStatusId()!= null && user.getStatusId().equals("0") )
            if( userDetail.getStatusId()!= null && userDetail.getStatusId().equals("0") )
            {
                errorJson =  buildErrorJson(errorJson, false,String.valueOf(  Constant.MobileWebServiceErrorCodes.USER_IS_NOT_ACTIVATED),Constant.MobileWebServiceErrorCodes.USER_IS_NOT_ACTIVATED_MSG );
                return errorJson.toString();
                       
            }
//            else if( user.isSystemUser()  )
            else if( userDetail.getIsSystem().equals("1")  )
            {
//                    json = authenticateSystemUser(json, errorJson, password, user);
                 json = authenticateSystemUser(json, errorJson, password, userDetail,false);
            }
            else
            {
//                    json = authenticateLDAPUser(json, errorJson, user, password);   
                json = authenticateLDAPUser(json, errorJson, userDetail, password);   
            }
            if( errorJson != null  ) 
            {
                    json = errorJson.toString();
            }
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
//            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                 ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        
        return json;
    }

//    private String authenticateLDAPUser(String json, StringBuilder errorJson, UserDbImpl user, String password) throws Exception
    private String authenticateLDAPUser(String json, StringBuilder errorJson, UserDetails userDetail, String password) throws Exception
    {
        logger.logInfo("authenticateLDAPUser|loginId:%s|password:%s",userDetail.getLoginId(),password);
        AuthenticationStatus au = com.avanza.core.security.SecurityManager.authenticateUser(
                                                                                             userDetail.getLoginId(),
                                                                                             password
                                                                                             
                                                                                           );
        logger.logInfo("authenticateLDAPUser|loginId:%s|authenticationStatus:%s",userDetail.getLoginId(),au.getAuthenStatus().toString());                
        if (au.getAuthenStatus().toString().equalsIgnoreCase("success"))
        {   
            
//            String returnJson = getPersonFromLoginId(user.getLoginId());
            String returnJson = getPersonFromLoginId(userDetail.getLoginId());
            if( returnJson!=null && returnJson.length()>0)
            {
                return returnJson;
            }
            else
            {
//            return getLoginJSON(
//                                    user.getLoginId(), 
//                                    -1l, 
//                                    user.getFullName(), 
//                                    user.getSecondaryFullName()!= null && user.getSecondaryFullName().length()>0 ?user.getSecondaryFullName():user.getFullName()
//            
//                              );
                            return getLoginJSON(
                                    userDetail.getLoginId(), 
                                    -1l, 
                                    userDetail.getFullName(), 
                                    userDetail.getFullNameSec()!= null && userDetail.getFullNameSec().length()>0 ?userDetail.getFullNameSec():userDetail.getFullName()
            
                              );
            }
        }
        else 
        {
            if (errorJson ==null ) errorJson=new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.USER_NOT_AUTHENTICATED ).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.USER_NOT_AUTHENTICATED_MSG,"en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.USER_NOT_AUTHENTICATED_MSG,"ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("}]}");
            return errorJson.toString(); 
        }
        
    }

//    private String authenticateSystemUser(String json, StringBuilder errorJson, String password, UserDbImpl user)throws Exception
    private String authenticateSystemUser(String json, StringBuilder errorJson, String password, UserDetails userDetail,boolean fromPortal)throws Exception
    {
        logger.logInfo("authenticateSystemUser|loginId:%s",userDetail.getLoginId());
//        String userPassword = user.getCryptographer().decrypt(user.getPassword());
        String userPassword = UserDbImpl.getCryptographer().decrypt(userDetail.getPassword());
        
//        logger.logDebug("User Password in db:", user.getPassword());
//        logger.logDebug("User Password in db:", userDetail.getPassword());
        //logger.logDebug("User Password in db(Decrypted):", userPassword);
        if( password.compareTo( userPassword )== 0)
        {
            if(!fromPortal)
            {
                String returnJson = getPersonFromLoginId(userDetail.getLoginId());
                if( returnJson!=null && returnJson.length()>0)
                {
                    return returnJson;
                }
                else
                {
                    return getLoginJSON(
                                            userDetail.getLoginId(), 
                                            -1l, 
                                            userDetail.getFullName(), 
                                            userDetail.getFullNameSec()!= null && userDetail.getFullNameSec().length()>0 ?userDetail.getFullNameSec():userDetail.getFullName()
                                      );
                }
            }
            //If from portal than message structure will be different
            else
            {
                json = message+
                       msgCode+"\"0\","+
                       msgEn+ResourceWrapper.getInstance().getProperty("msg.authenticate.success","en")+"\","+
                       msgAr+ResourceWrapper.getInstance().getProperty("msg.authenticate.success","ar")+"\"},"+
                       UtilityService.getPortalUserDetails(
                                                            "en",
                                                            userDetail.getLoginId()
                                                          ) +
                        "}";
                logger.logDebug("json:%s",json);
                
                return json;       
            }
        }
        else
        {
            if(!fromPortal)
            {
               if (errorJson ==null ) errorJson=new StringBuilder("{\"errors\": [");
               errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.PASSWORD_INCORRECT ).append(",");
               errorJson=errorJson.append( errorMsgEn ).append(
                                                                ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.PASSWORD_INCORRECT_MSG,"en")
                                                               ).
                                                               append("\",");
               errorJson=errorJson.append( errorMsgAr ).append(
                                                                ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.PASSWORD_INCORRECT_MSG,"ar")
                                                              ).
                                                              append("\"");
               errorJson = errorJson.append("}]}");
            }
            //If from portal than message structure will be different
            else
            {
                if (errorJson ==null ) errorJson=new StringBuilder(message);
                errorJson=errorJson.append( msgCode ).append( Constant.MobileWebServiceErrorCodes.PASSWORD_INCORRECT ).append(",");
                errorJson=errorJson.append( msgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.PASSWORD_INCORRECT_MSG,"en")
                                                                ).
                                                                append("\",");
                errorJson=errorJson.append( msgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.PASSWORD_INCORRECT_MSG,"ar")
                                                               ).
                                                               append("\"");
                errorJson = errorJson.append("}}");
    
            }
           return errorJson.toString();
        }
        
    }

    private String getPersonFromLoginId(String  loginId ) throws Exception 
    {
//        PropertyService ps = new PropertyService();
//        PersonView person  = ps.getLoginPersonInformation( loginId );
        PersonView person  = UtilityService.getPersonInformation( loginId,"-1" );
        if (person == null || person.getPersonId() == null)
            return null;
        return getLoginJSON(
                            loginId, 
                            person.getPersonId(), 
                            person.getFullNameEn()!=null && person.getFullNameEn().length()>0? person.getFullNameEn():person.getPersonFullName() ,
                            person.getPersonFullName() 
                          );
        
    }

    private String getLoginJSON(String loginId, Long personId, String nameEn, String nameAr) throws Exception{
//        boolean isResearcher = UtilityService.isResearcher(loginId);
        String roles = UtilityService.getSmartAppRoleForLoggedInUser(loginId);
        StringBuilder  successJSON = new StringBuilder("{").
                                                append("\"loginId\":\"").append( loginId ).append("\",").
                                                append("\"personId\":").append( personId ).append(",").
                                                append("\"nameEn\":\"").append( nameEn ).append("\",").
                                                append("\"nameAr\":\"").append( nameAr ).append("\",").
                                                append(roles).
//                                            append("\"isResearcher\":\"").append( (isResearcher?"1":"0") ).append("\"").
                                                append("}")
                                                ;
        return successJSON.toString();
    }


    private String hasNewDetailsInputParamErrors(
                                                    Integer newsId
                                                )
    {
        StringBuilder errorJson= new StringBuilder("{\"errors\": [");
        boolean hasError = false;
        if( newsId == null )
        {
            hasError = true;
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.NEWS_ID_NOT_SPECIFIED).append("}");
        }
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
    }
    @GET
    @Path("/newsDetails")    
    @Produces("application/json;charset=utf-8")
    
    public String getNewsDetails(@QueryParam("newsId")Integer newsId)
    {
        String json="";
        try
        {
            String errorJson = hasNewDetailsInputParamErrors(newsId);
            if( errorJson.length() >0  )
            {
              return errorJson;    
            }
            
            Session session = (Session) EntityManager.getBroker().getSession();
            List<News> newsList = null;
            Criteria newsCrit =  session.createCriteria(News.class);
            
            newsCrit.add(Restrictions.eq("newsId", Long.valueOf( newsId ) ) );
            //News news = EntityManager.getBroker().findById(News.class,Long.valueOf( newsId  ));
            newsList= newsCrit.list();
            News news = newsList.get(0);
            news.getBodyEn();
            news.getBodyAr();
            List<DocumentView> newsImage = searchDocument(null, news.getNewsId().toString(), "newsImage");
            if( newsImage != null && newsImage.size() > 0 )
            {
              DocumentView image = newsImage.get(0);
              news.setImageURL( downloadURL+image.getDocumentId() );
            }
            else {
                news.setImageURL( "" );
                
            }
            List<DocumentView> newsThumbNail = searchDocument(null, news.getNewsId().toString(), "newsThumbNail");
            if( newsThumbNail != null && newsThumbNail.size() > 0 )
            {
              DocumentView thumbNail = newsThumbNail.get(0);
              news.setThumbNailURL( downloadURL+thumbNail.getDocumentId() );
              
            }
            else {
                news.setThumbNailURL( "" );
                
            }
            
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            
            logger.logInfo("getNewsDetails|newsid:%s|ThumbnailLink:%s|ImageLink:%s|Body En Length:%s",news.getNewsId(),news.getThumbNailURL(),news.getImageURL(), news.getBodyEn()!= null ?news.getBodyEn().length():0);
            
            json = json + jsonObj.toJson(news);
            json = json.replace("\u003d", "=");
            session.evict(newsList);
            
        }
        catch(Exception e)
        {
            
            logger.LogException("Error occured", e);
//            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                 ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
                ApplicationContext.getContext().getTxnContext().release();
        }
        return json;
    }
    
    private String hasNewListInputParamErrors(
                                                String recordsPerPage,
                                                String currentPage,
                                                String orderBy, 
                                                String ascending 
                                             )throws Exception
    {
        StringBuilder errorJson= new StringBuilder("{\"errors\": [");
        boolean hasError = false;
//        StringBuilder pagingErrorJson = hasPagingRelatedErrors(recordsPerPage, currentPage);
        if( orderBy != null && orderBy.trim().length() > 0 )
        {
           if( !orderBy.equals("newsDate") &&  !orderBy.equals("headline") )    
           {
            
                
                if( errorJson == null )errorJson= new StringBuilder("");
                errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.INCORRECT_ORDER_BY).append(",");
                errorJson=errorJson.append( errorMsgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")
                                                                ).
                                                                append("\",");
                errorJson=errorJson.append( errorMsgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")
                                                               ).
                                                               append("\"");
                errorJson = errorJson.append("},");

                hasError = true;
            }
        }
//        if( pagingErrorJson != null && pagingErrorJson.length() > 0  )
//        {
//            errorJson.append( pagingErrorJson ) ;
//            hasError = true;
//        }
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
    }
    @GET
    @Path("/newsList")    
    @Produces("application/json;charset=utf-8")
    public String getNewsList(
                                @QueryParam("recordsPerPage")String recordsPerPage,
                                @QueryParam("currentPage")String currentPage,
                                @QueryParam("orderBy")String orderBy, 
                                @QueryParam("ascending")String ascending 
                              )
    {
        String json="";
        try
        {
            String errorJson = hasNewListInputParamErrors(recordsPerPage, currentPage, orderBy, ascending);
            if( errorJson.length() > 0 )                                      
            {
                return errorJson;
            }
            Map<Object,Object> criteriaMap = new HashMap<Object,Object>();
            Session session = (Session) EntityManager.getBroker().getSession();
            List<News> newsList = UtilityService.getNewsList(
                                                              criteriaMap,
                                                              Integer.valueOf(recordsPerPage)  ,
                                                              Integer.valueOf(currentPage)  ,
                                                              orderBy,
                                                              (ascending != null && ascending.equals("1")) ? true:false
                                      
                                                            );
            json="{\"news\": [";
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            boolean hasNews = false;
            for(News news : newsList)
            {
                News obj  = (News)BeanUtils.cloneBean(news);
                obj.setBodyAr("");
                obj.setBodyEn("");
                List<DocumentView> newsThumbNail = searchDocument(null, obj.getNewsId().toString(), "newsThumbNail");
                if( newsThumbNail != null && newsThumbNail.size() > 0 )
                {
                  DocumentView thumbNail = newsThumbNail.get(0);
                  obj.setThumbNailURL( downloadURL+thumbNail.getDocumentId() );
                }
                else 
                {
                    obj.setThumbNailURL( "" );
                }
                //IN IOS if empty, then its crashing,Thus copying the thumbnail url to image url.
                obj.setImageURL( obj.getThumbNailURL() );
                json = json + jsonObj.toJson(obj)+",";
                hasNews = true;
            }
            if( hasNews  )
            {
                json = json.substring(0,json.length()-1)+"]}";
             
            }
            else  
            {
                json = json +"]} ";    
            }
            json = json.replace("\u003d", "=");
            logger.logInfo(json);
            session.evict(newsList);
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
//            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }
    
    private StringBuilder hasPagingRelatedErrors(                       
                                                String recordsPerPage,
                                                String currentPage
                                        )throws Exception
    {
        StringBuilder errorJson= null;
        if( recordsPerPage == null || recordsPerPage.trim().length() <= 0  )
        {
            
            if( errorJson == null )errorJson= new StringBuilder("");
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.REC_PER_PAGE_NOT_SPECIFIED).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")
                                                           ).
                                                           append("\"");
            errorJson = errorJson.append("},");

        }
        else if( recordsPerPage != null || recordsPerPage.trim().length() > 0  )
        {
            
            try { Integer.parseInt(recordsPerPage);}
            catch(Exception e)
            {
            
                if( errorJson == null )errorJson= new StringBuilder("");
                errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.REC_PER_PAGE_NOT_NUMBER).append(",");
                errorJson=errorJson.append( errorMsgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")
                                                                ).
                                                                append("\",");
                errorJson=errorJson.append( errorMsgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")
                                                               ).
                                                               append("\"");
                errorJson = errorJson.append("},");
            }
        
        }
        
        if( currentPage == null || currentPage.trim().length() <= 0  )
        {
            
            if( errorJson == null )errorJson= new StringBuilder("");
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.CURR_PAGE_NOT_SPECIFIED).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")
                                                           ).
                                                           append("\"");
            errorJson = errorJson.append("},");
            
        }
        else if( currentPage != null || currentPage.trim().length() > 0  )
        {
            
            try { Integer.parseInt(currentPage);}
            catch(Exception e)
            {
            
                if( errorJson == null )errorJson= new StringBuilder("");
                errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.CURR_PAGE_NOT_NUMBER).append(",");
                errorJson=errorJson.append( errorMsgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")
                                                                ).
                                                                append("\",");
                errorJson=errorJson.append( errorMsgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")
                                                               ).
                                                               append("\"");
                errorJson = errorJson.append("},");
                    
            }
        
        }
        
        
        return errorJson;
    }
    private String hasInheritanceFileDetailByLoginIdInputParamErrors(
                                                                        String loginId,
                                                                        String recordsPerPage,
                                                                        String currentPage,
                                                                        String orderBy, 
                                                                        String ascending 
                                                                     )throws Exception
    {
        StringBuilder errorJson= new StringBuilder("{\"errors\": [");
        boolean hasError = false;
        if( loginId == null || loginId.trim().length() <= 0  )
        {
         //   errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.INHERITANCE_FILE_LIST_LOGIN_ID_NOT_SPECIFIED).append("},");
         
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.INHERITANCE_FILE_LIST_LOGIN_ID_NOT_SPECIFIED).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
//        StringBuilder pagingErrorJson = hasPagingRelatedErrors(recordsPerPage, currentPage);
        if( orderBy != null && orderBy.trim().length() > 0 )
        {
           if( !orderBy.equals("fileNumber") &&  !orderBy.equals("fileOwner") )    
           {
            
//                errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.INCORRECT_ORDER_BY).append("},");
                errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.INCORRECT_ORDER_BY).append(",");
                errorJson=errorJson.append( errorMsgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")
                                                                ).
                                                                append("\",");
                errorJson=errorJson.append( errorMsgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")
                                                               ).
                                                               append("\"");
                errorJson = errorJson.append("},");    
                hasError = true;
            }
        
        }
//        if( errorJson != null && errorJson.length() > 0  )
//        {
////            errorJson.append( pagingErrorJson ) ;
//            hasError = true;
//        }
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
        
    }
    @GET
    @Path("/filesList")    
    @Produces("application/json;charset=utf-8")
    
    public String filesList(
                                @QueryParam("loginId")                              String loginId, 
                                @DefaultValue ("10") @QueryParam("recordsPerPage")  String recordsPerPage,
                                @DefaultValue ("1")  @QueryParam("currentPage")     String currentPage,
                                @DefaultValue ("fileNumber") @QueryParam("orderBy") String orderBy, 
                                @DefaultValue ("1")  @QueryParam("ascending")       String ascending,
                                @DefaultValue ("0")  @QueryParam("isResearcher")    String isResearcher, 
                                @DefaultValue ("-1") @QueryParam("fromDate")        String fromDate, 
                                @DefaultValue ("-1") @QueryParam("toDate")          String toDate, 
                                @DefaultValue ("-1") @QueryParam("fileName")        String fileName, 
                                @DefaultValue ("-1") @QueryParam("beneficiaryName") String beneficiaryName, 
                                @DefaultValue ("-1") @QueryParam("fileType")        String fileType, 
                                @DefaultValue ("-1") @QueryParam("fileNumber")      String fileNumber,
                                @DefaultValue ("-1") @QueryParam("inheritanceFileId")      String inheritanceFileId
                          )
    {
        String json="{\"files\": [";
        try
        {
            logger.logInfo("filesList|loginId:%s|recordsPerPage:%s|currentPage:%s|orderBy:%s|ascending:%s|isResearcher:%s|" + 
            "fromDate:%s|toDate:%s|fileName:%s|beneficiaryName:%s|fileType:%s|fileNumber:%s|inheritanceFileId:%s",
                    loginId, recordsPerPage, currentPage, orderBy, ascending, isResearcher, fromDate, toDate, fileName, beneficiaryName,
                           fileType, fileNumber, inheritanceFileId
                           );
            String errorJson = hasInheritanceFileDetailByLoginIdInputParamErrors(loginId, recordsPerPage, currentPage, orderBy, ascending);
            if( errorJson.length() > 0 )                                      
            {
                return errorJson;
            }
            
            
            Map<Object,Object> searchCriteriaMap = new HashMap<Object,Object>();
            if(  isResearcher != null && isResearcher.equals("1"))
            {
                searchCriteriaMap.put("researcherId",loginId.trim());
            }
            else
            {
                searchCriteriaMap.put("loginId",loginId.trim());
            }
            if(  fileName != null && fileName.trim().length()>0 && !fileName.equals("-1"))
            {
                searchCriteriaMap.put("fileName",fileName.trim());
            }
            if(  beneficiaryName != null && beneficiaryName.trim().length()>0 && !beneficiaryName.equals("-1"))
            {
                searchCriteriaMap.put("beneficiaryName",beneficiaryName.trim());
            }
            if(  fileNumber != null && fileNumber.trim().length()>0 && !fileNumber.equals("-1"))
            {
                searchCriteriaMap.put("fileNumber",fileNumber.trim());
            }
            if(  fileType != null && fileType.trim().length()>0 && !fileType.equals("-1"))
            {
                searchCriteriaMap.put("fileType",fileType);
            }
            if(  fromDate != null && fromDate.trim().length()>0 && !fromDate.equals("-1") )
            {
                searchCriteriaMap.put("fromDate",fromDate.trim());
            }
            if(  toDate != null && toDate.trim().length()>0 && !toDate.equals("-1") )
            {
                searchCriteriaMap.put("toDate",toDate.trim());
            }
            if(  inheritanceFileId != null && !inheritanceFileId.equals("-1") )
            {
                logger.logInfo("filesList|addCriteria|inheritanceFileId:%s|", inheritanceFileId);
                searchCriteriaMap.put("inheritanceFileId",inheritanceFileId.trim());
            }
            int totalRecords = InheritanceFileService.getTotalNumberOfRecordsForInheritanceFileDetailByLoginId(searchCriteriaMap);
            List<FileDetailsByPersonId> list = InheritanceFileService.getInheritanceFileDetailByLoginId(
                                                                                                            searchCriteriaMap,
                                                                                                            recordsPerPage != null && recordsPerPage.length()>0? Integer.valueOf(recordsPerPage) :null,
                                                                                                            currentPage!= null && currentPage.length()>0 ?Integer.valueOf(currentPage):null  ,
                                                                                                            orderBy,
                                                                                                            (ascending != null && ascending.equals("1")) ? true:false
                                                                                                       );
            
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            boolean hasItems= false;
            Map<Long,Long> fileAlreadyPresent = new HashMap<Long,Long>();
            for(FileDetailsByPersonId obj : list)
            {
                if( fileAlreadyPresent != null && fileAlreadyPresent.containsKey(obj.getInheritanceFileId()) ) continue;
                
                logger.logDebug("File Number:%s", obj.getFileNumber());
                obj.setInheritanceFileId( obj.getFileDetailsByPersonIdCompositeId().getInheritanceFileId());
                obj.setPersonId( obj.getFileDetailsByPersonIdCompositeId().getPersonId());
                obj.setLoginId( obj.getFileDetailsByPersonIdCompositeId().getLoginId());
                json = json + jsonObj.toJson(obj)+",";
                hasItems= true;
                fileAlreadyPresent.put(obj.getInheritanceFileId(), obj.getInheritanceFileId());
                
            }
            if(hasItems )
            {
                json = json.substring(0,json.length()-1);
            }
            PaginationVO paginationInfo = new PaginationVO( 
                                                            recordsPerPage != null && recordsPerPage.length()>0? Integer.valueOf(recordsPerPage) :1,
                                                            totalRecords            
                                                          );
            json=json +"],\"paginationInfo\":"+ jsonObj.toJson(paginationInfo)+"}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
//            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
                        json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }
    
    private String hasBeneficiaryDetailsByFileIdInputParamErrors(
                                                                        Long inheritanceFileId,
                                                                        String loginId,
                                                                        String recordsPerPage,
                                                                        String currentPage,
                                                                        String orderBy, 
                                                                        String ascending 
                                                                     )throws Exception
    {
        StringBuilder errorJson= null;
        boolean hasError = false;
        if( inheritanceFileId == null  )
        {
            
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.INHERITANCE_FILE_ID_NOT_SPECIFIED).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        
        if( loginId == null || loginId.trim().length() <= 0  )
        {
            
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.INHERITANCE_FILE_LIST_LOGIN_ID_NOT_SPECIFIED).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
//        StringBuilder pagingErrorJson = hasPagingRelatedErrors(recordsPerPage, currentPage);
//         if( pagingErrorJson != null && pagingErrorJson.length() > 0  )
//        {
//            errorJson.append( pagingErrorJson ) ;
//            hasError = true;
//        }
        if( orderBy != null && orderBy.trim().length() > 0 )
        {
            logger.logInfo("hasBeneficiaryDetailsByFileIdInputParamErrors|order by|orderBy%s",orderBy);
           if( 
                !orderBy.equals("beneficiaryName") &&  !orderBy.equals("dateOfBirth") &&  !orderBy.equals("expMatDate") 
             )    
           {
                logger.logInfo("hasBeneficiaryDetailsByFileIdInputParamErrors|orderBy:%s",orderBy);
                if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
                errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.INCORRECT_ORDER_BY).append(",");
                errorJson=errorJson.append( errorMsgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")
                                                                ).
                                                                append("\",");
                errorJson=errorJson.append( errorMsgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")
                                                               ).
                                                               append("\"");
                errorJson = errorJson.append("},");

                hasError = true;
            }
        }
        
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
        
    }
    
    @GET
    @Path("/beneficiariesList")    
    @Produces("application/json;charset=utf-8")
    public String beneficiariesList(
                                                        @QueryParam("fileId")                                Long fileId,
                                                        @QueryParam("loginId")                               String loginId,
                                                        @DefaultValue ("10") @QueryParam("recordsPerPage")   String recordsPerPage,
                                                        @DefaultValue ("1") @QueryParam("currentPage")       String currentPage,
                                                        @DefaultValue ("beneficiaryName") @QueryParam("orderBy")           String orderBy, 
                                                        @DefaultValue ("1") @QueryParam("ascending")         String ascending ,
                                                        @DefaultValue ("-1") @QueryParam("beneficiaryName")  String beneficiaryName, 
                                                        @DefaultValue ("-1") @QueryParam("incomeCategory")   String incomeCategory, 
                                                        @DefaultValue ("-1") @QueryParam("isStudent")        String isStudent,
                                                        @DefaultValue ("-1") @QueryParam("minorStatus")      String minorStatus,
                                                        @DefaultValue ("-1") @QueryParam("beneficiaryId")    Long beneficiaryId
                                                        
                                                
                                                )
    {
        String json="{\"beneficiaries\": [";
        try
        {
            logger.logInfo("beneficiariesList:loginId:%s|fileId:%s| recordsPerPage:%s|currentPage:%s|orderBy:%s|ascending:%s|beneficiaryName:%s|incomeCategory:%s|isStudent:%s|minorStatus:%s|:beneficiaryId:%s",
                                                          loginId,fileId, recordsPerPage, currentPage, orderBy, ascending,beneficiaryName,incomeCategory,isStudent,minorStatus,beneficiaryId);
            String errorJson = hasBeneficiaryDetailsByFileIdInputParamErrors(fileId, loginId, recordsPerPage, currentPage, orderBy, ascending);
            if( errorJson.length() > 0 )                                      
            {
                return errorJson;
            }
            
            Map<Object,Object> searchCriteriaMap = new HashMap<Object,Object>();
            searchCriteriaMap.put("loginId", loginId.trim());
            searchCriteriaMap.put("fileId", fileId);
            if(!beneficiaryName.equals("-1"))
            {
                searchCriteriaMap.put("beneficiaryName", beneficiaryName.trim());
            }
            if(!incomeCategory.equals("-1"))
            
            {
                searchCriteriaMap.put("incomeCategory", incomeCategory.trim());
            }
            if(!isStudent.equals("-1"))
            
            {
                searchCriteriaMap.put("isStudent", isStudent.trim());
            }
            if(!minorStatus.equals("-1"))
            
            {
                searchCriteriaMap.put("minorStatus", minorStatus.trim());
            }
            if(beneficiaryId != null && beneficiaryId.compareTo(-1L)!=0)
            
            {
                searchCriteriaMap.put("beneficiaryId", beneficiaryId);
            }
            
            List<BeneficiaryDetailsForMobile> list = InheritanceFileService.getBeneficiaryDetailByFileId(
                                                                                                            searchCriteriaMap, 
                                                                                                            recordsPerPage != null && recordsPerPage.length() >0 ?Integer.valueOf(recordsPerPage) :null ,
                                                                                                            currentPage != null && currentPage.length() >0 ?Integer.valueOf(currentPage) :null  ,
                                                                                                            orderBy,
                                                                                                            (ascending != null && ascending.equals("1")) ? true:false
                                                                                                        );
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            boolean hasItems= false;
            for(BeneficiaryDetailsForMobile obj : list)
            {

                json = json + jsonObj.toJson(obj)+",";
                hasItems= true;
                
                
            }
            if(hasItems )
            {
                json = json.substring(0,json.length()-1);
            }
            
            json=json +"]}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                 ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }
    
    
    @GET
    @Path("/beneficiaryDetailsByFileIdLoginId")    
    @Produces("application/json;charset=utf-8")
    public String beneficiaryDetailsByFileIdLoginId(
                                                        @QueryParam("fileId")        Long fileId,
                                                        @QueryParam("loginId")       String loginId
                                                      )
    {
        String json="{\"beneficiaries\": [";
        try
        {
                logger.logInfo("beneficiaryDetailsByFileIdLoginId|fileId:%s|loginId:%s",fileId,loginId);
//                String errorJson = isLoginIdAllowedToMakeRequest(fileId, loginId);
//                logger.logInfo("beneficiaryDetailsByFileIdLoginId|errorJson:%s",errorJson);
//                if( errorJson!= null && errorJson.length()> 0 && !errorJson.equals(allowedToMakeRequestSuccess))
//                {
//                    return errorJson;
//                }
            
                Map<Object,Object> searchCriteriaMap = new HashMap<Object,Object>();
//                searchCriteriaMap.put("loginId", loginId.trim());
//                searchCriteriaMap.put("onlyRelatedBeneficiary", "1");
//                searchCriteriaMap.put("fileId", fileId);
                PersonView person  = UtilityService.getPersonInformation( loginId,"-1" );
                logger.logDebug("beneficiaryDetailsByFileIdLoginId:personId:%s",person.getPersonId());
//                List<BeneficiaryDetailsForMobile> list = InheritanceFileService.getBeneficiaryDetailByFileId(
//                                                                                                                searchCriteriaMap, 
//                                                                                                                null ,
//                                                                                                                null  ,
//                                                                                                                "inheritanceBeneficiaryId",
//                                                                                                                true
//                                                                                                            );
                List<BeneficiaryDetailsForMobile> list = InheritanceFileService.getRelatedPersonsOfPersonId(person.getPersonId(), fileId,true);
                Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();
                boolean hasItems= false;
                Map<Long,Long> beneficiaryAlreadyPresentMap = new HashMap<Long,Long>();
                for(BeneficiaryDetailsForMobile obj : list)
                {
                    //In case of representative if a beneficiary has more than one representative than beneficiary can be duplicated
                    if(beneficiaryAlreadyPresentMap.containsKey(obj.getInheritanceBeneficiaryId() )) continue;
                    
                    BeneficiaryDetailsForMobile newObj  = new BeneficiaryDetailsForMobile();
                    
                    newObj.setPersonId( obj.getPersonId() );
                    newObj.setInheritanceBeneficiaryId(obj.getInheritanceBeneficiaryId() );
                    
                    newObj.setBeneficiaryNameAr( obj.getBeneficiaryNameAr() != null ?obj.getBeneficiaryNameAr():"");
                    newObj.setBeneficiaryName( obj.getBeneficiaryName() != null     ?obj.getBeneficiaryName():(obj.getBeneficiaryNameAr() != null ?obj.getBeneficiaryNameAr():""));
                    String availableBalance = "N/A";
                    if( obj.getPersonId().compareTo(person.getPersonId())==0)
                    {
                        availableBalance = InheritanceFileService.getAvailableBalance( newObj.getPersonId(), fileId, null);
                        availableBalance =(availableBalance != null?availableBalance:"0");
                    }
                    newObj.setAvailableBalance( availableBalance );
                    
                    json = json + jsonObj.toJson(newObj)+",";
                    hasItems= true;
                }
                if(hasItems )
                {
                    json = json.substring(0,json.length()-1);
                }
                json=json +"]}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                    ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }

    public String isLoginIdAllowedToMakeRequest(Long fileId, String loginId) throws java.lang.Exception 
    {
        StringBuilder errorJson = null;
        logger.logInfo("fileId:%s|loginId:%s",fileId,loginId);
        //Get the list of persons allowed to make any request.
        List<PersonView> applicantsForFile=  UtilityService.getAllowedApplicantFromInheritanceFile( fileId );
        boolean isLoginIdAllowedToMakeRequest = false;
        logger.logInfo("Size of  applicantsForFile:%s",applicantsForFile.size());
        for(PersonView obj : applicantsForFile)
        {
            logger.logInfo("PersonId:%s|Name:%s|LoginId:%s",obj.getPersonId(),obj.getPersonFullName(),obj.getLoginId());
            if( obj.getLoginId() != null && obj.getLoginId().equals(loginId) )
            {
                isLoginIdAllowedToMakeRequest = true;
                break;
            }
        }
        if(!isLoginIdAllowedToMakeRequest)
        {
            
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.LOGIN_ID_NOT_ALLOWED_TO_MAKE_REQUEST  ).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("msg.error.loggedInPersonNotAllowedToCreateRequest","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("msg.error.loggedInPersonNotAllowedToCreateRequest","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("}");
        }
        if( errorJson != null)
        {
            return errorJson.toString()+"]}";
            
        }
        else
        {
          return allowedToMakeRequestSuccess;
        }
    }
    
    private String hasAssetDetailsByFileIdInputParamErrors(
                                                                        Long inheritanceFileId,
                                                                        String loginId,
                                                                        String recordsPerPage,
                                                                        String currentPage,
                                                                        String orderBy, 
                                                                        String ascending 
                                                                     )throws Exception
    {
        StringBuilder errorJson= null;
        boolean hasError = false;
        if( inheritanceFileId == null  )
        {
            
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.INHERITANCE_FILE_ID_NOT_SPECIFIED).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        
        if( loginId == null || loginId.trim().length() <= 0  )
        {
            
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.INHERITANCE_FILE_LIST_LOGIN_ID_NOT_SPECIFIED).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
    //        StringBuilder pagingErrorJson = hasPagingRelatedErrors(recordsPerPage, currentPage);
    //         if( pagingErrorJson != null && pagingErrorJson.length() > 0  )
    //        {
    //            errorJson.append( pagingErrorJson ) ;
    //            hasError = true;
    //        }
        if( orderBy != null && orderBy.trim().length() > 0 )
        {
           if( 
                !orderBy.equals("assetName") ||  !orderBy.equals("assetType") ||  !orderBy.equals("assetNumber") 
             )    
           {
                if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
                errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.INCORRECT_ORDER_BY).append(",");
                errorJson=errorJson.append( errorMsgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")
                                                                ).
                                                                append("\",");
                errorJson=errorJson.append( errorMsgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")
                                                               ).
                                                               append("\"");
                errorJson = errorJson.append("},");

                hasError = true;
            }
        }
        
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
        
    }
    
    
    @GET
    @Path("/assetsList")    
    @Produces("application/json;charset=utf-8")
    public String getAssetDetailsByFileId(
                                            @QueryParam("fileId")        Long fileId,
                                            @QueryParam("loginId")       String loginId,
                                            @QueryParam("recordsPerPage")String recordsPerPage,
                                            @QueryParam("currentPage")   String currentPage,
                                            @QueryParam("orderBy")       String orderBy, 
                                            @QueryParam("ascending")     String ascending 
                                                
                                        )
    {
        String json="{\"assets\": [";
        try
        {
            String errorJson = hasAssetDetailsByFileIdInputParamErrors(fileId, loginId, recordsPerPage, currentPage, orderBy, ascending);
            if( errorJson.length() > 0 )                                      
            {
                return errorJson;
            }
            
            Map<Object,Object> searchCriteriaMap = new HashMap<Object,Object>();
            searchCriteriaMap.put("loginId", loginId.trim());
            searchCriteriaMap.put("fileId", fileId);
            logger.logDebug("getAssetDetailsByFileId:loginId:%s|fileId:%s",loginId,fileId);
            List<AssetDetailsForMobile> list = InheritanceFileService.getAssetDetailByFileId(
                                                                                            searchCriteriaMap, 
                                                                                            recordsPerPage != null && recordsPerPage.length() >0 ?Integer.valueOf(recordsPerPage) :null ,
                                                                                            currentPage != null && currentPage.length() >0 ?Integer.valueOf(currentPage) :null  ,
                                                                                            orderBy,
                                                                                            (ascending != null && ascending.equals("1")) ? true:false
                                                                                            );
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            
            boolean hasItems= false;
            Map<Long,String> map = InheritanceFileService.getAssetSharesForLoginId(loginId, fileId);
            for(AssetDetailsForMobile obj : list)
            {
                if( map != null && map.containsKey(obj.getInheritedAssetId()))
                {
                  obj.setPersonShare( map.get(obj.getInheritedAssetId()));
                }
                else
                {
                        obj.setPersonShare( "0");
                }
                
                json = json + jsonObj.toJson(obj)+",";
                logger.logDebug("json::"+json);
                hasItems= true;
            }
            if(hasItems )
            {
                json = json.substring(0,json.length()-1);
            }
            
            json=json +"]}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
//            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
                        json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }
    @GET
    @Path("/aboutAmaf")    
    @Produces("application/json;charset=utf-8")
    public String getAboutAmaf()
    {
        String json="";
        try
        {
    
            json="{\"aboutAmaf\": \"http://amaf.gov.ae/amaf/aboutAMAF/Vision_Mission\"}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
//            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
                        json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
                logger.logDebug("getAboutAmaf-Finish");
        }
        return json;
    }
    
    @GET
    @Path("/contactDetails")    
    @Produces("application/json;charset=utf-8")
    
    public String getAmafContactDetails()
    {
        String json="";
        try
        {
//            logger.logDebug("getAmafContactDetails-Start");
            AmafContactDetails obj = UtilityService.getAmafContactDetails();
//            logger.logDebug("obj Address:"+obj.getAddress());
//            logger.logDebug("getAmafContactDetails-Initializing GSON");
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
//            logger.logDebug("getAmafContactDetails-before Converting Object In json");
            
            json =  jsonObj.toJson(obj);
//            logger.logDebug("getAmafContactDetails-After Converting Object In json");
//            logger.logDebug("json:"+json);
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
//            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
                logger.logDebug("getAmafContactDetails-Finish");
        }
        return json;
    }
    
    @GET
    @Path("/disbursementReasonSuperTypes")    
    @Produces("application/json;charset=utf-8")
    public String getDisbursementReasonSuperTypes()
    {
        String json="";
        try
        {
            json="{\"disbursementReasonSuperTypes\": [";
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            List<DomainData> objList = UtilityService.getDomainDataPOJOById(217L);
            boolean hasReason= false;
            for(DomainData obj : objList)
            {
                
                    json = json + jsonObj.toJson(obj)+",";
                    hasReason = true;
            }
            if( hasReason  )
            {
                json = json.substring(0,json.length()-1)+"]}";
            }
            else
            {
                logger.logDebug("Not Found..");
                json = json +"]}";    
            }
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
                logger.logDebug("getDisbursementReasonSuperTypes-Finish");
        }
        return json;
    }
    
    
    @GET
    @Path("/disbursementReason")    
    @Produces("application/json;charset=utf-8")
    public String getDisbursementReason(@QueryParam("mainReasonId")Long mainReasonId)
    {
        String json="";
        try
        {
            logger.logDebug("mainReasonId:%s",mainReasonId);
            json="{\"disbursementReason\": [";
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            List<DisbursementReason> objList = utilityServiceObj.getAllDisbursementReasonsList();
            boolean hasReason= false;
            for(DisbursementReason obj : objList)
            {
                
                if( 
                        mainReasonId.compareTo( -1L ) != 0  && obj.getType() != null &&
                        mainReasonId.compareTo( obj.getType().getDomainDataId() ) == 0 
                   )
                {
                    json = json + jsonObj.toJson(obj)+",";
                    hasReason = true;
                }
            }
            if( hasReason  )
            {
                json = json.substring(0,json.length()-1)+"]}";
            }
            else
            {
                logger.logDebug("Not Found..");
                json = json +"]}";    
            }
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
                logger.logDebug("getDisbursementReason-Finish");
        }
        return json;
    }
    
    @GET
    @Path("/disbursementReasonSubType")    
    @Produces("application/json;charset=utf-8")
    public String getDisbursementReasonSubType(@QueryParam("reasonId")String reasonId)
    {
        String json="";
        try
        {
            json="{\"disbursementReasonSubType\": [";
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            List<ItemType> objList = UtilityService.getItemTypesListByReasonId(reasonId);
            boolean hasReason= false;
            for(ItemType obj : objList)
            {
                
                json = json + jsonObj.toJson(obj)+",";
                hasReason = true;
            }
            if( hasReason  )
            {
                json = json.substring(0,json.length()-1)+"]}";
            }
            else
            {
                logger.logDebug("Not Found..");
                json = json +"]}";    
            }
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
                logger.logDebug("getDisbursementReasonSubType-Finish");
        }
        return json;
    }
    
    public String hasErrorNormalDisbursementRequest(NormalDisbursementInputParameter inputObj,RequestService requestService  )throws Exception
    {
        String hasError = "";
//        
//        if( inputObj.getDescription() == null || inputObj.getDescription().trim().length() <= 0 )
//        {
//        }
        return hasError;
    }
    
    @POST
    @Path("/persistNormalDisbursementRequest")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String persistNormalDisbursementRequest(String inputParameters)//(@FormParam("inputParameters")String inputParameters)
    {
        String json="";
        try
        {
            
            Gson jsonObj =  new GsonBuilder().create();
            NormalDisbursementInputParameter inputObj = jsonObj.fromJson(inputParameters,NormalDisbursementInputParameter.class);
            RequestService requestService  = new RequestService();
            String validationResult = hasErrorNormalDisbursementRequest(inputObj,requestService);
            if(validationResult != null && validationResult.trim().length()>0) return validationResult;
            
            
            RequestView requestView = requestService.persistNormalDisbursementRequest(inputObj);
            json=getRequestSavedSuccessJSON(requestView);
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
                logger.logDebug("persistNormalDisbursementRequest-Finish");
        }
        return json;
    }
    
    private String hasErrorNOLRequest(NolInputParameter inputObj,RequestService requestService  )throws Exception
    {
        StringBuilder errorJson = null;
        boolean hasError = false;
        if( 
            inputObj.getNolTypeId() == null || 
            inputObj.getNolTypeId().compareTo(-1L) == 0
          )
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.NOL_TYPE_REQUIRED).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( "msg.error.nolTypeRequired", "en" )
                                                           ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( "msg.error.nolTypeRequired", "ar" )
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( 
            inputObj.getDescription() == null || 
            inputObj.getDescription().trim().length() <= 0 
          )
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.NOL_REASON_REQUIRED ).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( "msg.error.nolReasonRequired", "en" )
                                                           ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( "msg.error.nolReasonRequired", "ar" )
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
    }
    
    
    @GET
    @Path("/memsNOLTypes")    
    @Produces("application/json;charset=utf-8")
    public String getMEMSNolTypes( @DefaultValue ("-1")  @QueryParam("context")String context)
    {
        String json="";
        try
        {
            
            json="{\"memsNOLTypes\": [";
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            List<MemsNolTypeView> objList = utilityServiceObj.getMemsNolTypeList( Long.parseLong(context) );
            boolean hasReason= false;
            for(MemsNolTypeView obj : objList)
            {
                if( obj.getDescription() == null  )
                {
                    obj.setDescription("");
                }
                json = json + jsonObj.toJson(obj)+",";
                hasReason = true;
            }
            if( hasReason  )
            {
                json = json.substring(0,json.length()-1)+"]}";
            }
            else
            {
                logger.logDebug("Not Found..");
                json = json +"]}";    
            }
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
                logger.logDebug("memsNOLTypes-Finish");
        }
        return json;
    }
    
    @POST
    @Path("/persistMEMSNolRequest")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String persistMEMSNolRequest(String inputParameters)
    {
        String json="";
        try
        {
            Gson jsonObj =  new GsonBuilder().create();
            NolInputParameter inputObj = jsonObj.fromJson(inputParameters,NolInputParameter.class);
            RequestService requestService  = new RequestService();
            String validationResult = hasErrorNOLRequest(inputObj,requestService);
            if(validationResult != null && validationResult.trim().length()>0) return validationResult;
            
            RequestView requestView = requestService.persistNolRequest(inputObj);
            json=getRequestSavedSuccessJSON(requestView);
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("persistMEMSNolRequest-Finish");
        }
        return json;
    }
    
    
    private String getRequestSavedSuccessJSON(RequestView requestView)throws Exception
    {
        
        
        String json="{\"result\": [{" +
                                    " \"requestNumber\":\""+requestView.getRequestNumber()+"\""+
                                    ",\"requestId\":\""+ requestView.getRequestId()+"\""+
                                    ",\"statusEn\":\""+ requestView.getStatusEn()+"\""+
                                    ",\"statusAr\":\""+ requestView.getStatusAr()+"\""+
                                    ",\"msgEn\":\""+ MessageFormat.format(ResourceWrapper.getInstance().getProperty("msg.success.requestSaved","en"),requestView.getRequestNumber())+"\""+
                                    ",\"msgAr\":\""+ MessageFormat.format(ResourceWrapper.getInstance().getProperty("msg.success.requestSaved","ar"),requestView.getRequestNumber())+"\"" +
                                 "}]}";
        return json;
    }
    
    private String hasUploadDocumentParamErrors(
                                                String associatedObjectId,
                                                String loginId,
                                                String externalId,
                                                String documentTitle
                                                
                                               )throws Exception
    {
        StringBuilder errorJson= null;
        boolean hasError = false;
        if( associatedObjectId == null  || associatedObjectId.trim().length()<= 0 )
        {
            
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.ASSOCIATED_OBJECT_ID_REQUIRED).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1101","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1101","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( externalId == null || externalId.trim().length() <= 0  )
        {
            
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.PROCEDURE_TYPE_ID_REQUIRED).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1102","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1102","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( loginId == null || loginId.trim().length() <= 0  )
        {
            
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.LOGIN_ID_REQUIRED).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1103","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1103","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( documentTitle== null || documentTitle.trim().length() <= 0  )
        {
            
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.DOCUMENT_TITLE_REQUIRED).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1104","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1104","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
    }
    
    @POST
    @Path("/uploadDocument")    
    @Produces("application/json;charset=utf-8")
    public String uploadDocument(
                                   @FormParam("base64EncodedString") String base64EncodedString,
                                   @FormParam("associatedObjectId")String associatedObjectId,
                                   @FormParam("loggedInUser")String loggedInUser,
                                   @FormParam("documentFileName")String documentFileName,
                                   @FormParam("contentType")String contentType,
                                   @FormParam("comments")String comments,
                                   @FormParam("externalId")String externalId,
                                   @FormParam("sizeInBytes")String   sizeInBytes,
                                   @FormParam("documentTitle")String documentTitle
                                )
    {
        String json ="";
        try
        {
            logger.logInfo(
                            "uploadDocument|base64EncodedString.length:%s|associatedObjectId:%s|loggedInUser:%s|" +
                            "documentFileName:%s|contentType:%s|comments:%s|externalId:%s|sizeInBytes:%s|documentTitle:%s",  base64EncodedString.length(),
                                                                                                                                associatedObjectId,
                                                                                                                                loggedInUser,documentFileName,contentType,
                                                                                                                                comments,externalId,sizeInBytes,documentTitle
                          );
            String errorJson = hasUploadDocumentParamErrors(associatedObjectId,loggedInUser,externalId,documentTitle);
            
            if( errorJson.length() > 0 )                                      
            {
                return errorJson;
            }
                
            DocumentView documentView = new DocumentView();
            
            documentView.setUpdatedOn( new Date() );
            documentView.setCreatedOn( new Date() );
            documentView.setIsDeleted(0L);
            documentView.setRecordStatus(1L);
            documentView.setDocumentId(null);
            documentView.setDocumentPath(null);
            documentView.setDocTypeId(8l);
            documentView.setRepositoryId("pimsDb");
            
            documentView.setCreatedBy(loggedInUser);
            documentView.setUpdatedBy(loggedInUser);
            documentView.setDocumentTitle(documentTitle);
            documentView.setAssociatedObjectId( associatedObjectId );
            documentView.setDocumentFileName(documentFileName);
            documentView.setContentType(contentType);
            documentView.setComments(comments);
            documentView.setExternalId(externalId);
            documentView.setSizeInBytes(Long.valueOf(  sizeInBytes  )  );
            
            documentView = utilityServiceObj.addAttachment(documentView, base64EncodedString) ;       
            logger.logInfo("uploadDocument|documentId:%s", documentView.getDocumentId());
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            List<DocumentView> list = new ArrayList<DocumentView>();
            list.add(documentView);
            json  = getDocumentJSONFromList(false, jsonObj, list);
            logger.logInfo("uploadDocument|json:%s", json);
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("uploadDocument-Finish");
        }
        return json;
    }
    
    @POST
    @Path("/uploadFile")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public String uploadFile(
                               @FormDataParam("file") InputStream uploadedInputStream,
                               @FormDataParam("file") FormDataContentDisposition fileDetail
                               
                            )
    {
        String json="";
        try
        {
            logger.logInfo("Info in upload");
            logger.logDebug("In uploadFile");
            Gson jsonObj =  new GsonBuilder().create();
            String fileName =  "No data";
            if( uploadedInputStream != null )
            {
                logger.logInfo("uploadedInputStream not null");
                fileName="uploadedInputStream not null";
            }
           else if( fileDetail != null )
            {
                fileName =  String.valueOf(fileDetail.getSize());
                      
            }
            
            json = jsonObj.toJson(fileName);
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("persistMEMSNolRequest-Finish");
        }
        return json;
    }
        
        
    @GET
    @Path("/allRequestTypes")    
    @Produces("application/json;charset=utf-8")
    public String getAllRequestTypes()
    {
        String json="";
        try
        {
            
            json="{\"requestTypes\": [";
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            List<RequestTypeView> objList = propertyServiceObj.getAllRequestTypes();
            boolean hasReason= false;
            for(RequestTypeView obj : objList)
            {
                
                if(
                    obj.getRequestTypeId().compareTo(Constant.MemsRequestType.NOL_REQUEST_ID) == 0 ||
                    obj.getRequestTypeId().compareTo(Constant.MemsRequestType.NORMAL_PAYMENT_REQUEST_ID) == 0 
                  )
                {
                    json = json + jsonObj.toJson(obj)+",";
                    hasReason = true;
                }
            }
            if( hasReason  )
            {
                json = json.substring(0,json.length()-1)+"]}";
            }
            else
            {

                json = json +"]}";    
                
            }

        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        
        return json;
    }
        
    @GET
    @Path("/requestTypesForUserId")    
    @Produces("application/json;charset=utf-8")
    public String requestTypesForUserId(   @QueryParam("loginId") String loginId)
    {
        String json="";
        boolean hasReason= false;
        try
        {
            logger.logInfo("requestTypesForUserId|loginId:%s|",loginId);
            json="{\"requestTypes\": [";
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            String roles = UtilityService.getSmartAppRoleForLoggedInUser(loginId);
            Session session = (Session) EntityManager.getBroker().getSession();
            List<Long> requestTypeIdList = new ArrayList<Long>();
            if(roles.contains("\"isBeneficiary\":\"1\""))
            {
                requestTypeIdList.add(Constant.MemsRequestType.NOL_REQUEST_ID );
                requestTypeIdList.add(Constant.MemsRequestType.NORMAL_PAYMENT_REQUEST_ID );
                requestTypeIdList.add(Constant.REQUEST_TYPE_MINOR_MAINTENANCE_REQUEST);
            }
            if (roles.contains("\"isTenant\":\"1\""))
            {
                
                requestTypeIdList.add(Constant.REQUEST_TYPE_MAINTENANCE_REQUEST);
                requestTypeIdList.add(Constant.REQUEST_TYPE_NOL);
                requestTypeIdList.add(Constant.RENEW_CONTRACT);
                requestTypeIdList.add(Constant.CANCEL_CONTRACT);
                requestTypeIdList.add(Constant.REQUEST_TYPE_REPLACE_CHEQUE);
                
                requestTypeIdList.add(Constant.REQUEST_TYPE_PAY_BOUNCE_CHEQUE);
                requestTypeIdList.add(Constant.REQUEST_TYPE_TRANSFER_CONTRACT);
                requestTypeIdList.add(Constant.REQUEST_TYPE_COMPLAINT_ID);
            }
            if(requestTypeIdList.size()<=0)
            {
                requestTypeIdList.add(-1l);
            }
            
            
            Criteria criteria = session.createCriteria( RequestType.class);
            criteria.add(Restrictions.eq("isDeleted", 0l) );
            criteria.add(Restrictions.in("requestTypeId", requestTypeIdList ));
            
            List<RequestType> objList = (ArrayList<RequestType>)criteria.list();
            for(RequestType obj : objList)
            {
                obj.getRequestTypeAr();
                obj.getRequestTypeEn();
                obj.getRequestTypeId();
                obj.getModule();
                    
                json = json + jsonObj.toJson(obj)+",";
                hasReason = true;
                
            }
            if( hasReason  )
            {
                json = json.substring(0,json.length()-1)+"]}";
            }
            else
            {
                logger.logDebug("Not Found..");
                json = json +"]}";    
            }
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        
        return json;
    }
    
        
    @GET
    @Path("/requestTypes")    
    @Produces("application/json;charset=utf-8")
    public String getRequestTypes()
    {
        String json="";
        boolean hasReason= false;
        try
        {
            json="{\"requestTypes\": [";
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            Session session = (Session) EntityManager.getBroker().getSession();
            List<Long> requestTypeIdList = new ArrayList<Long>();
            requestTypeIdList.add(Constant.MemsRequestType.NOL_REQUEST_ID );
            requestTypeIdList.add(Constant.MemsRequestType.NORMAL_PAYMENT_REQUEST_ID );
            requestTypeIdList.add(Constant.REQUEST_TYPE_MAINTENANCE_REQUEST);
            requestTypeIdList.add(Constant.REQUEST_TYPE_NOL);
            requestTypeIdList.add(Constant.RENEW_CONTRACT);
            requestTypeIdList.add(Constant.CANCEL_CONTRACT);
            requestTypeIdList.add(Constant.REQUEST_TYPE_REPLACE_CHEQUE);
            requestTypeIdList.add(Constant.REQUEST_TYPE_PAY_BOUNCE_CHEQUE);
            Criteria criteria = session.createCriteria( RequestType.class);
            criteria.add(Restrictions.eq("isDeleted", 0l) );
            criteria.add(Restrictions.in("requestTypeId", requestTypeIdList ));
            
            List<RequestType> objList = (ArrayList<RequestType>)criteria.list();
            for(RequestType obj : objList)
            {
         
                json = json + jsonObj.toJson(obj)+",";
                hasReason = true;
                
            }
            if( hasReason  )
            {
                json = json.substring(0,json.length()-1)+"]}";
            }
            else
            {
                logger.logDebug("Not Found..");
                json = json +"]}";    
            }
        
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        
        return json;
    }
    
    private String hasRequestListByLoginIdInputParamErrors  (
                                                                Long   personId,
                                                                String recordsPerPage,
                                                                String currentPage,
                                                                String orderBy, 
                                                                String ascending 
                                                            )throws Exception
    {
        StringBuilder errorJson= new StringBuilder("{\"errors\": [");
        boolean hasError = false;
        if( personId == null )
        {
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.INHERITANCE_FILE_LIST_LOGIN_ID_NOT_SPECIFIED).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
    //        StringBuilder pagingErrorJson = hasPagingRelatedErrors(recordsPerPage, currentPage);
        if( orderBy != null && orderBy.trim().length() > 0 )
        {
           if( !orderBy.equals("requestNumber") &&
               !orderBy.equals("createdOn") 
             )    
           {
            
    //                errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.INCORRECT_ORDER_BY).append("},");
                errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.INCORRECT_ORDER_BY).append(",");
                errorJson=errorJson.append( errorMsgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")
                                                                ).
                                                                append("\",");
                errorJson=errorJson.append( errorMsgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")
                                                               ).
                                                               append("\"");
                errorJson = errorJson.append("},");    
                hasError = true;
            }
            
        }
//        if( errorJson != null && errorJson.length() > 0  )
//        {
//    //            errorJson.append( pagingErrorJson ) ;
//            hasError = true;
//        }
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
    }
    
    @GET
    @Path("/requestList")    
    @Produces("application/json;charset=utf-8")
    public String requestList(
                                             @QueryParam("personId")       String personId, 
                                            @DefaultValue ("-1") @QueryParam("fromDate")       String fromDate, 
                                            @DefaultValue ("-1") @QueryParam("toDate")         String toDate, 
                                            @DefaultValue ("-1") @QueryParam("requestNumber")  String requestNumber, 
                                            @DefaultValue ("-1")  @QueryParam("requestType")  String requestType, 
                                            @DefaultValue ("10") @QueryParam("recordsPerPage") String recordsPerPage,
                                            @DefaultValue ("1") @QueryParam("currentPage")    String currentPage,
                                            @DefaultValue ("createdOn") @QueryParam("orderBy")        String orderBy, 
                                            @DefaultValue ("0") @QueryParam("ascending")      String ascending 
                                       )
    {
        String json="{\"requests\": [";
        try
        {
//            Gson jsonObj =  new GsonBuilder().create();
            logger.logInfo("requestList|personId:%s|fromDate:%s|toDate:%s|requestNumber:%s|requestType:%s|recordsPerPage:%s|currentPage:%s|orderBy:%s|ascending:%s", personId,fromDate,toDate,requestNumber,requestType,recordsPerPage,
                                                                                                                                        currentPage,orderBy,ascending
                          );
//            RequestViewForMobileSearch searchCriteria = jsonObj.fromJson(criteria, RequestViewForMobileSearch.class);
            String errorJson = hasRequestListByLoginIdInputParamErrors( new Long( personId ), recordsPerPage, currentPage, orderBy, ascending);
            
            if( errorJson.length() > 0 )                                      
            {
                logger.logInfo("hasError:True");
                return errorJson;
            }
            RequestService requestService = new RequestService();
            Map<Object,Object> searchCriteriaMap = new HashMap<Object,Object>();
            searchCriteriaMap.put("loginId",new Long( personId ));
            if( fromDate != null && fromDate.trim().length()> 0 && !fromDate.equals("-1"))
            {
                searchCriteriaMap.put("fromDate", fromDate);
            }
            if( toDate != null && toDate.trim().length()> 0 && !toDate.equals("-1"))
            {
                searchCriteriaMap.put("toDate", toDate);
            }
            if( requestNumber != null && requestNumber.trim().length()>0  && !requestNumber.equals("-1"))
            {
                searchCriteriaMap.put("requestNumber", requestNumber);
            }
            if( requestType != null && requestType.trim().length()> 0 && !requestType.equals("-1"))
            {
                searchCriteriaMap.put("requestType", requestType);
            }
            else
            {
                List<Long> requestTypeIn = new ArrayList<Long>();
                requestTypeIn.add( Constant.MemsRequestType.NOL_REQUEST_ID );
                requestTypeIn.add( Constant.MemsRequestType.NORMAL_PAYMENT_REQUEST_ID );
                requestTypeIn.add( Constant.REQUEST_TYPE_MINOR_MAINTENANCE_REQUEST);
                
                requestTypeIn.add( Constant.RENEW_CONTRACT);
                requestTypeIn.add( Constant.REQUEST_TYPE_NOL);
                requestTypeIn.add( Constant.REQUEST_TYPE_MAINTENANCE_REQUEST);
                requestTypeIn.add( Constant.REQUEST_TYPE_TRANSFER_CONTRACT);
                requestTypeIn.add( Constant.REQUEST_TYPE_REPLACE_CHEQUE);
                requestTypeIn.add( Constant.REQUEST_TYPE_PAY_BOUNCE_CHEQUE);
                requestTypeIn.add(Constant.CANCEL_CONTRACT);
                searchCriteriaMap.put("requestTypeIn", requestTypeIn);    
            }
            
            json = requestService.searchRequestsForMobile(
                                                            searchCriteriaMap,
                                                            recordsPerPage != null && recordsPerPage.length()>0? Integer.valueOf(recordsPerPage) :null,
                                                            currentPage!= null && currentPage.length()>0 ?Integer.valueOf(currentPage):null  ,
                                                            (orderBy != null && orderBy.trim().length()>0 &&!orderBy.equals("-1")) ? orderBy:"requestNumber",
                                                            (ascending != null && ascending.equals("1")) ? true:false
                                                          );
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
       
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                 ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }
 
 
    private String hasRequestDetailsErrors  (Request request,Long requestId )throws Exception
    {
        StringBuilder errorJson= new StringBuilder("{\"errors\": [");
        boolean hasError = false;
        if( 
             request == null  ||
             (
             request.getRequestType().getRequestTypeId().compareTo(Constant.MemsRequestType.NOL_REQUEST_ID ) != 0 &&
             request.getRequestType().getRequestTypeId().compareTo(Constant.MemsRequestType.NORMAL_PAYMENT_REQUEST_ID ) != 0 
             )
          )
        {
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.REQUEST_NOT_PRESENT_AGAINST_REQUEST_ID).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("msg.error.invalidRequestId","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("msg.error.invalidRequestId","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
    
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
    }
    
    @GET
    @Path("/requestDetails")    
    @Produces("application/json;charset=utf-8")
    public String getRequestDetails(
                                     @QueryParam("requestId")       Long requestId 
                                   )
    {
        String json="{\"request\": [";
        try
        {
            logger.logInfo("getRequestDetails|RequestId:%s",requestId);
            
            Request request = null;
            if( requestId != null  )
            {
                request = EntityManager.getBroker().findById(Request.class, requestId );
            }
            String errorJson = hasRequestDetailsErrors(request, requestId);
            
            if( errorJson.length() > 0 )                                      
            {
                logger.logInfo("hasError:%s", errorJson);
                return errorJson;
            }
            
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            
            if(request.getRequestType() != null && request.getRequestType().getRequestTypeId().compareTo(Constant.MemsRequestType.NOL_REQUEST_ID ) == 0 )
            {
                logger.logInfo("Nol Request");
                json = getMemsNolRequestDetails(request,jsonObj );
                return json;
            }
            else if(request.getRequestType() != null && request.getRequestType().getRequestTypeId().compareTo(Constant.MemsRequestType.NORMAL_PAYMENT_REQUEST_ID ) == 0 )
            {
                logger.logInfo("Normal Disbursement");
                json = getNormalDisbursementRequestDetails(request,jsonObj );
                return json;
            }
//            json=json +"]}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                 ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }
    private String getMemsNolRequestDetails(Request request,     Gson jsonObj )throws Exception
    {
      
      StringBuilder json = new StringBuilder("{\""+Constant.MemsRequestType.NOL_REQUEST_ID.toString()+"\": [{");    
      
      json = getRequestRelatedJSON(request, json);
      if( request.getMemsNolType()!= null )
      {
        MemsNolTypes nolType =  EntityManager.getBroker().findById(MemsNolTypes.class, request.getMemsNolType() );        
        json = json.append(",\"nolReasonTypeId\":").append( nolType.getMemsNolTypeId() )
                    .append(",\"nolReasonTypeEn\":\"").append(nolType.getNolTypeNameEn() ).append("\"")
                    .append(",\"nolReasonTypeAr\":\"").append(nolType.getNolTypeNameAr()).append("\"");
      }
      json = json.append(",\"nolDescription\":\"").append(request.getMemsNolReason() ).append("\"");
      json = json.append(",");
      json = json.append(  getDocumentsJSON( null, request.getRequestId().toString(), "memsNoObjectionLetter", jsonObj) );
      json = json.append( "}]}" );
      return json.toString();
    }

    private String getNormalDisbursementRequestDetails(Request request,     Gson jsonObj )throws Exception
    {
      
      StringBuilder json = new StringBuilder("{\""+Constant.MemsRequestType.NORMAL_PAYMENT_REQUEST_ID.toString()+"\": [{");    
      
      json = getRequestRelatedJSON(request,  json);
      json = getDisbursementDetailsJSON(request, json, jsonObj);
      json = json.append(",");
      json = json.append(getDocumentsJSON( null, request.getRequestId().toString(), "memsNormalPayments", jsonObj) );
      json = json.append( "}]}" );
      return json.toString();
    }
    private StringBuilder getRequestRelatedJSON(Request request, StringBuilder json)  throws Exception
    {
        DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        
        DomainData ddStatus = EntityManager.getBroker().findById(DomainData.class, request.getStatusId() );        
        json= json.append("\"requestId\":").append(request.getRequestId() )
                  .append(",\"requestNumber\":\"").append(request.getRequestNumber() ).append("\"")
                  .append(",\"requestDate\":\"").append(df.format( request.getCreatedOn() )  ).append("\"")
                  .append(",\"requestTypeId\":").append(request.getRequestType().getRequestTypeId())
                  .append(",\"requestTypeEn\":\"").append(request.getRequestType().getRequestTypeEn() ).append("\"")
                  .append(",\"requestTypeAr\":\"").append(request.getRequestType().getRequestTypeAr() ).append("\"")
                  .append(",\"requestStatusId\":").append(ddStatus.getDomainDataId() )
                  .append(",\"requestStatusEn\":\"").append(ddStatus.getDataDescEn() ).append("\"")
                  .append(",\"requestStatusAr\":\"").append(ddStatus.getDataDescAr() ).append("\"");
        if(request.getApplicant() != null && request.getApplicant().getPersonId() != null)
        {
          json = json.append(",\"applicantId\":").append(request.getApplicant().getPersonId()  )
                     .append(",\"applicantNameEn\":\"").append(request.getApplicant().getFullNameEn() != null ?request.getApplicant().getFullNameEn():request.getApplicant().getFullName() ).append("\"")
                     .append(",\"applicantNameAr\":\"").append(request.getApplicant().getFullName()).append("\"");
        }
        else
        {
            json = json.append(",\"applicantId\":").append("" )
                       .append(",\"applicantNameEn\":\"\"")
                       .append(",\"applicantNameAr\":\"\"");    
        }
        if(request.getInheritanceFile() != null && request.getInheritanceFile().getInheritanceFileId() != null)
        {
          InheritanceFile file = request.getInheritanceFile();
          DomainData ddFileType = EntityManager.getBroker().findById(DomainData.class, file.getFileType() );        
          DomainData ddFileStatus = EntityManager.getBroker().findById(DomainData.class, file.getStatus() );        
          json = json.append(",\"fileId\":").append( file.getInheritanceFileId() )
                     .append(",\"fileNumber\":\"").append(  file.getFileNumber() ).append("\"")
                     .append(",\"fileOwnerNameEn\":\"").append( file.getFileOwner().getFullNameEn() != null ?file.getFileOwner().getFullNameEn():file.getFileOwner().getFullName()).append("\"")
                     .append(",\"fileOwnerNameAr\":\"").append( file.getFileOwner().getFullName()).append("\"")
                     .append(",\"fileStatusId\":\"").append( ddFileStatus.getDomainDataId() ).append("\"")
                     .append(",\"fileStatusEn\":\"").append( ddFileStatus.getDataDescEn() ).append("\"")
                     .append(",\"fileStatusAr\":\"").append( ddFileStatus.getDataDescAr()).append("\"")
                     .append(",\"fileTypeId\":\"").append( ddFileType.getDomainDataId() ).append("\"")
                     .append(",\"fileTypeEn\":\"").append( ddFileType.getDataDescEn()).append("\"")
                     .append(",\"fileTypeAr\":\"").append(ddFileType.getDataDescAr()).append("\"");
        }
        else
        {
            json = json.append(",\"fileId\":").append( "")
                       .append(",\"fileNumber\":\"\"")
                       .append(",\"fileOwnerNameEn\":\"\"")
                       .append(",\"fileOwnerNameAr\":\"\"")
                       .append(",\"fileStatusId\":\"\"")
                       .append(",\"fileStatusEn\":\"\"")
                       .append(",\"fileStatusAr\":\"\"")
                       .append(",\"fileTypeId\":\"\"")
                       .append(",\"fileTypeEn\":\"\"")
                       .append(",\"fileTypeAr\":\"\"");
        }
        if(request.getUserHappinessIndex() != null && request.getUserHappinessIndex().getHappinessIndexId() != null)
        {
            json = json.append(",\"happinessIndexId\":").append( request.getUserHappinessIndex().getHappinessIndexId() )
                    .append(",\"happinessEn\":\"").append(  request.getUserHappinessIndex().getHappinessEn()).append("\"")
                    .append(",\"happinessAr\":\"").append( request.getUserHappinessIndex().getHappinessAr()).append("\"");
        }
        else if(request.getUserHappinessIndex() == null || request.getUserHappinessIndex().getHappinessIndexId() == null)
        {
            json = json.append(",\"happinessIndexId\":\"\"")
                    .append(",\"happinessEn\":\"\"")
                    .append(",\"happinessAr\":\"\"");
        }
        return json;
    }
   
   private StringBuilder getDisbursementDetailsJSON(Request request, StringBuilder json,Gson jsonObj ) throws Exception
   {
        DisbursementService disbursementService = new DisbursementService();
        List<Long> ids = new ArrayList<Long>();
        for(DisbursementDetails disbDet : request.getDisbursementDetails() )
        {
            ids.add( disbDet.getDisDetId() );
        }
        json= json.append(",\"disbursements\":[");
        List<DisbursementDetailsView>  list = disbursementService.getDisbursementDetails(ids);   
        boolean hasItem = false;
        for(DisbursementDetailsView obj : list )
        {
            if( obj.getBeneficiariesName() == null )
            {
              obj.setBeneficiariesName("");
            }
            if( obj.getReasonId() == null )
            {
                obj.setReasonId(-1l);
            }
            if( obj.getReasonEn() == null )
            {
                obj.setReasonEn("");
            }
            if( obj.getReasonAr() == null )
            {
                obj.setReasonAr("");
            }
            if( obj.getItemId() == null )
            {
                obj.setItemId(-1l);
            }
            if( obj.getItemEn() == null )
            {
                obj.setItemEn("");
            }  
            if( obj.getItemAr() == null )
            {
                obj.setItemAr("");
            }
              json.append( jsonObj.toJson(obj) ).append(",");            
//            json= json.append("{\"disbursementId\":").append( obj.getDisDetId() )
//                      .append(",\"disbursementNumber\":\"").append( obj.getRefNum() )
//                      .append("\",\"amount\":\"").append( obj.getAmount() )
//                      .append("\",\"description\":\"").append( obj.getDescription() )
//                      .append("\",\"disbStatusId\":\"").append( obj.getStatus()  )
//                      .append("\",\"disbStatusEn\":\"").append( obj.getStatusEn()  )
//                      .append("\",\"disbStatusAr\":\"").append( obj.getStatusAr()  )
//                      .append("\",\"disbStatusAr\":\"").append( obj.getReasonAr() )
//                      .append("},");
            hasItem = true;
            
        }
        if(  hasItem )
        {
            json = json.deleteCharAt(json.length()-1).append( "]" );
        }
       return json;
   }
   
   private String getDocumentsJSON( Long documentId, String associatedObjectId,String externalId, Gson jsonObj)throws Exception
   {
       List<DocumentView> list = searchDocument(documentId, associatedObjectId, externalId);    
       boolean isArray = documentId == null ?true:false;
       return getDocumentJSONFromList(isArray, jsonObj, list);
       
   }

    private String getDocumentJSONFromList(boolean isArray , Gson jsonObj, List<DocumentView> list) throws Exception
    {
       String json= "\"documents\":[";
       boolean hasItem = false;
       for(DocumentView obj : list )
       {
             
             obj.setDownloadURL(downloadURL+obj.getDocumentId());
             if( obj.getComments() == null )
             {
                 obj.setComments("");
             }
             if( obj.getDocumentTitle() == null )
             {
               obj.setDocumentTitle("");
             }
             if( obj.getContentType() == null )
             {
                 obj.setContentType("");
             }
             
       //             logger.logInfo("Document with id:%s",obj.getDocumentId() );
             json = json +  jsonObj.toJson(obj) + ",";            
             hasItem = true;
           
       }
       if(  hasItem )
       {
           
           //In case documentId is null it means there might be multiple documents so will be returning array
           if( isArray   )
           {
               
            json = json.substring(0,json.length()-1) +  "]" ;
           }
           else if( json.length()>0 )
           {
               json = json.substring(0,json.length()-1);
               json = json.replace("\"documents\":[", "");
           }
           logger.logInfo("json:%s",json);
       }
       else
       {
            json = json +  "]" ;   
       }
       return json;
    }

    private List<DocumentView> searchDocument(Long documentId, String associatedObjectId,String externalId) throws com.avanza.pims.business.exceptions.PimsBusinessException
    {
       DocumentView searchDocView = new DocumentView();
       if(associatedObjectId != null && associatedObjectId.trim().length() >0 )
       {
        searchDocView.setAssociatedObjectId(associatedObjectId);
       }
       if( externalId != null && externalId.trim().length() > 0 )
       {
        searchDocView.setExternalId(externalId);
       }
       if( documentId != null )
       {
        searchDocView.setDocumentId(documentId);
       }
       searchDocView.setRepositoryId(Constant.Attachment.PIMS_REPOSITORY_ID);
       List<DocumentView> list = utilityServiceObj.getDocuments(searchDocView);
        return list;
    }
   
    @GET
    @Path("/documentDetails")    
    @Produces("application/json;charset=utf-8")
    public String getDocumentDetails( @QueryParam("documentId") Long documentId )
    {
        String json="";
        try
        {
            logger.logInfo("getDocumentDetails|documentId:%s",documentId);
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            String errorJson = "";//hasRequestListByLoginIdInputParamErrors( new Long( personId ), recordsPerPage, currentPage, orderBy, ascending);
            
            if( errorJson.length() > 0 )                                      
            {
                logger.logInfo("hasError:%s", errorJson);
                return errorJson;
            }
//            json = "{";
            json= json+ getDocumentsJSON(documentId,null,null, jsonObj);
//            json=json+ "}";
            return json;
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                 ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }   

    @GET
    @Path("/documentContents")    
//    @Produces("application/octet-stream")
    public String getDocumentContent(
                                     @QueryParam("documentId")       Long documentId
                                   )
    {
        String json="{\"request\": [";
        try
        {
            logger.logInfo("getDocumentContent|documentId:%s",documentId);
            String errorJson = "";//hasRequestListByLoginIdInputParamErrors( new Long( personId ), recordsPerPage, currentPage, orderBy, ascending);
            
            if( errorJson.length() > 0 )                                      
            {
                logger.logInfo("hasError:%s", errorJson);
                return errorJson;
            }
             return utilityServiceObj.getDocumentContent(documentId);
//            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                 ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }   
    
    
    @GET
    @Path("/servieCategories")    
    @Produces("application/json;charset=utf-8")
    public String getServiceCatgories()
    {
        String json="";
        try
        {
            
            json="{\"servieCategories\": [";
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            List<MobileServiceCategory> objList = UtilityService.getMobileServiceCategory();
            boolean hasReason= false;
            for(MobileServiceCategory obj : objList)
            {
                obj.getBulletPointsAr();
                obj.getBulletPointsEn();
                obj.getCategoryAr();
                obj.getCategoryEn();
                obj.getCategoryId();
                obj.getDescEn();
                obj.getDescAr();
                obj.getImageURL();
                obj.getIconURL();
                json = json + jsonObj.toJson(obj)+",";
                hasReason = true;
            }
            if( hasReason  )
            {
                json = json.substring(0,json.length()-1)+"]}";
            }
            else
            {
                logger.logDebug("Not Found..");
                json = json +"]}";    
            }
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
                logger.logDebug("getServiceCatgories-Finish");
        }
        return json;
    }
    
    @GET
    @Path("/servieProcedureTypes")    
    @Produces("application/json;charset=utf-8")
    public String getServiceProcedureTypes()
    {
        String json="";
        try
        {
            
            json="{\"servieProcedureTypes\": [";
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            List<MobileServiceProcedureType> objList = UtilityService.getMobileServiceProcedureTypesList();
            boolean hasReason= false;
            for(MobileServiceProcedureType obj : objList)
            {
             
                obj.getTypeAr();
                obj.getTypeEn();
                obj.getTypeId();
                json = json + jsonObj.toJson(obj)+",";
                hasReason = true;
            }
            if( hasReason  )
            {
                json = json.substring(0,json.length()-1)+"]}";
            }
            else
            {
                logger.logDebug("Not Found..");
                json = json +"]}";    
            }
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
                logger.logDebug("servieProcedureTypes-Finish");
        }
        return json;
    }
    
    
    @GET
    @Path("/serviceProcedures")    
    @Produces("application/json;charset=utf-8")
    public String getServiceProcedures(@QueryParam("categoryId")Long categoryId,@QueryParam("typeId")Long typeId)
    {
        String json="";
        try
        {
            logger.logDebug("categoryId:%s|typeId:%s",categoryId,typeId);
            json="{\"serviceProcedures\": [";
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            if( categoryId.compareTo(-1L) ==0)
            {
              categoryId = null;    
            }
            if( typeId.compareTo(-1L) ==0)
            {
              typeId = null;    
            }
            List<MobileServiceProcedure> objList = UtilityService.getMobileServiceProcedures(categoryId, typeId);
            boolean hasReason= false;
            for (MobileServiceProcedure obj : objList) 
             {
                   obj.getMobileServiceProcedureType();
                   obj.getCategory();
                   obj.getDescAr();
                   obj.getDescEn();
                   obj.getDocURL();
                   obj.getFees();
                   
                   obj.getFlowAr();
                   obj.getFlowEn();
                   obj.getNameAr();
                   obj.getNameEn();
                   json = json + jsonObj.toJson(obj)+",";
                   hasReason = true;
             }
            
            if( hasReason  )
            {
                json = json.substring(0,json.length()-1)+"]}";
            }
            else
            {
                logger.logDebug("Not Found..");
                json = json +"]}";    
            }
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
                logger.logDebug("getServiceProcedures-Finish");
        }
        return json;
    }
    
    

    private String hasBalanceSummaryErrors(
                                            Long personId,
                                            Long inheritanceFileId,
                                            int trxDuration
                                        )throws Exception
    {
        StringBuilder errorJson= null;
        boolean hasError = false;
        if( personId == null  )
        {
            
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.PERSON_ID_REQUIRED).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1201","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1201","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        
        if( !(trxDuration == 7 || trxDuration == 15 || trxDuration == 30 || trxDuration == 60 ||  trxDuration == 90 ||trxDuration == 180 ) )
        {
            
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.INCORRECT_TRX_DURATION).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1202","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1202","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
        
    }
        
    
    @GET
    @Path("/balanceSummary")    
    @Produces("application/json;charset=utf-8")
    public String getBalanceSummary(
                                        @QueryParam("personId")Long personId,
                                        @QueryParam("inheritanceFileId")Long inheritanceFileId,
                                        @QueryParam("trxDuration")int trxDuration,
                                        @QueryParam("statementDate")String statementDate
                                    )
    {
        String json="";
        try
        {
            logger.logDebug("personId:%s|inheritanceFileId:%s|trxDuration:%s|statementDate:%s",personId,inheritanceFileId,trxDuration,statementDate);
            String errorJson = hasBalanceSummaryErrors(personId,inheritanceFileId,trxDuration);
            if( errorJson.length() > 0 ) {return errorJson;}
            json="{\"balanceSummary\": [";
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            if( inheritanceFileId != null && inheritanceFileId.compareTo(-1L) ==0)
            {
              inheritanceFileId = null;    
            }
            
            Date toDate = new Date();
            Calendar fromCal=new GregorianCalendar();
            fromCal.add(Calendar.DATE, (trxDuration*-1) ); 
            Date fromDate = fromCal.getTime();
            if(statementDate != null )
            {
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                toDate = df.parse(statementDate);
                fromDate = new Date(toDate.getTime()- trxDuration * 24 * 3600 * 1000l ) ;
            }
            
            MinorBalanceSummaryView obj = UtilityService.getBalanceForMobile(personId, inheritanceFileId, fromDate, toDate);
            
            logger.logDebug( "Total Transactions:%s",
                                                        obj == null || obj.getTransactionDetails()== null ?0:obj.getTransactionDetails().size()
                           );
            boolean hasReason= true;
            json = json + jsonObj.toJson(obj)+",";
            
            if( hasReason  )
            {
                json = json.substring(0,json.length()-1)+"]}";
            }
            else
            {
                logger.logDebug("Not Found..");
                json = json +"]}";    
            }
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
                logger.logDebug("getBalanceSummary-Finish");
        }
        return json;
    }
    
    
    @GET
    @Path("/relatedPersonsForBalanceSummary")    
    @Produces("application/json;charset=utf-8")
    public String getRelatedPersonsForBalanceSummary(
                                                        @QueryParam("personId")        Long personId
                                                      )
    {
        String json="{\"relatedPersonsForBalanceSummary\": [";
        try
        {
                logger.logInfo("getRelatedPersonsForBalanceSummary|personId:%s",personId);
                //As Per AMAF the person can only view his balance details ONLY.
                List<BeneficiaryDetailsForMobile> list = InheritanceFileService.getRelatedPersonsOfPersonId(personId,null,false);
            
                Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();
                boolean hasItems= false;
                Map<Long,Long> personIdMap = new HashMap<Long,Long>();
            
                for(BeneficiaryDetailsForMobile obj : list)
                {
                    if(personIdMap.containsKey( obj.getPersonId() ) )continue;
                    personIdMap.put( obj.getPersonId() ,obj.getPersonId()  ) ;
                    BeneficiaryDetailsForMobile newObj  = new BeneficiaryDetailsForMobile();
                    
                    newObj.setPersonId( obj.getPersonId() );
                    newObj.setInheritanceBeneficiaryId(obj.getInheritanceBeneficiaryId() );
                    
                    newObj.setBeneficiaryNameAr( obj.getBeneficiaryNameAr() != null ?obj.getBeneficiaryNameAr():"");
                    newObj.setBeneficiaryName( obj.getBeneficiaryName() != null     ?obj.getBeneficiaryName():(obj.getBeneficiaryNameAr() != null ?obj.getBeneficiaryNameAr():""));
                    json = json + jsonObj.toJson(newObj)+",";
                    hasItems= true;
                }
                if(hasItems )
                {
                    json = json.substring(0,json.length()-1);
                }
                json=json +"]}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                    ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }
    
    @GET
    @Path("/registerUser")    
    @Produces("application/json;charset=utf-8")

    public String registerUser( @QueryParam("registrationData") String registrationData )
    {
        String json="";
        StringBuilder errorJson=null;
        try
        {
            Cryptography crypto = new com.emitac.Utils.Cryptography();
            String inputData = crypto.decrypt(registrationData);
            logger.logDebug("inputData:"+inputData);
            //<Emirates Id>;<Mobile Number>;<Email Address>:<Password>:<UserId>
            String[] arr = inputData.split(";");
            String emiratesId  = arr[0].trim().replace(" ", "");
            String mobile   =arr[1].trim().replace(" ", "");
            String emailAddress =arr[2].trim().replace(" ", "");
            String password =arr[3].trim().replace(" ", "");
            String userId =arr[4].trim().replace(" ", "");
            logger.logInfo("emiratesId:%s|mobile:%s|emailAddress:%s|password:%s|userId:%s",emiratesId,mobile,emailAddress,password,userId );
            List<Person> personPresentAgainstProvidedValues = new ArrayList<Person>();
            
            String errorInInputDetails = hasRegisterInputDetailsError( errorJson, 
                                                                       emiratesId, 
                                                                       mobile, 
                                                                       emailAddress, 
                                                                       password,
                                                                       userId,
                                                                       personPresentAgainstProvidedValues,
                                                                       false 
                                                                      );
            logger.logInfo("persons found:%s",
                            personPresentAgainstProvidedValues!= null? String.valueOf(  personPresentAgainstProvidedValues.size()):"0"
                          );
            if( errorInInputDetails != null  ) {return errorInInputDetails;}
            Person person = RegisterExternalUserService.persistExternalUserAsSecUser(userId, emailAddress,mobile, password, personPresentAgainstProvidedValues);
            return      "{\"result\": [{" +
                                        " \"userName\":\""+person.getFullName()+"\""+
                                        ",\"loginId\":\""+ person.getLoginId()+"\""+
                                        ",\"personId\":\""+ person.getPersonId()+"\""+
                                        ",\"pin\":\""+ crypto.encrpyt(  person.getPin()  ) +"\""+
                                        ",\"msgEn\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.userSuccessfullyRegisteredPINSent","en")+"\""+
                                        ",\"msgAr\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.userSuccessfullyRegisteredPINSent","ar")+"\"" +
                                     "}]}";
        }
        catch(Exception e)
        {
            logger.LogException("registerUser|Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                 ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }
    
    
    @POST
    @Path("/registerUserFromPortal")    
    @Produces("application/json;charset=utf-8")

    public String registerUserFromPortal(  String registrationData )
    {
        String json="";
        StringBuilder errorJson=null;
        try
        {
            Cryptography crypto = new com.emitac.Utils.Cryptography();
            String inputData = crypto.decrypt(registrationData);
            logger.logInfo("inputData:"+inputData);
            Gson jsonObj =  new GsonBuilder().create();
            RegisterUser inputObj = jsonObj.fromJson(inputData,RegisterUser.class);
//            logger.logInfo("registerUserFromPortal:%s|",inputObj.toString() );
            List<Person> personPresentAgainstProvidedValues = new ArrayList<Person>();
            String errorInInputDetails = hasRegisterInputDetailsError( errorJson, 
                                                                       inputObj.getUserDetails().getEmiratesId().trim().replace(" ", ""), 
                                                                       inputObj.getUserDetails().getCellNumber().trim().replace(" ", ""), 
                                                                       inputObj.getUserDetails().getEmail().trim().replace(" ", ""), 
                                                                       inputObj.getUserDetails().getPassword().trim().replace(" ", ""),
                                                                       inputObj.getUserDetails().getLoginId().trim().replace(" ", ""),
                                                                       personPresentAgainstProvidedValues ,
                                                                       true
                                                                      );
            if( errorInInputDetails != null  ) {return errorInInputDetails;}
            Person person = RegisterExternalUserService.persistExternalUserAsSecUser(inputObj, personPresentAgainstProvidedValues);
            {
                json = message+
                       msgCode+"\"0\","+
                       msgEn+ResourceWrapper.getInstance().getProperty("msg.success.userSuccessfullyRegisteredPINSent","en")+"\","+
                       msgAr+ResourceWrapper.getInstance().getProperty("msg.success.userSuccessfullyRegisteredPINSent","ar")+"\"}}";
                logger.logDebug("json:%s",json);
                
                return json;       
            }
            
        }
        catch(Exception e)
        {
            logger.LogException("registerUserFromPortal|Error occured", e);
            json="{\"message\": {\"msgCode\":\"000\",\"msgEn\":\"An error has occured please try again later\""+
                                                             ",\"msgAr\":\"An error has occured please try again later\"}}";
            
        }
        return json;
    }
    
    @GET
    @Path("/registerUserFromEservices")    
    @Produces("application/json;charset=utf-8")

    public String registerUserFromEservices( @QueryParam("registrationData") String registrationData )
    {
        String json="";
        StringBuilder errorJson=null;
        try
        {
            
            //TODO: Public Private Key cryptography requir
    //            Cryptography crypto = new com.emitac.Utils.Cryptography();
    //            String inputData = crypto.decrypt(registrationData);
            logger.logDebug("inputData:"+registrationData);
                //<Emirates Id>;<Mobile Number>;<Email Address>;<Password>;<UserId>
            String[] arr = registrationData.split(";");
            String emiratesId  = arr[0].trim().replace(" ", "");
            String mobile   =arr[1].trim().replace(" ", "");
            String emailAddress =arr[2].trim().replace(" ", "");
            String password =arr[3].trim().replace(" ", "");
            String userId =arr[4].trim().replace(" ", "");
            logger.logInfo("emiratesId:%s|mobile:%s|emailAddress:%s|password:%s|userId:%s",emiratesId,mobile,emailAddress,password,userId );
            List<Person> personPresentAgainstProvidedValues = new ArrayList<Person>();
            String errorInInputDetails = hasRegisterInputDetailsError( errorJson, emiratesId, mobile, emailAddress, password,userId,personPresentAgainstProvidedValues,false );
            if( errorInInputDetails != null  ) {return errorInInputDetails;}
            Person person = RegisterExternalUserService.persistExternalUserAsSecUser(userId, emailAddress,mobile, password, personPresentAgainstProvidedValues);
            return      "{\"result\": [{" +
                                        " \"userName\":\""+person.getFullName()+"\""+
                                        ",\"loginId\":\""+ person.getLoginId()+"\""+
                                        ",\"personId\":\""+ person.getPersonId()+"\""+
    //                                        ",\"pin\":\""+ person.getPin()   +"\""+
                                        ",\"msgEn\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.userSuccessfullyRegisteredPINSent","en")+"\""+
                                        ",\"msgAr\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.userSuccessfullyRegisteredPINSent","ar")+"\"" +
                                     "}]}";
        }
        catch(Exception e)
        {
            logger.LogException("registerUserFromEservices|Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                 ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }

    private StringBuilder buildErrorJson(
                                                        StringBuilder errorJson,
                                                        boolean isCalledFromPortal,
                                                        String messageCodeKey,
                                                        String messageKey,
                                                        Object... params
                                        )throws Exception
    {
        if(isCalledFromPortal){
            if (errorJson ==null ) errorJson=new StringBuilder(message);
            errorJson=errorJson.append( msgCode ).append(messageCodeKey).append(",");
            errorJson=errorJson.append( msgEn ).append( ResourceWrapper.getInstance().getProperty(messageKey,"en",params)).append("\",");
            errorJson=errorJson.append( msgAr ).append(ResourceWrapper.getInstance().getProperty(messageKey,"ar",params)).append("\"");
            errorJson = errorJson.append("}}");
            return errorJson;
        }
        else{
        if (errorJson ==null ) errorJson=new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( messageCodeKey).append(",");
            errorJson=errorJson.append( errorMsgEn ).append( ResourceWrapper.getInstance().getProperty(messageKey,"en",params)).append("\",");
            errorJson=errorJson.append( errorMsgAr ).append( ResourceWrapper.getInstance().getProperty( messageKey,"ar",params)).append("\"");
            errorJson = errorJson.append("}]}");
            return errorJson;
        }
    }

    private String hasRegisterInputDetailsError(
                                                    StringBuilder errorJson, 
                                                    String emiratesId,
                                                    String mobileNumber,
                                                    String emailAddress, 
                                                    String password,
                                                    String userId,
                                                    List<Person> personPresentAgainstProvidedValues ,
                                                    Boolean isCalledFromPortal
                                                )  throws Exception
    {
        //Emirate Id is not provided        
        if( emiratesId == null || emiratesId.trim().length() <=0 ){
            errorJson = buildErrorJson(errorJson, isCalledFromPortal,Constant.MobileWebServiceErrorCodes.REGISTER_USER_EMIRATES_ID_NOT_SPECIFIED,Constant.MobileWebServiceErrorCodes.REGISTER_USER_EMIRATES_ID_NOT_SPECIFIED);
            return errorJson.toString();
        }
        //Password is not provided
        else if( password == null || password.trim().length()<= 0 ){
            errorJson = buildErrorJson(errorJson, isCalledFromPortal,Constant.MobileWebServiceErrorCodes.REGISTER_USER_PASSWORD_NOT_SPECIFIED,Constant.MobileWebServiceErrorCodes.REGISTER_USER_PASSWORD_NOT_SPECIFIED);
            return errorJson.toString();
        }
        //Mobile is not provided
        else if( mobileNumber == null || mobileNumber.trim().length()<= 0 ){
            errorJson = buildErrorJson(errorJson, isCalledFromPortal,Constant.MobileWebServiceErrorCodes.REGISTER_USER_MOBILE_NOT_SPECIFIED,Constant.MobileWebServiceErrorCodes.REGISTER_USER_MOBILE_NOT_SPECIFIED);
            return errorJson.toString();
        }
        //If all the required field are provided, Validating following business rules
        //1- Emirates Id present in System(This will not be checked for the Portal user).
        //2- Login Id is already registered with another user.
        //3- Person against the provided emirates id already registered with another login id.
        else{
            List<Person> personList = new ArrayList<Person>();
            //Checking for emirates Id availability--Start
                //For portal user emirates id presense will not be checked, as its possible that user is job seeker who is not regitered or present in pims 
                if( !isCalledFromPortal )
                {
                       personList.addAll(RegisterExternalUserService.getPersonsForEmirateId(emiratesId));                                                                            
                       if (personList == null || personList.size() <= 0){
                         logger.logInfo("Validation-Error|Person Not Present for emirates id:%s", emiratesId);
                         errorJson = buildErrorJson(errorJson, false,Constant.MobileWebServiceErrorCodes.REGISTER_USER_PERSON_NOT_AVAILABLE_FOR_EMIRATE_ID,Constant.MobileWebServiceErrorCodes.REGISTER_USER_PERSON_NOT_AVAILABLE_FOR_EMIRATE_ID);
                         return errorJson.toString();
                        }     
                
                }
            //Checking for emirates Id availability-Finish
            //Checking for Person Present against the Login Id -Start  
                String personForLoginIdPresent  = getPersonFromLoginId(userId);
                if( personForLoginIdPresent != null && personForLoginIdPresent.length() > 0 )
                {
                      logger.logInfo("Validation-Error|Person already Present for login id:%s", userId);
                       errorJson = buildErrorJson(errorJson, false,Constant.MobileWebServiceErrorCodes.REGISTER_USER_PERSON_ALREADY_AVAILABLE_FOR_LOGIN_ID,
                                                    Constant.MobileWebServiceErrorCodes.REGISTER_USER_PERSON_ALREADY_AVAILABLE_FOR_LOGIN_ID,
                                                  userId );
                       return errorJson.toString();
                }
                //Checking for Person Present against the Login Id -Finish
                //Checking for Login Id already Generated for personId Start
                String alreadyGeneratedLoginId =RegisterExternalUserService.hasLoginIdGeneratedForPersons(personList);
                if (alreadyGeneratedLoginId != null && alreadyGeneratedLoginId.length() > 0) 
                {
                    logger.logInfo("Validation-Error|Person with emirate Id:%s, already has login id:%s", emiratesId, alreadyGeneratedLoginId);
                    errorJson = buildErrorJson(errorJson, false,Constant.MobileWebServiceErrorCodes.REGISTER_USER_PERSON_WITH_EMIRATEID_ALREADY_REGISTERED_WITH_ANOTHER_LOGIN_ID,
                                               Constant.MobileWebServiceErrorCodes.REGISTER_USER_PERSON_WITH_EMIRATEID_ALREADY_REGISTERED_WITH_ANOTHER_LOGIN_ID  );
                    return errorJson.toString();
                }
                //Checking for Login Id already Generated for personId Finish
                personPresentAgainstProvidedValues.addAll(personList);
        }
        return null;    
    }

    
    
    @GET
    @Path("/activateUser")    
    @Produces("application/json;charset=utf-8")
    public String activateUser(             
                                @QueryParam("loginId")       String loginId,
                                @QueryParam("emiratesId")       String emiratesId,
                                @QueryParam("pin")       String pin
                              )
    {
        String json="";
        try
        {
            logger.logInfo( "activateUser|loginId:%s|pin:%s|emiratesId:%s",    loginId,    pin, emiratesId );
            PersonView person  = UtilityService.getPersonInformation(loginId, emiratesId);
            String storedPIN =  person.getPin() ;
            if ( !storedPIN.equals( pin ) )
            {
                return "{\"errors\": [{" +
                    "                       \"errorCode\":\"1309\"" +
                                          ",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_INVALID_PIN,"en")+"\""+
                                            ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_INVALID_PIN,"ar")+"\"" +
                                "  }]}";
            }
            else
            {
                RegisterExternalUserService.activateUser(   
                                                           person.getLoginId(), 
                                                            person.getPersonId() 
                                                        );
                return      "{\"result\": [{" +
                                            "\"msgEn\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.userActivated","en")+"\""+
                                            ",\"msgAr\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.userActivated","ar")+"\"" +
                                         "}]}";
            }
          
        }
        
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        return json;
        
    }
    @GET
    @Path("/activateUserFromPortal")    
    @Produces("application/json;charset=utf-8")
    public String activateUserFromPortal(             
                                            @QueryParam("loginId")       String loginId,
                                            @QueryParam("emiratesId")       String emiratesId,
                                            @QueryParam("pin")       String pin
                                        )
    {
        String json="";
        try
        {
            logger.logInfo( "activateUserFromPortal|loginId:%s|pin:%s|emiratesId:%s",    loginId,    pin, emiratesId );
            PersonView person  = UtilityService.getPersonInformation(loginId, emiratesId);
            String storedPIN =  person.getPin() ;
            StringBuilder returnJson = new StringBuilder(message);
            if ( !storedPIN.equals( pin ) )
            {
                
                returnJson=returnJson.append( msgCode ).append("1309").append(",");
                returnJson=returnJson.append( msgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_INVALID_PIN,"en")
                                                                ).
                                                                append("\",");
                returnJson=returnJson.append( msgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_INVALID_PIN,"ar")
                                                               ).
                                                               append("\"");
                
                returnJson = returnJson.append("}");
                return returnJson.toString();
            }
            else
            {
                RegisterExternalUserService.activateUser(   
                                                           person.getLoginId(), 
                                                            person.getPersonId() 
                                                        );
                
                returnJson=returnJson.append( msgCode ).append("0").append(",");
                returnJson=returnJson.append( msgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty("msg.success.userActivated","en")
                                                                ).
                                                                append("\",");
                returnJson=returnJson.append( msgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty("msg.success.userActivated","ar")
                                                               ).
                                                               append("\"");
                
                returnJson = returnJson.append("}}");
                
                logger.logDebug("activateUserFromPortal|", returnJson.toString());
                json = returnJson.toString();
                
            }
          
        }
        
        catch(Exception e)
        {
            logger.LogException("Error occured", e);

            json="{\"message\": {\"msgCode\":\"000\",\"msgEn\":\"An error has occured please try again later\""+
                                                                 ",\"msgAr\":\"An error has occured please try again later\"}}";

                                                                       
        }
        return json;
        
    }
    
    
     
    @GET
    @Path("/regenerateNewPINToActivateLogin")    
    @Produces("application/json;charset=utf-8")
    public String regenerateNewPINToActivateLogin( @QueryParam("loginId") String loginId,@QueryParam("emiratesId") String emiratesId )
    {
        String json=null;
        try
        {
                logger.logInfo("regenerateNewPINToActivateLogin|loginId:%s|emiratesId:%s",loginId,emiratesId);
                List<Person> personList = null;
                if ( ( loginId == null || loginId.length()<=0 || loginId.equals("-1")) && 
                      (emiratesId != null && emiratesId.length()>0) 
                   )
                {
                    personList = RegisterExternalUserService.getPersonsForEmirateId(emiratesId);    
                }
                else if ( loginId != null && loginId.length() > 0 && !loginId.equals("-1"))  
                { 
                    personList = PropertyService.getLoginPersonListByLoginId(loginId);
                }
                if( personList==null || personList.size() <=0 )
                {
                    if(  loginId != null && loginId.length() > 0 )
                    {
                        return "{\"errors\": ["+errorCode+ Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_LOGIN_ID+","+
                                           errorMsgEn+ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_LOGIN_ID,"en")+ "\","+
                                           errorMsgAr+ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_LOGIN_ID,"ar")+ "\""+
                                "}]}";
                    }
                    else if(  emiratesId != null && emiratesId.length() > 0 )
                    {
                        return "{\"errors\": ["+errorCode+ Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_EMIRATES_ID+","+
                                           errorMsgEn+ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_EMIRATES_ID,"en")+ "\","+
                                           errorMsgAr+ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_EMIRATES_ID,"ar")+ "\""+
                                "}]}";    
                            
                    }
                }
                else
                {
                    //In case, when user provides emirates id then we need to explicitly take loginId from person
                    if  ( loginId == null || loginId.length()<=0 || loginId.equals("-1") )
                    {loginId = personList.get(0).getLoginId();}
                    
//                    UserDbImpl user  = (UserDbImpl) com.avanza.core.security.SecurityManager.getUser( loginId );
                    UserDetails userDetail = UtilityService.getUserDetailsFromLoginId(loginId);
//                    if( user.getStatusId() != null && user.getStatusId().compareTo("1") == 0  )
                    if( userDetail.getStatusId() != null && userDetail.getStatusId().compareTo("1") == 0  )
                    {
                        return "{\"errors\": ["+errorCode+ Constant.MobileWebServiceErrorCodes.REGISTER_USER_LOGIN_ID_ALREADY_ACTIVATED+","+
                                               errorMsgEn+ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_LOGIN_ID_ALREADY_ACTIVATED,"en")+ "\","+
                                               errorMsgAr+ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_LOGIN_ID_ALREADY_ACTIVATED,"ar")+ "\""+
                                "}]}";
                    }
                    else
                    {
                        RegisterExternalUserService.generateAndSendPIN( personList.get( 0 ) );
                        return      "{\"result\": [{"+
                                        "\"loginId\":\""+ userDetail.getLoginId()+"\","+
                                        "\"msgEn\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.pinResent","en")+"\","+
                                        "\"msgAr\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.pinResent","ar")+"\"" +
                                    "}]}";
                    }
                }
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                    ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }
    
    @GET
    @Path("/getPINForLoginId")    
    @Produces("application/json;charset=utf-8")
    public String getPINForLoginId( @QueryParam("loginId") String loginId )
    {
        String json=null;
        try
        {
                logger.logInfo("getPINForLoginId|loginId:%s",loginId);
//                PersonView person= UtilityService.getPersonInformation(loginId,"-1");
                UserDetails userDetail = (UserDetails) UtilityService.getUserDetailsFromLoginId( loginId );
                String dbPassword = UserDbImpl.getCryptographer().decrypt(userDetail.getPassword());
                if( userDetail==null || userDetail.getLoginId() == null )
                {
                    return "{\"errors\": ["+errorCode+ Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_LOGIN_ID+","+
                                           errorMsgEn+ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_LOGIN_ID,"en")+ "\","+
                                           errorMsgAr+ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_LOGIN_ID,"ar")+ "\""+
                            "}]}";
                    
                }
                else
                {
                        
                        return      "{\"pin\":\""+dbPassword+"\"}";

                }
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                    ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }
    
    private String hasChangePasswordError(
                                                    StringBuilder errorJson, 
                                                    String loginId,
                                                    String oldPassword,
                                                    String password,
                                                    boolean fromPortal
                                                )  throws Exception
    {
        //Login Id is not provided        
        List<Person> personList = PropertyService.getLoginPersonListByLoginId(loginId);
        if( personList == null || personList.size() <=0 )
        {
            if(!fromPortal){
            if (errorJson ==null ) errorJson=new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_LOGIN_ID).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_LOGIN_ID,"en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_LOGIN_ID,"ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("}]}");
            return errorJson.toString();
            }else
            {
                            
                                if (errorJson ==null ) errorJson=new StringBuilder(message);
                                errorJson=errorJson.append( msgCode ).append( Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_LOGIN_ID ).append(",");
                                errorJson=errorJson.append( msgEn ).append(
                                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_LOGIN_ID,"en")
                                                                                ).
                                                                                append("\",");
                                errorJson=errorJson.append( msgAr ).append(
                                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_LOGIN_ID,"ar")
                                                                               ).
                                                                               append("\"");
                                
                                errorJson = errorJson.append("}}");
                                return errorJson.toString();
            }
        }
        
        
        return errorJson != null ?errorJson.toString():null;
    }
    
    @GET
    @Path("/forgetPassword")    
    @Produces("application/json;charset=utf-8")
    public String forgetPassword( @QueryParam("loginId") String loginId,@QueryParam("emiratesId") String emiratesId )
    {
        String json=null;
        try
        {
                logger.logInfo("forgetPassword|loginId:%s|emiratesId:%s",loginId,emiratesId);
                List<Person> personList = null;
                if ( ( loginId == null || loginId.length()<=0 ||loginId.equals("-1")) && 
                      (emiratesId != null && emiratesId.length()>0) 
                   )
                {
                    personList = RegisterExternalUserService.getPersonsForEmirateId(emiratesId);    
                    
                }
                else if ( loginId != null && loginId.length() > 0 && !loginId.equals("-1"))  
                { 
                    personList = PropertyService.getLoginPersonListByLoginId(loginId);
                }
                if( personList==null || personList.size() <=0 )
                {
                    if(  loginId != null && loginId.length() > 0 )
                    {
                        return "{\"errors\": ["+errorCode+ Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_LOGIN_ID+","+
                                           errorMsgEn+ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_LOGIN_ID,"en")+ "\","+
                                           errorMsgAr+ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_LOGIN_ID,"ar")+ "\""+
                                "}]}";
                    }
                    else if(  emiratesId != null && emiratesId.length() > 0 )
                    {
                        return "{\"errors\": ["+errorCode+ Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_EMIRATES_ID+","+
                                           errorMsgEn+ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_EMIRATES_ID,"en")+ "\","+
                                           errorMsgAr+ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_EMIRATES_ID,"ar")+ "\""+
                                "}]}";    
                            
                    }
                    
                }
                else
                {
                    Person person = personList.get( 0 ) ;
                    RegisterExternalUserService.generateDummyPasswordAndSend( person );
                    return      "{\"result\": [{"+
                                        "\"loginId\":\""+ person.getLoginId()+"\","+
                                        "\"msgEn\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.forgetPassword","en")+"\","+
                                        "\"msgAr\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.forgetPassword","ar")+"\"" +
                                 "}]}";
                    
                }
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                    ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }
    
    @GET
    @Path("/forgetPasswordFromPortal")    
    @Produces("application/json;charset=utf-8")
    public String forgetPasswordFromPortal( @QueryParam("loginId") String loginId,@QueryParam("emiratesId") String emiratesId )
    {
        String json=null;
        try
        {
            
                
                logger.logInfo("forgetPasswordFromPortal|loginId:%s|emiratesId:%s",loginId,emiratesId);
                StringBuilder errorJson  =null; 
                List<Person> personList = null;
                if ( ( loginId == null || loginId.length()<=0 ||loginId.equals("-1")) && 
                      (emiratesId != null && emiratesId.length()>0) 
                   )
                {
                    personList = RegisterExternalUserService.getPersonsForEmirateId(emiratesId);    
                    
                }
                else if ( loginId != null && loginId.length() > 0 && !loginId.equals("-1"))  
                { 
                    personList = PropertyService.getLoginPersonListByLoginId(loginId);
                }
                if( personList==null || personList.size() <=0 )
                {
                    if(  loginId != null && loginId.length() > 0 )
                    {
                        if (errorJson ==null ) errorJson=new StringBuilder(message);
                        errorJson=errorJson.append( msgCode ).append( Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_LOGIN_ID ).append(",");
                        errorJson=errorJson.append( msgEn ).append(
                                                                         ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_LOGIN_ID,"en")
                                                                        ).
                                                                        append("\",");
                        errorJson=errorJson.append( msgAr ).append(
                                                                         ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_LOGIN_ID,"ar")
                                                                       ).
                                                                       append("\"");
                        
                        errorJson = errorJson.append("}}");
                        return errorJson.toString();
                    }
                    else if(  emiratesId != null && emiratesId.length() > 0 )
                    {
                        if (errorJson ==null ) errorJson=new StringBuilder(message);
                        errorJson=errorJson.append( msgCode ).append( Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_EMIRATES_ID ).append(",");
                        errorJson=errorJson.append( msgEn ).append(
                                                                         ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_EMIRATES_ID,"en")
                                                                        ).
                                                                        append("\",");
                        errorJson=errorJson.append( msgAr ).append(
                                                                         ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.REGISTER_USER_NO_PERSON_ASSOCIATED_WITH_EMIRATES_ID,"ar")
                                                                       ).
                                                                       append("\"");
                        
                        errorJson = errorJson.append("}}");
                        return errorJson.toString();
                    }
                    
                }
                else
                {
                    Person person = personList.get( 0 ) ;
                    RegisterExternalUserService.generateDummyPasswordAndSend( person );
                    return      "{\"message\": [{"+
                                        "\"msgCode\":\"1\","+
                                        "\"msgEn\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.forgetPassword","en")+"\","+
                                        "\"msgAr\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.forgetPassword","ar")+"\"" +
                                 "}]}";
                    
                }
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"message\": {\"msgCode\":\"000\",\"msgEn\":\"An error has occured please try again later\""+
                                                             ",\"msgAr\":\"An error has occured please try again later\"}}";
        }
        return json;
    }
    
    @GET
    @Path("/changePassword")    
    @Produces("application/json;charset=utf-8")
    public String changePassword( @QueryParam("inputData") String inputData )
    {
        String json="";
        StringBuilder errorJson=null;
        try
        {
            logger.logDebug("changePassword:inputData Encrypted:", inputData);
            Cryptography crypto = new com.emitac.Utils.Cryptography();
            inputData = crypto.decrypt(inputData);
//            logger.logDebug("changePassword:inputData Decrypted:", inputData);
            return changePasswordProcessing(inputData,false);
            
            
        }
        catch(Exception e)
        {
            logger.LogException("changePassword|Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                 ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }

    @GET
    @Path("/changePasswordFromPortal")    
    @Produces("application/json;charset=utf-8")
    public String changePasswordFromPortal( @QueryParam("inputData") String inputData )
    {
        String json="";
        StringBuilder errorJson=null;
        try
        {
          Cryptography crypto = new com.emitac.Utils.Cryptography();
          inputData = crypto.decrypt(inputData);
          return  changePasswordProcessing(inputData,true);
        }
        catch(Exception e)
        {
            logger.LogException("changePasswordFromPortal|Error occured", e);
            json="{\"message\": {\"msgCode\":\"000\",\"msgEn\":\"An error has occured please try again later\""+
                                                             ",\"msgAr\":\"An error has occured please try again later\"}}";
        }
        return json;
    }


    private String changePasswordProcessing(String inputData,boolean fromPortal)throws Exception
    {
        //<Login Id>;<Old Password>;<newPassword>
        
        String[] arr = inputData.split(";");
        String loginId  = arr[0].trim().replace(" ", "");;
        String oldPassword   =arr[1];
        String password =arr[2];
        logger.logDebug("changePassword|loginId:%s",loginId);
        StringBuilder errorJson=null;
        String errorInInputDetails = hasChangePasswordError( errorJson, loginId, oldPassword, password,fromPortal );
        if( errorInInputDetails != null  ) {return errorInInputDetails;}
        //            UserDbImpl user = (UserDbImpl) com.avanza.core.security.SecurityManager.getUser( loginId );
        UserDetails userDetail = (UserDetails) UtilityService.getUserDetailsFromLoginId( loginId );
        
        //          String dbPassword = UserDbImpl.getCryptographer().decrypt(user.getPassword());
        String dbPassword = UserDbImpl.getCryptographer().decrypt(userDetail.getPassword());
        
        if( !dbPassword.equals( oldPassword ) )
        {
            if(!fromPortal){
                if (errorJson ==null ) errorJson=new StringBuilder("{\"errors\": [");
                errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.CHANGE_PASSWORD_PASSWORD_MISMATCH).append(",");
                errorJson=errorJson.append( errorMsgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.CHANGE_PASSWORD_PASSWORD_MISMATCH,"en")
                                                                ).
                                                                append("\",");
                errorJson=errorJson.append( errorMsgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.CHANGE_PASSWORD_PASSWORD_MISMATCH,"ar")
                                                               ).
                                                               append("\"");
                
                errorJson = errorJson.append("}]}");
                return errorJson.toString();    
            }
            else{
                
                    if (errorJson ==null ) errorJson=new StringBuilder(message);
                    errorJson=errorJson.append( msgCode ).append( Constant.MobileWebServiceErrorCodes.CHANGE_PASSWORD_PASSWORD_MISMATCH ).append(",");
                    errorJson=errorJson.append( msgEn ).append(
                                                                     ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.CHANGE_PASSWORD_PASSWORD_MISMATCH,"en")
                                                                    ).
                                                                    append("\",");
                    errorJson=errorJson.append( msgAr ).append(
                                                                     ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.CHANGE_PASSWORD_PASSWORD_MISMATCH,"ar")
                                                                   ).
                                                                   append("\"");
                    
                    errorJson = errorJson.append("}}");
                    return errorJson.toString();
            }
        }
        UserDbImpl user = (UserDbImpl) com.avanza.core.security.SecurityManager.getUser( loginId );
        user.setPassword( UserDbImpl.getCryptographer().encrypt( password ) );  
        
        com.avanza.core.security.SecurityManager.persistUser(user);
        if(!fromPortal){
        return      "{\"result\": [{" +
                                    "\"msgEn\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.passwordChanged","en")+"\""+
                                    ",\"msgAr\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.passwordChanged","ar")+"\"" +
                                 "}]}";
        }
        else{
            return      "{\"message\": [{"+
                                "\"msgCode\":\"1\","+
                                "\"msgEn\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.passwordChanged","en")+"\","+
                                "\"msgAr\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.passwordChanged","ar")+"\"" +
                         "}]}";
        }
    }


    @GET
    @Path("/appointmentList")    
    @Produces("application/json;charset=utf-8")
    @SuppressWarnings("unchecked")
    public String getAppointmentByLoginId(
                                            @QueryParam("loginId")       String loginId, 
                                            @QueryParam("filters")       String filters, 
                                            @QueryParam("recordsPerPage") String recordsPerPage,
                                            @QueryParam("currentPage")    String currentPage,
                                            @QueryParam("orderBy")        String orderBy, 
                                            @QueryParam("ascending")      String ascending 
                                         )
    {
        String json="{\"appointments\": [";
        try
        {
    //      
            logger.logInfo("getAppointmentByLoginId|loginId:%s|filters:%s|recordsPerPage:%s|currentPage:%s|orderBy:%s|ascending:%s", loginId,filters,recordsPerPage,currentPage,orderBy,ascending);
       
            String errorJson = hasAppointmentsListByLoginIdInputParamErrors( loginId, filters, recordsPerPage, currentPage, orderBy, ascending );
            
            if( errorJson.length() > 0 ){logger.logInfo("hasError:%s", errorJson);return errorJson;}
            AppointmentService service = new AppointmentService();
            Map<Object,Object> searchCriteriaMap = new HashMap<Object,Object>();
            searchCriteriaMap.put(Constant.AppointmentCriteria.CREATED_BY,loginId );
            
            List<AppointmentDetails> list = service.getAppointmentList(
                                                                        searchCriteriaMap,
                                                                        recordsPerPage != null && recordsPerPage.length()>0? Integer.valueOf(recordsPerPage) :null,
                                                                        currentPage!= null && currentPage.length()>0 ?Integer.valueOf(currentPage):null  ,
                                                                        (orderBy != null && orderBy.trim().length()>0 &&!orderBy.equals("-1")) ? orderBy:"appointmentDate",
                                                                        (ascending != null && ascending.equals("1")) ? true:false
                                                                     );
            boolean hasItems= false;
            DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            HashMap ddMap = new UtilityManager().getDomainDataMap();
            for (AppointmentDetails obj : list) 
            {
//                logger.logInfo("getAppointmentByLoginId|Iterating appointment id:%s",obj.getAppointmentId());
                obj.setRequestNumber(obj.getRequest().getRequestNumber());
                
                if( obj.getRequest().getStatusId().compareTo(Constant.REQUEST_STATUS_NEW_ID) == 0  ) 
                {
                  obj.setIsEditable("1");    
                }
                else
                {
                    obj.setIsEditable("0");        
                }
                if( ddMap.containsKey(  obj.getRequest().getStatusId() ) )
                {
                    DomainData dd =  (DomainData)ddMap.get(  obj.getRequest().getStatusId() ) ;
                    obj.setStatusEn( dd.getDataDescEn() );
                    obj.setStatusAr( dd.getDataDescAr() );
                }
                if( obj.getRequest().getInheritanceFile() != null && obj.getRequest().getInheritanceFile().getInheritanceFileId() != null )
                {
                    obj.setFileNumber( obj.getRequest().getInheritanceFile().getFileNumber() );
                
                    obj.setInheritanceFileId( obj.getRequest().getInheritanceFile().getInheritanceFileId() );
                }
                else
                {
                        obj.setFileNumber( "");
                        
//                        obj.setInheritanceFileId( "" );
                    }
                if( obj.getAppointmentWith() != null )
                {
                    obj.setAppointmentWithUserId( obj.getAppointmentWith().getLoginId() );
                    obj.setAppointmentWithUserName( obj.getAppointmentWith().getFullName() );
                    obj.setAppointmentWithUserNameAr( obj.getAppointmentWith().getSecondaryFullName() );
                }
                if( obj.getContactEmail() == null )
                {
                    obj.setContactEmail("");
                }
                if( obj.getContactNumber() == null )
                {
                    obj.setContactNumber("");
                }
                if( obj.getAppointmentWithComments() == null )
                {
                    obj.setAppointmentWithComments("");
                }
                json = json + jsonObj.toJson(obj)+",";
                hasItems= true;
            }
            if(hasItems )
            {
                json = json.substring(0,json.length()-1);
            }
            json=json +"]}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                 ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }
    
    private String hasAppointmentsListByLoginIdInputParamErrors  (
                                                                    String loginId,
                                                                    String filters,
                                                                    String recordsPerPage,
                                                                    String currentPage,
                                                                    String orderBy, 
                                                                    String ascending 
                                                                  )throws Exception
    {
        StringBuilder errorJson= new StringBuilder("{\"errors\": [");
        boolean hasError = false;
        if( loginId == null )
        {
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.LOGIN_ID_REQUIRED).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1103","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1103","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
    //        StringBuilder pagingErrorJson = hasPagingRelatedErrors(recordsPerPage, currentPage);
        if( orderBy != null && orderBy.trim().length() > 0 && !orderBy.equals("-1"))
        {
           if( !orderBy.equals("appointmentDate") &&
               !orderBy.equals("createdOn") 
             )    
           {
            
    //                errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.INCORRECT_ORDER_BY).append("},");
                errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.INCORRECT_ORDER_BY).append(",");
                errorJson=errorJson.append( errorMsgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")
                                                                ).
                                                                append("\",");
                errorJson=errorJson.append( errorMsgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")
                                                               ).
                                                               append("\"");
                errorJson = errorJson.append("},");    
                hasError = true;
            }
            
        }
    //        if( errorJson != null && errorJson.length() > 0  )
    //        {
    //    //            errorJson.append( pagingErrorJson ) ;
    //            hasError = true;
    //        }
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
    }
    

    private String hasErrorAppointmentRequest(AppointmentInputParameter inputObj )throws Exception
    {
        StringBuilder errorJson = null;
        boolean hasError = false;
        //Appointment created by not provided
        if( 
            inputObj.getCreatedBy() == null || 
            inputObj.getCreatedBy().trim().length() <= 0 
          )
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.APPOINTMENT_CREATED_BY_USER_REQUIRED ).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.APPOINTMENT_CREATED_BY_USER_REQUIRED , "en" )
                                                           ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.APPOINTMENT_CREATED_BY_USER_REQUIRED , "ar" )
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        
        //Appointment with user not provided
        if( 
            inputObj.getAppointmentWith() == null || 
            inputObj.getAppointmentWith().trim().length() <= 0 
          )
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.APPOINTMENT_WITH_PERSON_REQUIRED ).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.APPOINTMENT_WITH_PERSON_REQUIRED , "en" )
                                                           ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.APPOINTMENT_WITH_PERSON_REQUIRED , "ar" )
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        //Appointment with user id not present in system (SEC_USER)
        else
        {
            
            if( inputObj.getAppointmentWithUser() == null )
            {
                if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
                errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.APPOINTMENT_WITH_PERSON_INVALID).append( "," );
                errorJson=errorJson.append( errorMsgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.APPOINTMENT_WITH_PERSON_INVALID , "en" )
                                                               ).
                                                                append("\",");
                errorJson=errorJson.append( errorMsgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.APPOINTMENT_WITH_PERSON_INVALID , "ar" )
                                                               ).
                                                               append("\"");
                
                errorJson = errorJson.append("},");
                hasError = true;
            }
        }
        //Appointment Start Date not provided
        if( 
            inputObj.getAppointmentStartDate() == null || 
            inputObj.getAppointmentStartDate().trim().length() <= 0 
          )
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.APPOINTMENT_START_DATE_REQUIRED ).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.APPOINTMENT_START_DATE_REQUIRED, "en" )
                                                           ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.APPOINTMENT_START_DATE_REQUIRED, "ar" )
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        //Appointment Start Date is not in correct format (dd/MM/yyyy hh:mm)
        else
        {
            try
            {
              inputObj.getAppStartDate();
            }
            catch(Exception e)
            {
                    if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
                    errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.APPOINTMENT_START_DATE_INVALID ).append( "," );
                    errorJson=errorJson.append( errorMsgEn ).append(
                                                                     ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.APPOINTMENT_START_DATE_INVALID, "en" )
                                                                   ).
                                                                    append("\",");
                    errorJson=errorJson.append( errorMsgAr ).append(
                                                                     ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.APPOINTMENT_START_DATE_INVALID, "ar" )
                                                                   ).
                                                                   append("\"");
                    
                    errorJson = errorJson.append("},");
                    hasError = true;
            }
        }
        //Appointment Duration (in hours) not provided
        if( 
            inputObj.getDuration() == null || inputObj.getDuration().compareTo(0)<=0
          )
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.APPOINTMENT_DURATION_REQUIRED ).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.APPOINTMENT_DURATION_REQUIRED, "en" )
                                                           ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.APPOINTMENT_DURATION_REQUIRED, "ar" )
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        //Appointment Title required
        if( 
            inputObj.getAppointmentTitle() == null || inputObj.getAppointmentTitle().length() <= 0
          )
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.APPOINTMENT_TITLE_REQUIRED ).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.APPOINTMENT_TITLE_REQUIRED, "en" )
                                                           ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.APPOINTMENT_TITLE_REQUIRED, "ar" )
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        //Appointment Description required
        if( 
            inputObj.getDescription() == null || inputObj.getDescription().length() <= 0
          )
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.APPOINTMENT_DESCRIPTION_REQUIRED ).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.APPOINTMENT_DESCRIPTION_REQUIRED, "en" )
                                                           ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.APPOINTMENT_DESCRIPTION_REQUIRED, "ar" )
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
    }
        
    @POST
    @Path("/persistAppointment")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String persistAppointment(String inputParameters)
    {
        String json="";
        try
        {
            Gson jsonObj =  new GsonBuilder().create();
            AppointmentInputParameter inputObj = jsonObj.fromJson(inputParameters,AppointmentInputParameter.class);
            
            String validationResult = hasErrorAppointmentRequest( inputObj );
            if(validationResult != null && validationResult.trim().length()>0) return validationResult;
            
            AppointmentDetails appointment = AppointmentService.persistAppointments(inputObj);
            json=
            "{\"result\": [{" +
                                                " \"requestNumber\":\""+appointment.getRequest().getRequestNumber()+"\""+
                                                ",\"requestId\":\""+ appointment.getRequest().getRequestId()+"\""+
                                                ",\"appointmentId\":\""+ appointment.getAppointmentId()+"\""+
                                                ",\"startDateTime\":\""+ appointment.getAppointmentDate()+"\""+
                                                ",\"completeDateTime\":\""+ appointment.getAppointmentDateComplete()+"\""+
                                                ",\"msgEn\":\""+ MessageFormat.format(ResourceWrapper.getInstance().getProperty("msg.success.appointmentRequestSaved","en"),appointment.getRequest().getRequestNumber())+"\""+
                                                ",\"msgAr\":\""+ MessageFormat.format(ResourceWrapper.getInstance().getProperty("msg.success.appointmentRequestSaved","ar"),appointment.getRequest().getRequestNumber())+"\"" +
                                             "}]}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("persistAppointment-Finish");
        }
        return json;
    }
    

    @GET
    @Path("/deleteAppointment")    
    @Produces("application/json;charset=utf-8")
    @SuppressWarnings("unchecked")
    public String deleteAppointment(
                                     @QueryParam("appointmentId") Long appointmentId,
                                     @QueryParam("loginId") String loginId
                                   )
    {
        String json;
        try
        {
            AppointmentDetails appointment = AppointmentService.deleteAppointments(appointmentId, loginId);
            json =   "{\"deleteAppointment\": [{" +
                                                "\"msgEn\":\""+ MessageFormat.format(ResourceWrapper.getInstance().getProperty("msg.success.appointmentDeleted","en"),appointment.getRequest().getRequestNumber())+"\""+
                                                ",\"msgAr\":\""+ MessageFormat.format(ResourceWrapper.getInstance().getProperty("msg.success.appointmentDeleted","ar"),appointment.getRequest().getRequestNumber())+"\"" +
                                             "}]}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        
        return json;
    }
    

    @GET
    @Path("/relatedAmafEmployeeForLoginId")    
    @Produces("application/json;charset=utf-8")
    @SuppressWarnings("unchecked")
    public  String relatedAmafEmployeeForLoginId(@QueryParam("loginId") String loginId )throws Exception
    {
         String json="{\"relatedAmafEmployees\": [";
         boolean hasItems = false;
         try
         {
            Map<Object,Object> searchCriteriaMap = new HashMap<Object,Object>();
            searchCriteriaMap.put("loginId",loginId.trim());
            List<FileDetailsByPersonId> list = InheritanceFileService.getInheritanceFileDetailByLoginId(
                                                                                                            searchCriteriaMap,
                                                                                                            null,
                                                                                                            null  ,
                                                                                                            "fileNumber",
                                                                                                            true
                                                                                                       );            
             logger.logInfo("relatedAmafEmployeeForLoginId|%s records found",list.size());
            Map<Long,Long> fileAlreadyPresent = new HashMap<Long,Long>();
            for(FileDetailsByPersonId obj : list)
            {
                
                if( fileAlreadyPresent != null && fileAlreadyPresent.containsKey(obj.getInheritanceFileId()) ) continue;
                if(obj.getResearcherId() == null || obj.getResearcherId().trim().length()<=0)continue;
                json+=  "{" +
                            " \"employeeNameEn\":\""+obj.getResearcherNameEn()+"\""+
                            ",\"employeeNameAr\":\""+obj.getResearcherNameAr()+"\""+
                            ",\"employeeNameId\":\""+ obj.getResearcherId()+"\""+
                                                    
                           "},";
                hasItems  = true;
                fileAlreadyPresent.put(obj.getInheritanceFileId(), obj.getInheritanceFileId());
            }
             if(hasItems )
             {
                 json = json.substring(0,json.length()-1);
             }
             json=json +"]}";
             logger.logInfo("json :%s ",json);
         }
        catch(Exception e)
        {
                    logger.LogException("Error occured", e);
                    json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }
    
    
    
    @POST
    @Path("/persistChatMessage")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String persistChatMessage(
                                        String inputParameters 
                                        
                                    )
    {
        
        String json="{\"chat\": {";
        try
        {
            logger.logInfo("persistChatMessage|inputParameters:%s", inputParameters );
            Gson jsonObj =  new GsonBuilder().create();
            PersistChatInputParameters inputObj = jsonObj.fromJson(inputParameters,PersistChatInputParameters.class);
            String validationResult = hasErrorChatPersist(inputObj.getMsg(), inputObj.getCreatedBy(), inputObj.getInheritanceFileId() );
            if(validationResult != null && validationResult.trim().length()>0) return validationResult;
            ChatMessages newMsg = new ChatMessages();
            newMsg.setMessage(inputObj.getMsg());
            newMsg.setCreatedById(inputObj.getCreatedBy());
            newMsg.setInheritanceFileId(inputObj.getInheritanceFileId());
            newMsg.setReceipientId(inputObj.getReceipientId());
            //logger.logInfo("persistChatMessage|saving" );
            newMsg =ChatService.persistChat( newMsg );
            DateFormat df  = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss aaa");
            if(newMsg.getCreatedById() == null )newMsg.setCreatedById(""); 
            if(newMsg.getReceipientId() == null )newMsg.setReceipientId(""); 
            //logger.logInfo("persistChatMessage|tojson:chatMessageId:%s",newMsg.getChatId());
            json += "\"chatId\":\""+    newMsg.getChatId().toString()+
            "\",\"createdById\":\""+ newMsg.getCreatedById()+
            "\",\"createdOn\":\""+ df.format(  newMsg.getCreatedOn() )+
                    
            "\",\"receipientId\":\""+ newMsg.getReceipientId()+
            "\",\"inheritanceFileId\":\""+ newMsg.getInheritanceFileId()+
            "\",\"message\":\""+ newMsg.getMessage() +"\"}}";
            
//            logger.logDebug("persistChatMessage|json before document:%s", json);
            
//            json = json +  getDocumentsJSON( null, newMsg.getChatId().toString(), "chat", jsonObj)+"}";
            
//            json=json +"}";
            logger.logDebug("persistChatMessage|json:%s", json);
            return json;
            //return "successMsgId:"+newMsg.getChatId().toString();
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("persistChatMessage-Finish");
        }
        return json;
    }
    
    private String hasErrorChatPersist(String msg,String createdBy,Long inheritanceFileId)throws Exception
    {
        StringBuilder errorJson = null;
        boolean hasError = false;
//      Created By Not provided
        if( 
            msg == null || 
            msg.trim().length() <= 0 
          )
        {
                if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
                errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.CHAT_MSG_REQUIRED ).append( "," );
                errorJson=errorJson.append( errorMsgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.CHAT_MSG_REQUIRED, "en" )
                                                               ).
                                                                append("\",");
                errorJson=errorJson.append( errorMsgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.CHAT_MSG_REQUIRED , "ar" )
                                                               ).
                                                               append("\"");
                
                errorJson = errorJson.append("},");
                hasError = true;
        }
        if( 
            createdBy == null || 
            createdBy.trim().length() <= 0 
          )
        {
                if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
                errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.CHAT_CREATED_BY_USER_REQUIRED ).append( "," );
                errorJson=errorJson.append( errorMsgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.CHAT_CREATED_BY_USER_REQUIRED, "en" )
                                                               ).
                                                                append("\",");
                errorJson=errorJson.append( errorMsgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.CHAT_CREATED_BY_USER_REQUIRED , "ar" )
                                                               ).
                                                               append("\"");
                
                errorJson = errorJson.append("},");
                hasError = true;
        }
        if( 
            inheritanceFileId == null 
          )
        {
                if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
                errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.CHAT_INHERITANCE_FILE_ID_REQUIRED ).append( "," );
                errorJson=errorJson.append( errorMsgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.CHAT_INHERITANCE_FILE_ID_REQUIRED, "en" )
                                                               ).
                                                                append("\",");
                errorJson=errorJson.append( errorMsgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.CHAT_INHERITANCE_FILE_ID_REQUIRED , "ar" )
                                                               ).
                                                               append("\"");
                
                errorJson = errorJson.append("},");
                hasError = true;
        }
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
    }
    
    

    @GET
    @Path("/chatList")    
    @Produces("application/json;charset=utf-8")
    @SuppressWarnings("unchecked")
        public String getChatList(
                                    @QueryParam("loginId")       String loginId, 
                                    @QueryParam("inheritanceFileId")       String inheritanceFileId, 
                                    @QueryParam("filters")       String filters, 
                                    @QueryParam("recordsPerPage") String recordsPerPage,
                                    @QueryParam("currentPage")    String currentPage,
                                    @QueryParam("orderBy")        String orderBy, 
                                    @QueryParam("ascending")      String ascending 
                                 )
    {
        String json="{\"chat\": [";
        try
        {
    //      
            logger.logInfo("getChatList|loginId:%s|inheritanceFileId:%s|filters:%s|recordsPerPage:%s|currentPage:%s|orderBy:%s|ascending:%s", loginId,inheritanceFileId,filters,recordsPerPage,currentPage,orderBy,ascending);
       
            String errorJson = hasChatListByInputParamErrors( loginId, inheritanceFileId, filters, recordsPerPage, currentPage, orderBy, ascending );
            
            if( errorJson.length() > 0 ){logger.logInfo("hasError:%s", errorJson);return errorJson;}
            ChatService service = new ChatService();
            Map<Object,Object> searchCriteriaMap = new HashMap<Object,Object>();
            searchCriteriaMap.put(Constant.ChatSearchCriteria.LOGGED_IN_USER,loginId );
            searchCriteriaMap.put(Constant.ChatSearchCriteria.INHERITANCE_FILE,inheritanceFileId);
            
            List<ChatMessages> list = service.getChatList (
                                                            searchCriteriaMap,
                                                            recordsPerPage != null && recordsPerPage.length()>0? Integer.valueOf(recordsPerPage) :null,
                                                            currentPage!= null && currentPage.length()>0 ?Integer.valueOf(currentPage):null  ,
                                                            (orderBy != null && orderBy.trim().length()>0 &&!orderBy.equals("-1")) ? orderBy:"chatId",
                                                            (ascending != null && ascending.equals("1")) ? true:false
                                                          );
            boolean hasItems= false;
//            DateFormat df = new SimpleDateFormat("dd-MMM-yyyy hh:mm");
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            HashMap ddMap = new UtilityManager().getDomainDataMap();
            for (ChatMessages obj : list) 
            {
             
                if(obj.getCreatedById() == null )obj.setCreatedById(""); 
                if(obj.getReceipientId() == null )obj.setReceipientId(""); 
                
                obj.getInheritanceFile();
                obj.getInheritanceFileId();
                json = json + jsonObj.toJson(obj);
                json = json.substring(0,json.length()-1)+",";
                json = json +  getDocumentsJSON( null, obj.getChatId().toString(), "chat", jsonObj)+"},";
                hasItems= true;
            }
            if(hasItems )
            {
                json = json.substring(0,json.length()-1);
            }
            json=json +"]}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                 ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }
    
        
    private String hasChatListByInputParamErrors  (
                                                    String loginId,
                                                    String inheritanceFileId,
                                                    String filters,
                                                    String recordsPerPage,
                                                    String currentPage,
                                                    String orderBy, 
                                                    String ascending 
                                                  )throws Exception
    {
        StringBuilder errorJson= new StringBuilder("{\"errors\": [");
        boolean hasError = false;
        if( loginId == null )
        {
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.LOGIN_ID_REQUIRED).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1103","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1103","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( inheritanceFileId == null )
        {
            errorJson=errorJson.append( errorCode ).append(  Constant.MobileWebServiceErrorCodes.INHERITANCE_FILE_ID_NOT_SPECIFIED ).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( "105" ,"en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( "105","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
    //        StringBuilder pagingErrorJson = hasPagingRelatedErrors(recordsPerPage, currentPage);
        if( orderBy != null && orderBy.trim().length() > 0 && !orderBy.equals("-1"))
        {
           if( !orderBy.equals("chatId") &&
               !orderBy.equals("createdOn") 
             )    
           {
            
    
                errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.INCORRECT_ORDER_BY).append(",");
                errorJson=errorJson.append( errorMsgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")
                                                                ).
                                                                append("\",");
                errorJson=errorJson.append( errorMsgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")
                                                               ).
                                                               append("\"");
                errorJson = errorJson.append("},");    
                hasError = true;
            }
            
        }
    
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
    }
    


    private String hasErrorPersistUserFeedback(UserFeedbackView inputObj )throws Exception
    {
        StringBuilder errorJson = null;
        boolean hasError = false;
        //User name not provided
        if( 
            inputObj.getUserName() == null || 
            inputObj.getUserName().trim().length() <= 0 
          )
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.FEEDBACK_USER_NAME_REQUIRED ).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.FEEDBACK_USER_NAME_REQUIRED , "en" )
                                                           ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.FEEDBACK_USER_NAME_REQUIRED , "ar" )
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        
        
        //Related To required
        if( 
            inputObj.getRelatedTo() == null || inputObj.getRelatedTo().length() <= 0
          )
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.FEEDBACK_RELATED_TO_REQUIRED ).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.FEEDBACK_RELATED_TO_REQUIRED, "en" )
                                                           ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.FEEDBACK_RELATED_TO_REQUIRED, "ar" )
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        //Description required
        if( 
            inputObj.getDescription() == null || inputObj.getDescription().length() <= 0
          )
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.FEEDBACK_REQUIRED ).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.FEEDBACK_REQUIRED, "en" )
                                                           ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.FEEDBACK_REQUIRED, "ar" )
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
    }    
    
    @POST
    @Path("/persistUserFeedback")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String persistUserFeedback(String inputParameters)
    {
        String json="";
        try
        {
            Gson jsonObj =  new GsonBuilder().create();
            UserFeedbackView inputObj = jsonObj.fromJson(inputParameters,UserFeedbackView.class);
            String validationResult = hasErrorPersistUserFeedback( inputObj );
            if(validationResult != null && validationResult.trim().length()>0) return validationResult;
            
            inputObj =  UserFeedbackService.persistUserFeedbackFromOnline(inputObj);
            json= jsonObj.toJson(inputObj,UserFeedbackView.class);
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            
            
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("persistUserFeedback-Finish");
        }
        return json;
    }
    
    
    @GET
    @Path("/persistUserHappinessForRequestId")    
    @Produces("application/json;charset=utf-8")
    public String persistUserHappinessForRequestId(  
                                                     @QueryParam("userId")      String userId,  
                                                     @QueryParam("requestId")   String requestId, 
                                                     @QueryParam("happinessId") String happinessId
                                                  )
    {
        String json="";
        try
        {
            
            RequestService.persistUserHappinessForRequestId(requestId, userId, happinessId);
            json="{\"msgEn\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.usrHappinesssaved","en")+"\""+
                    ",\"msgAr\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.usrHappinesssaved","ar")+"\"}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("persistUserHappinessForRequestId-Finish");
        }
        return json;
    }
        
        
    
    @GET
    @Path("/getUserHappinessIndex")    
    @Produces("application/json;charset=utf-8")
    public String getUserHappinessIndex( )
    {
        String json="{\"happinessIndex\": [";
        try
        {
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();            
            List<UserHappinessIndex> list = UtilityService.getAllUserHappinessIndex();
            boolean hasReason= false;
            for(UserHappinessIndex obj : list)
            {
                json = json + jsonObj.toJson(obj)+",";
                hasReason = true;
            }
            if( hasReason  )
            {
                json = json.substring(0,json.length()-1)+"]}";
            }
            else
            {
                json = json +"]}";    
            }
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("getUserHappinessIndex-Finish");
        }
        return json;
    }   
    
    @GET
    @Path("/getAccomodationFormDetails")    
    @Produces("application/json;charset=utf-8")
    public String getAccomodationFormDetails(  
                                             @QueryParam("researcherId")      String researcherId,  
                                             @QueryParam("beneficiaryId")   Long beneficiaryId, 
                                             @QueryParam("socialResearchId") Long socialResearchId
                                            )
    {
        String json="";
        try
        {
            logger.logDebug("getAccomodationFormDetails|researcherId:%s|beneficiaryId:%s|socialResearchId:%s",researcherId,beneficiaryId,socialResearchId);
            json ="{" +SocialResearchService.getAccomodationAspectsForm(researcherId, beneficiaryId, socialResearchId)+"}";
//          json = "{\"msgEn\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.usrHappinesssaved","en")+"\""+
//                    ",\"msgAr\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.usrHappinesssaved","ar")+"\"}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("getAccomodationFormDetails-Finish");
        }
        return json;
    }

    @GET
    @Path("/getEducationFormDetails")    
    @Produces("application/json;charset=utf-8")
    public String getEducationFormDetails(  
                                             @QueryParam("researcherId")     String researcherId,  
                                             @QueryParam("beneficiaryId")    Long beneficiaryId, 
                                             @QueryParam("socialResearchId") Long socialResearchId
                                          )
    {
        String json="";
        try
        {
            logger.logDebug("getEducationFormDetails|researcherId:%s|beneficiaryId:%s|socialResearchId:%s",researcherId,beneficiaryId,socialResearchId);
            json = "{" +SocialResearchService.getEducationAspectsForm(researcherId, beneficiaryId, socialResearchId)+"}";
    //          json = "{\"msgEn\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.usrHappinesssaved","en")+"\""+
    //                    ",\"msgAr\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.usrHappinesssaved","ar")+"\"}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("getEducationFormDetails-Finish");
        }
        return json;
    }

    @GET
    @Path("/getHealthFormDetails")    
    @Produces("application/json;charset=utf-8")
    public String getHealthFormDetails(  
                                             @QueryParam("researcherId")     String researcherId,  
                                             @QueryParam("beneficiaryId")    Long beneficiaryId, 
                                             @QueryParam("socialResearchId") Long socialResearchId
                                          )
    {
        String json="";
        try
        {
            logger.logDebug("getHealthFormDetails|researcherId:%s|beneficiaryId:%s|socialResearchId:%s",researcherId,beneficiaryId,socialResearchId);
            json = "{" +SocialResearchService.getHealthAspectsForm(researcherId, beneficiaryId, socialResearchId)+"}";
    //          json = "{\"msgEn\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.usrHappinesssaved","en")+"\""+
    //                    ",\"msgAr\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.usrHappinesssaved","ar")+"\"}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("getHealthFormDetails-Finish");
        }
        return json;
    }
    
    @GET
    @Path("/getSocialFormDetails")    
    @Produces("application/json;charset=utf-8")
    public String getSocialFormDetails(  
                                             @QueryParam("researcherId")     String researcherId,  
                                             @QueryParam("beneficiaryId")    Long beneficiaryId, 
                                             @QueryParam("socialResearchId") Long socialResearchId
                                          )
    {
        String json="";
        try
        {
            logger.logDebug("getSocialFormDetails|researcherId:%s|beneficiaryId:%s|socialResearchId:%s",researcherId,beneficiaryId,socialResearchId);
            json = "{" +SocialResearchService.getSocialAspectsForm(researcherId, beneficiaryId, socialResearchId)+"}";
    //          json = "{\"msgEn\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.usrHappinesssaved","en")+"\""+
    //                    ",\"msgAr\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.usrHappinesssaved","ar")+"\"}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("getSocialFormDetails-Finish");
        }
        return json;
    }

    @GET
    @Path("/getFinancialFormDetails")    
    @Produces("application/json;charset=utf-8")
    public String getFinancialFormDetails(  
                                             @QueryParam("researcherId")      String researcherId,  
                                             @QueryParam("beneficiaryId")   Long beneficiaryId, 
                                             @QueryParam("socialResearchId") Long socialResearchId
                                            )
    {
        String json="";
        try
        {
            logger.logDebug("getFinancialFormDetails|researcherId:%s|beneficiaryId:%s|socialResearchId:%s",researcherId,beneficiaryId,socialResearchId);
            json = "{" +SocialResearchService.getFinancialAspectsForm(researcherId, beneficiaryId, socialResearchId)+"}";
    //          json = "{\"msgEn\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.usrHappinesssaved","en")+"\""+
    //                    ",\"msgAr\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.usrHappinesssaved","ar")+"\"}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("getFinancialFormDetails-Finish");
        }
        return json;
    }


    @GET
    @Path("/getLocationFormDetails")    
    @Produces("application/json;charset=utf-8")
    public String getLocationFormDetails(  
                                         @QueryParam("researcherId")      String researcherId,  
                                         @QueryParam("beneficiaryId")   Long beneficiaryId, 
                                         @QueryParam("socialResearchId") Long socialResearchId,
                                         @QueryParam("inheritanceFileId") Long inheritanceFileId
                                        )
    {
        String json="";
        try
        {
            logger.logDebug("getLocationFormDetails|researcherId:%s|beneficiaryId:%s|socialResearchId:%s|inheritanceFileId:%s",researcherId,beneficiaryId,socialResearchId,inheritanceFileId);
            json = "{" +LocationsService.getLocationsFormDetails(inheritanceFileId, socialResearchId, beneficiaryId, researcherId,-1L)+"}";
            
    
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("getLocationFormDetails-Finish");
        }
        return json;
    }
    private String hasErrorPersistLocation(Locations location)throws Exception
    {
        StringBuilder errorJson = null;
        boolean hasError = false;
    
        if( 
            location.getInheritanceFileId() == null || 
            location.getInheritanceFileId().compareTo(0L) <= 0 
          )
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.INHERITANCE_FILE_ID_NOT_SPECIFIED ).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(ResourceWrapper.getInstance().getProperty("105", "en" )).append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(ResourceWrapper.getInstance().getProperty( "105", "ar" )).append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        
        
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
    }
    
    @POST
    @Path("/persistLocation")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String persistLocation(
                                   String inputParameters 
                                 )
    {
        
        String json="{\"location\": []}";
        try
        {
            logger.logDebug("persistLocation|inputParameters:%s", inputParameters );
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            Locations inputObj = jsonObj.fromJson(inputParameters,Locations.class);
            String validationResult = hasErrorPersistLocation(inputObj);
            if(validationResult != null && validationResult.trim().length()>0) return validationResult;
            inputObj = LocationsService.persistLocation( inputObj );
            if(inputObj == null ){return json; }
            json= "{"+LocationsService.getLocationsFormDetails(-1L, -1L, -1L, "-1", inputObj.getLocationId())+"}";
            logger.logDebug("persistLocation|json:%s", json);
            return json;
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("persistLocation-Finish");
        }
        return json;
    }
    
    @GET
    @Path("/deleteLocation")    
    @Produces("application/json;charset=utf-8")
    public String deleteLocation(  
                                    @QueryParam("locationId")   Long locationId, 
                                    @QueryParam("loginId") String loginId
                                )
    {
        String json="";
        try
        {
            logger.logDebug("deleteLocation|loactionId:%s|loginId:%s ",locationId,loginId);
            LocationsService.deleteLocation(locationId, loginId);
            json = "{\"msgEn\":\""+   ResourceWrapper.getInstance().getProperty("msg.success.locationRemovedSuccess","en")+
                    "\",\"msgAr\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.locationRemovedSuccess","ar")+"\"}";
            
    
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ 
                     ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                  ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("deleteLocation-Finish");
        }
        return json;
    }
    @POST
    @Path("/persistSocialResearch")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String persistSocialResearch(
                                        String inputParameters 
                                       )
    {
        
        String json="{\"socialResearch\": {";
        try
        {
            logger.logInfo("persistSocialResearch|inputParameters:%s", inputParameters );
            Gson jsonObj =  new GsonBuilder().create();
            PersistSocialResearchInputParameters inputObj = jsonObj.fromJson(inputParameters,PersistSocialResearchInputParameters.class);
            SocialResearch socialResearch = SocialResearchService.persistSocialResearch( inputObj );
            if(socialResearch == null ){json+= "}}";}
            DateFormat df  = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss aaa");
//            json += "\"socialResearchId\":\""+    socialResearch.getSocialResearchId()+
//                    "\",\"createdById\":\""+ socialResearch.getCreatedBy()+
//                    "\",\"createdOn\":\""+ df.format(  socialResearch.getCreatedOn() )+
//                    "\",\"updatedById\":\""+ socialResearch.getUpdatedBy()+
//                    "\",\"updatedOn\":\""+ df.format(  socialResearch.getUpdatedOn() )+
//                    "\",\"inheritanceFileId\":\""+ (socialResearch.getInheritanceFile()!= null ?socialResearch.getInheritanceFile().getInheritanceFileId():-1)+"\"}}";
            json = SocialResearchService.getSocialResearchDetailJson(
                                                                        inputObj.getLoginId(),
                                                                        inputObj.getBeneficiaryId().toString(), 
                                                                        socialResearch.getSocialResearchId().toString()
                                                                      );
            logger.logDebug("persistSocialResearch|json:%s", json);
            return json;
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("persistSocialResearch-Finish");
        }
        return json;
    }
    
    @GET
    @Path("/getRelations")    
    @Produces("application/json;charset=utf-8")
    public String getRelations()
    {
        String json="";
        try
        {
            InheritanceFileService fileService =new InheritanceFileService();
            json="{\"relations\": [";
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            List<RelationshipView> objList = fileService.getAllRelationships();
            boolean hasReason= false;
            for(RelationshipView obj : objList)
            {
                    json = json + jsonObj.toJson(obj)+",";
                    hasReason = true;
                
            }
            if( hasReason  )
            {
                    json = json.substring(0,json.length()-1)+"]}";
            }
            else
            {
                
                json = json +"]}";    
                
            }
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
//        finally
//        {
//                logger.logDebug("getAllRequestTypes-Finish");
//        }
        return json;
    }
        
        
    
    @GET
    @Path("/fileTypes")    
    @Produces("application/json;charset=utf-8")
    public String getFileTypes()
    {
        String json="";
        try
        {
            json="{\"fileTypes\": [";
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            List<DomainData> objList = UtilityService.getDomainDataPOJOById(161L);
            boolean hasItem = false;
            for(DomainData obj : objList)
            {
                
                    json = json + jsonObj.toJson(obj)+",";
                    hasItem = true;
            }
            if( hasItem  )
            {
                json = json.substring(0,json.length()-1)+"]}";
            }
            else
            {
                logger.logDebug("Not Found..");
                json = json +"]}";    
            }
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
                logger.logDebug("getFileTypes-Finish");
        }
        return json;
    }   
    
    @GET
    @Path("/incomeCategory")    
    @Produces("application/json;charset=utf-8")
    public String getIncomeCategory()
    {
        String json="";
        try
        {
            json="{\"incomeCategory\": [";
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            List<DomainData> objList = UtilityService.getDomainDataPOJOById(175L);
            boolean hasItem = false;
            for(DomainData obj : objList)
            {
                
                    json = json + jsonObj.toJson(obj)+",";
                    hasItem = true;
            }
            if( hasItem  )
            {
                json = json.substring(0,json.length()-1)+"]}";
            }
            else
            {
                logger.logDebug("Not Found..");
                json = json +"]}";    
            }
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
                logger.logDebug("getIncomeCategory-Finish");
        }
        return json;
    }   

    @GET
    @Path("/minorStatus")    
    @Produces("application/json;charset=utf-8")
    public String getMinorStatus()
    {
        String json="";
        try
        {
            json="{\"minorStatus\": [";
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            List<DomainData> objList = UtilityService.getDomainDataPOJOById(227L);
            boolean hasItem = false;
            for(DomainData obj : objList)
            {
                
                    json = json + jsonObj.toJson(obj)+",";
                    hasItem = true;
            }
            if( hasItem  )
            {
                json = json.substring(0,json.length()-1)+"]}";
            }
            else
            {
                logger.logDebug("Not Found..");
                json = json +"]}";    
            }
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
           logger.logDebug("getMinorStatus-Finish");
        }
        return json;
    }   
    
    
    
    @GET
    @Path("/donationProgramsList")    
    @Produces("application/json;charset=utf-8")
    
    public String getDonationProgramsList(
                                            @DefaultValue ("-1")@QueryParam("loginId")          String loginId, 
                                            @DefaultValue ("-1")@QueryParam("deviceId")         String deviceId, 
                                            @DefaultValue ("10") @QueryParam("recordsPerPage")  String recordsPerPage,
                                            @DefaultValue ("1")  @QueryParam("currentPage")     String currentPage,
                                            @DefaultValue ("endowmentProgramId") @QueryParam("orderBy") String orderBy, 
                                            @DefaultValue ("1")  @QueryParam("ascending")       String ascending,
                                            @DefaultValue ("-1") @QueryParam("fromStartDate")        String fromStartDate, 
                                            @DefaultValue ("-1") @QueryParam("toStartDate")          String toStartDate, 
                                            @DefaultValue ("-1") @QueryParam("fromEndDate")        String fromEndDate, 
                                            @DefaultValue ("-1") @QueryParam("toEndDate")          String toEndDate, 
                                            @DefaultValue ("-1") @QueryParam("programName")     String programName, 
                                            @DefaultValue ("-1") @QueryParam("objective")       String objective, 
                                            @DefaultValue ("-1") @QueryParam("status")          String status
                                         )
    {
        String json="{\"donationPrograms\": [";
        try
        {
            String errorJson = "";//hasInheritanceFileDetailByLoginIdInputParamErrors(loginId, recordsPerPage, currentPage, orderBy, ascending);
            if( errorJson.length() > 0 )                                      
            {
                return errorJson;
            }
            Map<Object,Object> searchCriteriaMap = new HashMap<Object,Object>();
            if (loginId != null && !loginId.equals("-1") )
            {
                searchCriteriaMap.put("loginId",loginId.trim());
            }
            if (deviceId != null && !deviceId.equals("-1") )
            {
                searchCriteriaMap.put("deviceId",deviceId.trim());
            }
            
            if(  objective != null &&  !objective.equals("1"))
            {
                searchCriteriaMap.put("objective",objective.trim());
            }
            
            if(  fromStartDate != null && !fromStartDate.equals("1"))
            {
                searchCriteriaMap.put("fromStartDate",fromStartDate.trim());
            }
            
            if(  toStartDate != null && toStartDate.trim().length()>0 && !toStartDate.equals("-1"))
            {
                searchCriteriaMap.put("toStartDate",toStartDate.trim());
            }
            
            if(  fromEndDate != null && !fromEndDate.equals("1"))
            {
                searchCriteriaMap.put("fromEndDate",fromEndDate.trim());
            }
            
            if(  toEndDate != null && toEndDate.trim().length()>0 && !toEndDate.equals("-1"))
            {
                searchCriteriaMap.put("toEndDate",toEndDate.trim());
            }
            if(  programName != null && programName.trim().length()>0 && !programName.equals("-1"))
            {
                searchCriteriaMap.put("programName",programName.trim());
            }
            if(  status != null && status.trim().length()>0 && !status.equals("-1"))
            {
                searchCriteriaMap.put("status",status.trim());
            }
            EndowmentProgramService  programService = new EndowmentProgramService();
            List<EndowmentProgramView> list = programService.searchProgramsForMobile(
                                                                                        searchCriteriaMap,
                                                                                        recordsPerPage != null && recordsPerPage.length()>0? Integer.valueOf(recordsPerPage) :null,
                                                                                        currentPage!= null && currentPage.length()>0 ?Integer.valueOf(currentPage):null  ,
                                                                                        orderBy,
                                                                                        (ascending != null && ascending.equals("1")) ? true:false
                                                                                    );
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().disableHtmlEscaping().create();
            boolean hasItems= false;
            Map<Long,Long> prograAlreadyPresent = new HashMap<Long,Long>();
            for(EndowmentProgramView obj : list)
            {
                if( prograAlreadyPresent != null && prograAlreadyPresent.containsKey(obj.getEndowmentProgramId()) ) continue;
                DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                 obj.setTargetAmount(EndowmentProgramService.GetRaisedAmountForDonationProgram(obj.getEndowmentProgramId()));
                if(obj.getTargetAmount()== null)
                {
                    obj.setTargetAmount("0");
                }
                
                if(obj.getStartDate()!= null)
                {
                 obj.setStartDateDisplay( df.format( obj.getStartDate() ) );
                }
                else
                {
                  obj.setStartDateDisplay( "" );
                }
                 if(obj.getEndDate()!= null)
                 {
                  obj.setEndDateDisplay( df.format( obj.getEndDate() ) );
                 }
                 else
                 {
                   obj.setEndDateDisplay( "" );
                 }
                 
                 obj.setObjectiveIcon( "" );
                 String documentJson="";
                 if(obj.getObjectiveId() != null )
                 {
                     List<DocumentView> documents = searchDocument(null, obj.getObjectiveId().toString(), "objectiveIcon");
                     if( documents != null && documents.size() > 0 )
                     {
                       DocumentView document = documents.get(0);
                       obj.setObjectiveIcon( downloadURL+document.getDocumentId() );
                       documentJson=  getDocumentJSONFromList(true,jsonObj,documents);
                           
//                           getDocumentsJSON(document.getDocumentId(),null,null,jsonObj);
                     }

                 }
                json = json + jsonObj.toJson(obj);
                json=json.trim();
                if(documentJson != null && documentJson.trim().length()>0 )
                {
                    json =  json.substring(0,json.length()-1)+","+documentJson+"},";
                }
                else
                {
                     json =  json.substring(0,json.length()-1)+"},";
                }
                hasItems= true;
                prograAlreadyPresent.put(obj.getEndowmentProgramId(), obj.getEndowmentProgramId());
             }
            if(hasItems )
            {
                json = json.substring(0,json.length()-1).replace("\u003d", "=");
            }
            
            json=json +"]}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                  ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }
    
    @GET
    @Path("/donationProgramStatus")    
    @Produces("application/json;charset=utf-8")
    public String getDonationProgramStatus()
    {
        String json="";
        try
        {
            json="{\"donationProgramStatus\": [";
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            List<DomainData> objList = UtilityService.getDomainDataPOJOById(200L);
            boolean hasItem = false;
            for(DomainData obj : objList)
            {
                
                    json = json + jsonObj.toJson(obj)+",";
                    hasItem = true;
            }
            if( hasItem  )
            {
                json = json.substring(0,json.length()-1)+"]}";
            }
            else
            {
                logger.logDebug("Not Found..");
                json = json +"]}";    
            }
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
           logger.logDebug("getDonationProgramStatus-Finish");
        }
        return json;
    }   
    
    
    
    @GET
    @Path("/donationProgramObjective")    
    @Produces("application/json;charset=utf-8")
    public String getDonationProgramObjective()
    {
        String json="";
        try
        {
            json="{\"donationProgramObjective\": [";
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            UtilityService us = new UtilityService();
            List<EndProgObjective> objList = us.getEndowmentProgramObjectives();
            boolean hasItem = false;
            for(EndProgObjective obj : objList)
            {
                
                    json = json + jsonObj.toJson(obj)+",";
                    hasItem = true;
            }
            if( hasItem  )
            {
                json = json.substring(0,json.length()-1)+"]}";
            }
            else
            {
                logger.logDebug("Not Found..");
                json = json +"]}";    
            }
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
           logger.logDebug("getDonationProgramObjective-Finish");
        }
        return json;
    }   
    
    
    @POST
    @Path("/persistDonationRequest")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String persistDonationRequest(String inputParameters)//(@FormParam("inputParameters")String inputParameters)
    {
        String json="";
        try
        {
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().disableHtmlEscaping().create();
            logger.logInfo("persistDonationRequest|input:%s", inputParameters);
            PersistDonationsInputParameters input = jsonObj.fromJson(inputParameters,PersistDonationsInputParameters.class);
            RequestService requestService  = new RequestService();
            String validationResult = hasErrorDonationRequest(input);
            if(validationResult != null && validationResult.trim().length()>0) return validationResult;
            
            requestService.persistDonationRequestFromMobile(input);
            input.setMsgEn(MessageFormat.format(ResourceWrapper.getInstance().getProperty("msg.success.donationRequestSaved","en"),input.getDonationAmount()));
            input.setMsgAr(MessageFormat.format(ResourceWrapper.getInstance().getProperty("msg.success.donationRequestSaved","ar"),input.getDonationAmount()));
            
            json=jsonObj.toJson(input);
//          if( input.getPaymentMethod()!= null && input.getPaymentMethod().equals(Constant.MobileDonationRequestMethod.CREDIT_CARD_ID ) )
//          {
//            
//                json = json.trim();
//                json = json.substring(0,json.length()-1)+getEPayJSON()+"}";
//          } 
            logger.logInfo("persistDonationRequest|json:%s", json);
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
                logger.logDebug("persistDonationRequest-Finish");
        }
        return json;
    }
    

    private String hasErrorDonationRequest(PersistDonationsInputParameters inputObj )throws Exception
    {
        StringBuilder errorJson = null;
        boolean hasError = false;
        
        if( 
            ( inputObj.getLoginId() == null || inputObj.getLoginId().trim().length() <= 0  || inputObj.getLoginId().equals("-1") ) &&
            ( inputObj.getDeviceId() == null || inputObj.getDeviceId().trim().length() <= 0  || inputObj.getDeviceId().equals("-1") ) 
                                                                                                                             
          )
        
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.DONATION_REQUEST_CREATED_BY_REQUIRED ).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.DONATION_REQUEST_CREATED_BY_REQUIRED , "en" )
                                                           ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.DONATION_REQUEST_CREATED_BY_REQUIRED , "ar" )
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        
        //Appointment with user not provided
        if( 
            inputObj.getDonationAmount() == null  
            
          )
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.DONATION_REQUEST_AMOUNT_REQ ).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.DONATION_REQUEST_AMOUNT_REQ , "en" )
                                                           ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.DONATION_REQUEST_AMOUNT_REQ , "ar" )
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        
        else
        {
            
            if( inputObj.getDonationAmount().compareTo(0D)<=0 )
            {
                if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
                errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.DONATION_REQUEST_INVALID_AMOUNT).append( "," );
                errorJson=errorJson.append( errorMsgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.DONATION_REQUEST_INVALID_AMOUNT , "en" )
                                                               ).
                                                                append("\",");
                errorJson=errorJson.append( errorMsgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.DONATION_REQUEST_INVALID_AMOUNT , "ar" )
                                                               ).
                                                               append("\"");
                
                errorJson = errorJson.append("},");
                hasError = true;
            }
        }
        
        if( 
            inputObj.getPaymentMethod() == null || 
            !( inputObj.getPaymentMethod().equals("1") ||  inputObj.getPaymentMethod().equals("2")  )
          )
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.DONATION_REQUEST_METHOD_REQ ).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.DONATION_REQUEST_METHOD_REQ, "en" )
                                                           ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.DONATION_REQUEST_METHOD_REQ, "ar" )
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        
        if( 
            inputObj.getDonationProgramId() == null || inputObj.getDonationProgramId().equals("-1") 
          )
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.DONATION_REQUEST_PROGRAM_REQ).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.DONATION_REQUEST_PROGRAM_REQ, "en" )
                                                           ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( Constant.MobileWebServiceErrorCodes.DONATION_REQUEST_PROGRAM_REQ, "ar" )
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
        
    
    }
    
    
    @GET
    @Path("/donationProgramDetails")    
    @Produces("application/json;charset=utf-8")

    public String getDonationProgramDetails(  
                                             @QueryParam("donationProgramId")      Long donationProgramId
                                            )
    {
        String json="";
        try
        {
            logger.logDebug("getDonationProgramDetails|donationProgramId:%s|",donationProgramId);
            json =EndowmentProgramService.getProgramDetails(donationProgramId);
    //          json = "{\"msgEn\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.usrHappinesssaved","en")+"\""+
    //                    ",\"msgAr\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.usrHappinesssaved","ar")+"\"}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("getDonationProgramDetails-Finish");
        }
        return json;
    }
    
    
    
    @GET
    @Path("/getMyContributionsInDonationProgram")    
    @Produces("application/json;charset=utf-8")
    public String getMyContributionsInDonationProgram(
                                                         @QueryParam("loginId") String loginId,
                                                         @QueryParam("deviceId") String deviceId,
                                                         @QueryParam("donationProgramId") Long donationProgramId
        
                                                     )
    {
        String json="";
        try
        {
            
            
            json = "{" +EndowmentProgramService.getMyContributionsInDonationProgram(loginId,deviceId,donationProgramId)+"}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
           logger.logDebug("getMyContributionsInDonationProgram-Finish");
        }
        return json;
    }   
    
    
    @GET
    @Path("/getMyContributions")    
    @Produces("application/json;charset=utf-8")
    public String getMyContributions(
                                     @QueryParam("loginId") String loginId,
                                     @QueryParam("deviceId") String deviceId
                                    )
    {
        String json="";
        try
        {
            
            
            json = "{" +EndowmentProgramService.getMyContributions(loginId,deviceId )+"}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
           logger.logDebug("getMyContributions-Finish");
        }
        return json;
    }   
    
    
    @GET
    @Path("/getSMSAmounts")    
    @Produces("application/json;charset=utf-8")
    public String getSMSAmounts(
                                 @QueryParam("donationProgramId") Long donationProgramId
                               )
    {
        String json="";
        try
        {
            
            
            json = "{" +EndowmentProgramService.getSMSAmounts(donationProgramId)+"}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
           logger.logDebug("getSMSAmounts-Finish");
        }
        return json;
    }   
    
    
    @GET
    @Path("/getGeneralFormDetails")    
    @Produces("application/json;charset=utf-8")
    public String getGeneralFormDetails(  
                                             @QueryParam("researcherId")     String researcherId,  
                                             @QueryParam("beneficiaryId")    Long beneficiaryId, 
                                             @QueryParam("socialResearchId") Long socialResearchId
                                          )
    {
        String json="";
        try
        {
            logger.logDebug("getGeneralFormDetails|researcherId:%s|beneficiaryId:%s|socialResearchId:%s",researcherId,beneficiaryId,socialResearchId);
            json = "{" +SocialResearchService.getGeneralAspectsForm(researcherId, beneficiaryId, socialResearchId)+"}";
    //          json = "{\"msgEn\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.usrHappinesssaved","en")+"\""+
    //                    ",\"msgAr\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.usrHappinesssaved","ar")+"\"}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("getGeneralFormDetails-Finish");
        }
        return json;
    }
    
    @GET
    @Path("/socialResearchList")    
    @Produces("application/json;charset=utf-8")
    
    public String socialResearchList(
                                        @QueryParam("loginId")                              String loginId, 
                                        @DefaultValue ("10") @QueryParam("recordsPerPage")  String recordsPerPage,
                                        @DefaultValue ("1")  @QueryParam("currentPage")     String currentPage,
                                        @DefaultValue ("createdOn") @QueryParam("orderBy") String orderBy, 
                                        @DefaultValue ("0")  @QueryParam("ascending")       String ascending,
                                        @DefaultValue ("-1") @QueryParam("fromDate")        String fromDate, 
                                        @DefaultValue ("-1") @QueryParam("toDate")          String toDate, 
                                        @DefaultValue ("-1") @QueryParam("fileName")        String fileName, 
                                        @DefaultValue ("-1") @QueryParam("fileNumber")      String fileNumber,
                                        @DefaultValue ("-1") @QueryParam("costCenter") String costCenter,
                                        @DefaultValue ("-1") @QueryParam("socialResearchNumber") String socialResearchNumber
                                    )
    {
        String json="{\"socialResearches\": [";
        try
        {
//            String errorJson = hasInheritanceFileDetailByLoginIdInputParamErrors(loginId, recordsPerPage, currentPage, orderBy, ascending);
//            if( errorJson.length() > 0 )                                      
//            {
//                return errorJson;
//            }
            
            SocialResearchSearchCriteria criteria = new SocialResearchSearchCriteria();
            
            if(  loginId != null && !loginId.equals("-1"))
            {
                
                criteria.setLoginId(loginId.trim());
            }
            if(  fileName != null && fileName.trim().length()>0 && !fileName.equals("-1"))
            {
                criteria.setFileOwnerName(fileName.trim());
            }
            if(  costCenter != null && costCenter.trim().length()>0 && !costCenter.equals("-1"))
            {
                
                criteria.setCostCenter(costCenter.trim());
            }
            if(  fileNumber != null && fileNumber.trim().length()>0 && !fileNumber.equals("-1"))
            {
                
                criteria.setFileNumber(fileNumber.trim());
            }
            
            if(  fromDate != null && fromDate.trim().length()>0 && !fromDate.equals("-1") )
            {
                
                criteria.setCreatedOnFrom(fromDate.trim());
            }
            if(  toDate != null && toDate.trim().length()>0 && !toDate.equals("-1") )
            {
                
                criteria.setCreatedOnTo(toDate.trim());
            }
            if(  socialResearchNumber != null && socialResearchNumber.trim().length()>0 && !socialResearchNumber.equals("-1") )
            {
                
                criteria.setSocialResearchNumber(socialResearchNumber.trim());
            }
            List<SocialResearch> list = SocialResearchService.searchForMobile(
                                                                                criteria,
                                                                                recordsPerPage != null && recordsPerPage.length()>0? Integer.valueOf(recordsPerPage) :null,
                                                                                currentPage!= null && currentPage.length()>0 ?Integer.valueOf(currentPage):null  ,
                                                                                orderBy,
                                                                                (ascending != null && ascending.equals("1")) ? true:false
                                                                             );
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
            boolean hasItems= false;
            HashMap ddMap = new UtilityManager().getDomainDataMap();
            HashMap<String ,UserView>  userMap = new UtilityService().getUsersMap( new UserView() );
            for(SocialResearch arg : list)
            {
                SocialResearchMobileView ret = new SocialResearchMobileView();
                MinorTransformUtil.transformToSocialResearchMobileView(arg, ret, ddMap, userMap);
                json = json + jsonObj.toJson(ret)+",";
                hasItems= true;
                
                
            }
            if(hasItems )
            {
                json = json.substring(0,json.length()-1);
            }
            
            json=json +"]}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }

    @GET
    @Path("/getMaintenanceSearchCriteria")    
    @Produces("application/json;charset=utf-8")
    public String getMaintenanceSearchCriteria(  )
    {
        String json="";
        try
        {
            logger.logDebug("getMaintenanceSearchCriteria");
            json ="{" +SocialResearchService.getMaintenanceSearchCriteria()+"}";
    
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("getMaintenanceSearchCriteria-Finish");
        }
        return json;
    }
    
    @GET
    @Path("/minorMaintenanceList")    
    @Produces("application/json;charset=utf-8")
    public String minorMaintenanceList(
                                        @QueryParam("loginId") String loginId,
                                        @QueryParam("inheritanceFileId") String inheritanceFileId,
                                        @DefaultValue ("10") @QueryParam("recordsPerPage") String recordsPerPage,
                                        @DefaultValue ("1")  @QueryParam("pageNumber") String pageNumber,
                                        @DefaultValue ("-1") @QueryParam("statusId") String statusId,
                                        @DefaultValue ("-1") @QueryParam("requestNumber") String requestNumber,
                                        @DefaultValue ("-1") @QueryParam("maintenanceType") String maintenanceType,
                                        @DefaultValue ("-1") @QueryParam("maintenanceWorkTypes") String  maintenanceWorkTypes
                                      )
    {
        String json="";
        try
        {
            logger.logDebug("minorMaintenanceList| loginId:%s|inheritanceFileId:%s|recordsPerPage:%s|pageNumber:%s" +
                            "statusId:%s|requestNumber:%s|maintenanceType:%s|maintenanceWorkTypes:%s",
                            loginId, inheritanceFileId, recordsPerPage, pageNumber,statusId, requestNumber, maintenanceType,
                            maintenanceWorkTypes
                           );
            json =
                SocialResearchService.getMinorMaintenanceList(
                                                                loginId, inheritanceFileId, recordsPerPage, pageNumber,
                                                                statusId, requestNumber, maintenanceType,
                                                                maintenanceWorkTypes
                                                              );
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("getMaintenanceSearchCriteria-Finish");
        }
        return json;
    }
    
    private String hasPersistMinorMaintenanceRequestErrors  (
                                                                    PersistMinorMaintenanceInputParameters input
                                                            )throws Exception
    {
        StringBuilder errorJson= new StringBuilder("{\"errors\": [");
        boolean hasError = false;
        if( input.getLoginId() == null )
        {
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.LOGIN_ID_REQUIRED).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1103","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1103","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( input.getInheritanceFileId() == null   || input.getInheritanceFileId().equals("-1"))
        {
            
            
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.INHERITANCE_FILE_ID_NOT_SPECIFIED).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( input.getAssetMemsId() == null || input.getAssetMemsId().equals("-1"))
        {
            errorJson=errorJson.append( errorCode ).append("3000").append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("3000","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("3000","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( input.getMaintenanceTypeId() == null || input.getMaintenanceTypeId().equals("-1"))
        {
            errorJson=errorJson.append( errorCode ).append("3001").append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("3001","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("3001","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( input.getMaintenanceWorkTypeId() == null || input.getMaintenanceWorkTypeId().equals("-1"))
        {
            errorJson=errorJson.append( errorCode ).append("3002").append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("3002","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("3002","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( input.getMaintenanceDetails() == null || input.getMaintenanceDetails().trim().length()<=0)
        {
            errorJson=errorJson.append( errorCode ).append("3004").append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("3004","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("3004","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        else if( input.getMaintenanceDetails().length()>1000)
        {
            errorJson=errorJson.append( errorCode ).append("3005").append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("3005","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("3005","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
            
        }
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
    }
    
    @POST
    @Path("/persistMinorMaintenanceRequest")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String persistMinorMaintenanceRequest( String inputParameters )
    {
        
        String json=null;
        try
        {
            logger.logInfo("persistMinorMaintenanceRequest|inputParameters:%s", inputParameters );
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            PersistMinorMaintenanceInputParameters inputObj = jsonObj.fromJson(inputParameters,PersistMinorMaintenanceInputParameters.class);
            String errorJson = hasPersistMinorMaintenanceRequestErrors( inputObj );
            if( errorJson.length() > 0 ){logger.logInfo("hasError:%s", errorJson);return errorJson;}
            
            Request request = SocialResearchService.persistMinorMaintenanceRequest( inputObj,Constant.REQUEST_ORIGINATION_MOBILE_ID );
            
            if(request == null || request.getRequestId()==null){json+= "{\"requestDetails\": {}}";}
            json = SocialResearchService.getMinorsMaintReqDetail(inputObj.getLoginId(), request.getRequestId().toString());
            logger.logDebug("persistMinorMaintenanceRequest|json:%s", json);
            return json;
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("persistMinorMaintenanceRequest-Finish");
        }
        return json;
    }
    @GET
    @Path("/minorMaintenanceFormDetails")    
    @Produces("application/json;charset=utf-8")
    public String minorMaintenanceFormDetails(@QueryParam("requestId") String requestId,@QueryParam("loginId") String loginId)
    {
        String json="";
        try
        {
            logger.logDebug( "minorMaintenanceRequestDetails| requestid:%s|loginId:%s",requestId,loginId );
            json =SocialResearchService.getMinorsMaintReqDetail(loginId, requestId );
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("minorMaintenanceRequestDetails-Finish");
        }
        return json;
    }
      
    @GET
    @Path("/deleteMinorMaintenanceRequest")    
    @Produces("application/json;charset=utf-8")
    @SuppressWarnings("unchecked")
    public String deleteMinorMaintenanceRequest(
                                                @QueryParam("requestId") Long requestId,
                                                @QueryParam("loginId") String loginId
                                               )
    {
        String json;
        try
        {
            Request request = SocialResearchService.deleteMinorMaintenanceRequest(requestId, loginId);
            json =   "{\"deleteMinorMaintenanceRequest\": [{" +
                                                "\"msgEn\":\""+ MessageFormat.format(ResourceWrapper.getInstance().getProperty("msg.success.minorMaintenanceRequestDeleted","en"),request.getRequestNumber())+"\""+
                                                ",\"msgAr\":\""+ MessageFormat.format(ResourceWrapper.getInstance().getProperty("msg.success.minorMaintenanceRequestDeleted","ar"),request.getRequestNumber())+"\"" +
                                             "}]}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        
        return json;
    }

    private String hasPersistRecommendationErrors  (
                                                    PersistRecommendationsInputParameters input
                                                   )throws Exception
    {
        StringBuilder errorJson= new StringBuilder("{\"errors\": [");
        boolean hasError = false;
        if( input.getLoginId() == null )
        {
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.LOGIN_ID_REQUIRED).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1103","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1103","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( input.getInheritanceFileId() == null   || input.getInheritanceFileId().equals("-1"))
        {
            
            
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.INHERITANCE_FILE_ID_NOT_SPECIFIED).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( input.getCaringTypeId() == null   || input.getCaringTypeId().equals("-1"))
        {
            
            
            errorJson=errorJson.append( errorCode ).append("4000").append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("4000","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("4000","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( input.getPersonId() == null   || input.getPersonId().equals("-1"))
        {
            
            
            errorJson=errorJson.append( errorCode ).append("4001").append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("4001","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("4001","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
    }    
    
    @GET
    @Path("/recommendationsList")    
    @Produces("application/json;charset=utf-8")
    public String recommendationsList(
                                        @QueryParam("loginId") String loginId,
                                        @DefaultValue ("-1")  @QueryParam("socialResearchId") String socialResearchId,
                                        @DefaultValue ("-1")  @QueryParam("inheritanceFileId") String inheritanceFileId,
                                        @DefaultValue ("10") @QueryParam("recordsPerPage") String recordsPerPage,
                                        @DefaultValue ("1")  @QueryParam("pageNumber") String pageNumber
                                      )
    {
        String json="";
        try
        {
            logger.logDebug("recommendationsList| loginId:%s|socialResearchId:%s|recordsPerPage:%s|pageNumber:%s|inheritanceFileId:%s",
                                                    loginId, socialResearchId,   recordsPerPage,   pageNumber,   inheritanceFileId
                           );
            json =
                SocialResearchService.getRecommendationsList(
                                                                loginId, socialResearchId, inheritanceFileId, recordsPerPage, pageNumber
                                                            );
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("recommendationList-Finish");
        }
        return json;
    }
    @GET
    @Path("/recommendationFormDetails")    
    @Produces("application/json;charset=utf-8")
    public String recommendationFormDetails(
                                            @QueryParam("loginId") String loginId,
                                            @QueryParam("recommendationId") String recommendationId,
                                            @QueryParam("socialResearchId") String socialResearchId,
                                            @QueryParam("inheritanceFileId") String inheritanceFileId,
                                            @QueryParam("beneficiaryId") String beneficiaryId
                                            )
    {
        String json="";
        try
        {
            logger.logInfo( "recommendationFormDetails| recommendationId:%s|loginId:%s|socialResearchId:%s|inheritanceFileId:%s|beneficiaryId:%s",
                                                        recommendationId   ,loginId  , socialResearchId   ,inheritanceFileId   ,beneficiaryId
                          );
            json =SocialResearchService.getRecommendationDetail(loginId,recommendationId, socialResearchId,inheritanceFileId,beneficiaryId);
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("recommendationFormDetails-Finish");
        }
        return json;
    }
    
    @POST
    @Path("/persistRecommendation")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String persistRecommendation( String inputParameters )
    {
        
        String json=null;
        try
        {
            logger.logInfo("persistRecommendation|inputParameters:%s", inputParameters );
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            PersistRecommendationsInputParameters inputObj = jsonObj.fromJson(inputParameters,PersistRecommendationsInputParameters.class);
            String errorJson = hasPersistRecommendationErrors( inputObj );
            if( errorJson.length() > 0 ){logger.logInfo("hasError:%s", errorJson);return errorJson;}
            
            ResearchRecommendation recommendation = SocialResearchService.persistRecommendation( inputObj );
            
            if(recommendation == null || recommendation.getResearchRecommendationId()==null){json+= "{\"recommendationDetails\": {}}";}
            json = SocialResearchService.getRecommendationDetail(
                                                                    inputObj.getLoginId(), 
                                                                    recommendation.getResearchRecommendationId().toString(),
                                                                    recommendation.getSocialResearch().getSocialResearchId().toString(),
                                                                    recommendation.getSocialResearch().getInheritanceFile().getInheritanceFileId().toString(),
                                                                    recommendation.getPerson().getPersonId().toString()
                                                                );
            logger.logDebug("persistRecommendation|json:%s", json);
            return json;
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("persistRecommendation-Finish");
        }
        return json;
    }    
    @GET
    @Path("/deleteRecommendation")    
    @Produces("application/json;charset=utf-8")
    @SuppressWarnings("unchecked")
    public String deleteRecommendation(
                                                @QueryParam("recommendationId") Long recommendationId,
                                                @QueryParam("loginId") String loginId
                                               )
    {
        String json;
        try
        {
            SocialResearchService.deleteResearchRecommendation(recommendationId, loginId);
            json =   "{\"deleteRecommendation\": [{" +
                                                "\"msgEn\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.recommendationDeleted","en")+"\""+
                                                ",\"msgAr\":\""+ResourceWrapper.getInstance().getProperty("msg.success.recommendationDeleted","ar")+"\"" +
                                             "}]}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        
        return json;
    }
    
    @POST
    @Path("/persistBeneficiaryDetails")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String persistBeneficiaryDetails( String inputParameters )
    {
        String json=null;
        try
        {
            logger.logInfo("persistBeneficiary|inputParameters:%s", inputParameters );
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            PersistBeneficiaryInputParameters inputObj = jsonObj.fromJson(inputParameters,PersistBeneficiaryInputParameters.class);
//          String errorJson = hasPersistRecommendationErrors( inputObj );
//          if( errorJson.length() > 0 ){logger.logInfo("hasError:%s", errorJson);return errorJson;}
            InheritanceFileService.persistInheritanceBeneficiaryDetailsFromMobile(inputObj);
            json="{\"msgEn\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.beneficiaryDetailsSaved","en")+"\""+
                 ",\"msgAr\":\""+ ResourceWrapper.getInstance().getProperty("msg.success.beneficiaryDetailsSaved","ar")+"\"}";
            logger.logDebug("persistBeneficiaryDetails|json:%s", json);
            return json;
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("persistBeneficiary-Finish");
        }
        return json;
    }

    @GET
    @Path("/researchStats")    
    @Produces("application/json;charset=utf-8")
    public String researchStats( @QueryParam("loginId") String loginId )
    {
        String json="";
        try
        {
            json = SocialResearchService.getResearchStats(loginId);
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("researchStats-Finish");
        }
        return json;
    }


    @GET
    @Path("/getComparisonList")    
    @Produces("application/json;charset=utf-8")
    public String comparisonList( 
                                 
                                @QueryParam("aspect")  String aspect,
                                @DefaultValue("5")     @QueryParam("records") String records,
                                @QueryParam("loginId") String loginId ,
                                @QueryParam("inheritanceFileId") String inheritanceFileId,
                                @QueryParam("beneficiaryId") String beneficiaryId
                                )
    {
        String json="";
        try
        {
            json = SocialResearchService.getComparisonList(	aspect,
								records,
								loginId,
								inheritanceFileId,
								beneficiaryId
                                                           );
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("comparisonList-Finish");
        }
        return json;
    }
    
    
    @GET
    @Path("/compareSavedAspects")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String compareSavedAspects( 
                                        @QueryParam("aspect")      String aspect,
                                        @QueryParam("personId")    Long   personId,
                                        @QueryParam("oldAspectId") Long   oldAspectId
                                     )
    {
        String json=null;
        try
        {
            logger.logInfo("compareSavedAspects|aspect:%s|personId:%s|oldAspectId:%s", aspect, personId, oldAspectId );
            Gson jsonObj =  new GsonBuilder().create();
            AspectsCompareInputParameters inputObj = new AspectsCompareInputParameters();
            inputObj.setAspectName(aspect);
            inputObj.setBeneficiaryId(personId);
            inputObj.setOldAspectId(oldAspectId.toString());
            json  = SocialResearchService.compareSavedAspects(inputObj);
            logger.logDebug("compareSavedAspects|json:%s", json);
            return json;
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("compareAspects-Finish");
        }
        return json;
    }       
    @POST
    @Path("/compareAspects")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String compareAspects( String inputParameters )
    {
        String json=null;
        try
        {
            logger.logInfo("compareAspects|inputParameters:%s", inputParameters );
            Gson jsonObj =  new GsonBuilder().create();
            AspectsCompareInputParameters inputObj = jsonObj.fromJson(inputParameters,AspectsCompareInputParameters.class);
            json  = SocialResearchService.compareAspects(inputObj);
    
            logger.logDebug("compareAspects|json:%s", json);
            return json;
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("compareAspects-Finish");
        }
        return json;
    }

    public String getEPayJSON( )
    {
            return          ",\"ePay\": {\"ePayRequestURL\":\"http://esvapps.amaf.ae/pims_eservice/pages/SPServlet.jsp\""+
                            ",\"ePayResponseURI\":\"EPayResponse\"}";
        
        
    }

    @GET
    @Path("/checkEPayTrxStatus")
    @Produces("application/json;charset=utf-8")
   public String checkEPayTrxStatus(@QueryParam("token") String token)
   {
        String json=null;
        String statusCode="1";
        String msgEn="";
        String msgAr="";
        try
        {
            logger.logInfo("checkEPayTrxStatus|token:%s", token);
            String parameters ="";
            Gson gson =  new GsonBuilder().create();                               
            String url = "http://172.16.1.20:7001/epayintegration/resources/emitac/";
            parameters = "token="+ token+"&spCode=AMAF&serviceCode=AMAFLeasing";
            logger.logInfo("checkEPayTrxStatus|calling checkEPayTrxStatus:%s ",parameters);
            String response = CallWebservice.GET(url,"checkEPayTrxStatus",parameters);
            logger.logInfo("checkEPayTrxStatus|response:%s ",response);
            EPayTokenDetails tokenDetails = gson.fromJson(response, EPayTokenDetails.class);
            String msg="";
            String msgCode="";
            
            if(tokenDetails.getSuccessMsgCode() != null && tokenDetails.getSuccessMsgCode().length()>0)
            {
                msg  = tokenDetails.getSuccessMsg();
                msgCode = tokenDetails.getSuccessMsgCode();
                //Success
                if(tokenDetails.getSuccessMsgCode().equals(Constant.EPayParameters.EPAY_TRX_SUCCESS_CODE))
                {
                    msgEn =  ResourceWrapper.getInstance().getProperty("msg.success.epayTrx","en");
                    msgAr =  ResourceWrapper.getInstance().getProperty("msg.success.epayTrx","ar");
                    statusCode="1";
                }
                //Cancelled by user
                else if(tokenDetails.getSuccessMsgCode().equals(Constant.EPayParameters.EPAY_TRX_CANCELLED_CODE))
                {
                    msgEn =  ResourceWrapper.getInstance().getProperty("error.epay.10","en");
                    msgAr =  ResourceWrapper.getInstance().getProperty("error.epay.10","ar");
                    statusCode="-1";
                }   
                //Any other issue
                else
                {
                    msgEn =  tokenDetails.getSuccessMsg();
                    msgAr =  tokenDetails.getSuccessMsg();
                    statusCode="0";
                }   
            }
            else
            {
                msg = tokenDetails.getFailureMsg();
                msgCode = tokenDetails.getFailureMsgCode();
                statusCode="0";
                msgEn =  ResourceWrapper.getInstance().getProperty("error.epay.0","en");
                msgAr =  ResourceWrapper.getInstance().getProperty("error.epay.0","ar");
            }

            EPayTransactionService epayService = new EPayTransactionService();
            epayService.ePayResponseFromDEG(
                                            tokenDetails.getSptrn(),
                                            tokenDetails.getDegtrx(),
                                            msg,
                                            msgCode,
                                            token,
                                            tokenDetails.getPaymentMethod()
                                            
                                            );
            json = "{\"status\":\""+statusCode+"\",\"msgEn\":\""+msgEn+"\",\"msgAr\":\""+msgAr+"\"}";
            logger.logDebug("checkEPayTrxStatus|json:%s", json);
            return json;
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("checkEPayTrxStatus-Finish");
        }   
        return json;
    }
    
    @GET
    @Path("/getPrivacyPolicy")    
    @Produces("application/json;charset=utf-8")
    public String getPrivacyPolicy( @QueryParam("loginId") String loginId,@QueryParam("deviceId") String deviceId)
    {
        String json=null;
        try
        {
            
            logger.logInfo("getPrivacyPolicy|loginId:%s|deviceId:%s",loginId,deviceId);
            json = UtilityService.getPrivacyPolicy(loginId,deviceId);
                
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                    ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }

    @GET
    @Path("/getFAQS")    
    @Produces("application/json;charset=utf-8")
    public String getFAQS( @QueryParam("loginId") String loginId,@QueryParam("deviceId") String deviceId)
    {
        String json=null;
        try
        {
            logger.logInfo("getFAQS|loginId:%s|deviceId:%s",loginId,deviceId);
            json = UtilityService.getFAQS( loginId, deviceId);
                
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                    ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }
    
    @GET
    @Path("/getSocialResearchDetails")    
    @Produces("application/json;charset=utf-8")
    public String getSocialResearchDetails( @QueryParam("loginId") String loginId,
                                            @DefaultValue("-1") @QueryParam("socialResearchId") String socialResearchId,
                                            @QueryParam("beneficiaryId") String beneficiaryId    
                                        )
    {
        String json=null;
        try
        {
            logger.logInfo("getSocialResearchDetails|loginId:%s|socialResearchId:%s|beneficiaryId:%s",loginId,socialResearchId,beneficiaryId);
            json = SocialResearchService.getSocialResearchDetailJson( loginId, beneficiaryId ,socialResearchId);
                
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                    ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }
    

    @GET
    @Path("/getPasswordForLoginId")    
    @Produces("application/json;charset=utf-8")
    public String getPasswordForLoginId( @QueryParam("loginId") String loginId, @QueryParam("securityKey") String securityKey)
    {
        String json=null;
        try
        {
                logger.logInfo("getPasswordForLoginId|securityKey:%s",securityKey);
                if(!securityKey.equals("AMAF$12$PassReq"))
                {return json;}
                UserDetails user = UtilityService.getUserDetailsFromLoginId(loginId);
                String dbPassword = UserDbImpl.getCryptographer().decrypt(user.getPassword());        
                return      "{\"password\":\""+dbPassword +"\"}";
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                    ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        return json;
    }
    
    @GET
    @Path("/propertyNOLTypes")    
    @Produces("application/json;charset=utf-8")
    public String propertyNOLTypes(@QueryParam("loginId") String loginId)
    {
        String json="";
        try
        {
            json = UtilityService.getPropertyNOLTypes();
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        
        return json;
    }
        
    @GET
    @Path("/getLeaseContractDetails")    
    @Produces("application/json;charset=utf-8")

    public String getLeaseContractDetails(  
                                             @QueryParam("contractId")      Long contractId
                                            )
    {
        String json="";
        try
        {
            logger.logDebug("getLeaseContractDetails|contractId:%s|",contractId);
            json =PropertyService.getLeaseContractDetails(contractId);
    
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("getLeaseContractDetails-Finish");
        }
        return json;
    }        

    @GET
    @Path("/leaseContractList")    
    @Produces("application/json;charset=utf-8")
    public String leaseContractList(  @QueryParam("filters")String filters )
    {
        String json="";
        try
        {
           logger.logInfo("leaseContractList|filters|%s", filters);
           Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().setPrettyPrinting().create();
           ContractSearchCriteria searchCriteria = jsonObj.fromJson( 
                                                              filters,  
                                                              ContractSearchCriteria.class
                                                            );
           searchCriteria.setStatus(String.valueOf(  Constant.CONTRACT_STATUS_ACTIVE_ID) );
           json = PropertyService.getLeaseContractListJson(searchCriteria);
           
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("leaseContractList-Finish");
        }
        return json;
    }        
    
    
    private String hasErrorPropertyNOLRequest(NolInputParameter inputObj,RequestService requestService  )throws Exception
    {
        StringBuilder errorJson = null;
        boolean hasError = false;
        if( 
            inputObj.getNolTypeId() == null || 
            inputObj.getNolTypeId().compareTo(-1L) == 0
          )
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.NOL_TYPE_REQUIRED).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( "msg.error.nolTypeRequired", "en" )
                                                           ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( "msg.error.nolTypeRequired", "ar" )
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( 
            inputObj.getDescription() == null || 
            inputObj.getDescription().trim().length() <= 0 
          )
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.NOL_REASON_REQUIRED ).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( "msg.error.nolReasonRequired", "en" )
                                                           ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( "msg.error.nolReasonRequired", "ar" )
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
    }
    @POST
    @Path("/persistPropertyNolRequest")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String persistPropertyNolRequest(String inputParameters)
    {
        String json="";
        try
        {
            
            Gson jsonObj =  new GsonBuilder().create();
            NolInputParameter inputObj = jsonObj.fromJson(inputParameters,NolInputParameter.class);
            
            String validationResult = hasErrorPropertyNOLRequest(inputObj,requestServiceObj);
            if(validationResult != null && validationResult.trim().length()>0) return validationResult;
            RequestView requestView = requestServiceObj.persistPropertyNolRequest(inputObj);
            json=getRequestSavedSuccessJSON(requestView);
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStrequestListackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("persistPropertyNolRequest-Finish");
        }
        return json;
    }
    
    
    @GET
    @Path("/defaulPaymentsForPropertyNOLType")    
    @Produces("application/json;charset=utf-8")
    
    public String defaulPaymentsForPropertyNOLType(
                                                    @QueryParam("loginId")String loginId, 
                                                    @QueryParam("nolTypeId")Long nolTypeId, 
                                                    @QueryParam("contractId")Long contractId
                                                  )
    {
        String json="";
        try
        {
            logger.logDebug("defaulPaymentsForPropertyNOLType|loginId:%s|nolTypeId:%s|contractId:%s",loginId,nolTypeId,contractId);    
            json = UtilityService.getDefaultPaymentsJSONForPropertyNolType(contractId, nolTypeId);
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        
        return json;
    }
    
    
    @GET
    @Path("/dataForRenewContract")    
    @Produces("application/json;charset=utf-8")
    
    public String dataForRenewContract(
                                                    @QueryParam("loginId")String loginId, 
                                                    @QueryParam("contractId")Long contractId
                                    )
    {
        String json="";
        try
        {
            logger.logDebug("dataForRenewContract|loginId:%s|contractId:%s",loginId,contractId);    
            
            json = propertyServiceObj.getDataForRenewContract(loginId,contractId);
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        
        return json;
    }


    @GET
    @Path("/createRenewContractRequest")    
    @Produces("application/json;charset=utf-8")
    public String createRenewContractRequest(
                                                    @QueryParam("loginId")String loginId, 
                                                    @QueryParam("contractId")Long contractId,
                                                    @QueryParam("personId")Long personId,
                                                    @DefaultValue("1") @QueryParam("requestSource")String requestSource
                                             )
    {
        String json="";
        try
        {
            logger.logDebug("createRenewContractRequest|loginId:%s|contractId:%s|requestSource:%s",loginId,contractId,requestSource);    
//            String validationResult = hasErrorPropertyNOLRequest(inputObj,requestServiceObj);
//            if(validationResult != null && validationResult.trim().length()>0) return validationResult;
            
            
            RequestView requestView =propertyServiceObj.createRenewContractRequest(loginId, contractId, personId, requestSource);
            json=getRequestSavedSuccessJSON(requestView);            
        }
        catch(Exception e)
        {
            logger.LogException("createRenewContractRequest|Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        
        return json;
    }
    
    @GET
    @Path("/getPropertyMaintenanceRequestDetails")    
    @Produces("application/json;charset=utf-8")
    public String getPropertyMaintenanceRequestDetails(
                                                    @QueryParam("loginId")String loginId, 
                                                    @DefaultValue("-1")@QueryParam("contractId")Long contractId,
                                                    @DefaultValue("-1")@QueryParam("unitId")Long unitId,
                                                    @DefaultValue("-1")@QueryParam("propertyId")Long propertyId,
                                                    @DefaultValue("-1")@QueryParam("requestId")Long requestId
                                                    
                                             )
    {
        String json="";
        try
        {
            logger.logDebug("getPropertyMaintenanceRequestDetails|loginId:%s|contractId:%s|unitId:%s|propertyId:%s|requestId:%s",
                            loginId,contractId, unitId ,propertyId,requestId);    
            json = RequestService.getPropertyMaintenanceRequestDetails(loginId, contractId,unitId,propertyId,requestId);
        }
        catch(Exception e)
        {
            logger.LogException("getPropertyMaintenanceRequestDetails|Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        
        return json;

    }
    
    
    private String hasPersistPropertyMaintenanceRequestErrors  (
                                                                    PersistPropertyMaintenanceInputParameters input
                                                            )throws Exception
    {
        StringBuilder errorJson= new StringBuilder("{\"errors\": [");
        boolean hasError = false;
        if( input.getLoginId() == null )
        {
            errorJson=errorJson.append( errorCode ).append(Constant.MobileWebServiceErrorCodes.LOGIN_ID_REQUIRED).append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1103","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("error.1103","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
//        
//        if( input.getMaintenanceTypeId() == null || input.getMaintenanceTypeId().equals("-1"))
//        {
//            errorJson=errorJson.append( errorCode ).append("3001").append(",");
//            errorJson=errorJson.append( errorMsgEn ).append(
//                                                             ResourceWrapper.getInstance().getProperty("3001","en")
//                                                            ).
//                                                            append("\",");
//            errorJson=errorJson.append( errorMsgAr ).append(
//                                                             ResourceWrapper.getInstance().getProperty("3001","ar")
//                                                           ).
//                                                           append("\"");
//            
//            errorJson = errorJson.append("},");
//            hasError = true;
//        }
        if( input.getMaintenanceWorkTypeId() == null || input.getMaintenanceWorkTypeId().equals("-1"))
        {
            errorJson=errorJson.append( errorCode ).append("3002").append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("3002","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("3002","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( input.getMaintenanceDetails() == null || input.getMaintenanceDetails().trim().length()<=0)
        {
            errorJson=errorJson.append( errorCode ).append("3004").append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("3004","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("3004","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        else if( input.getMaintenanceDetails().length()>1000)
        {
            errorJson=errorJson.append( errorCode ).append("3005").append(",");
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty("3005","en")
                                                            ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty("3005","ar")
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
            
        }
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
    }
    
    @POST
    @Path("/persistPropertyMaintenanceRequest")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String persistPropertyMaintenanceRequest( String inputParameters )
    {
        
        String json=null;
        try
        {
            logger.logInfo("persistPropertyMaintenanceRequest|inputParameters:%s", inputParameters );
            Gson jsonObj =  new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            PersistPropertyMaintenanceInputParameters inputObj = jsonObj.fromJson(inputParameters,PersistPropertyMaintenanceInputParameters.class);
            String errorJson = hasPersistPropertyMaintenanceRequestErrors( inputObj );
            if( errorJson.length() > 0 ){logger.logInfo("hasError:%s", errorJson);return errorJson;}
            
            RequestView requestView = requestServiceObj.persistPropertyMaintenanceRequest( inputObj);
            
            if(requestView == null || requestView.getRequestId()==null){json+= "{\"requestDetails\": {}}";}
            json=getRequestSavedSuccessJSON(requestView);            
            logger.logDebug("persistPropertyMaintenanceRequest|json:%s", json);
            return json;
            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("persistPropertyMaintenanceRequest-Finish");
        }
        return json;
    }
    
    @GET
    @Path("/checkStatus")    
    @Produces("application/json;charset=utf-8")
    public String checkStatus( @DefaultValue ("-1")  @QueryParam("devicePlatform")String devicePlatform
                               
                            )
    {
        String json="";
        try
        {
            json = UtilityService.IsEpayEnabled(devicePlatform);
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
                logger.logDebug("checkStatus-Finish");
        }
        return json;
    }
    @GET
    @Path("/setStatus")    
    @Produces("application/json;charset=utf-8")
    public String setStatus( 
                                @DefaultValue ("-1")  @QueryParam("devicePlatform")String devicePlatform,
                                @DefaultValue ("0")   @QueryParam("status")String status
                             )
    {
        String json="";
        try
        {
            logger.logInfo("setStatus|devicePlatform:%s|status:%s",devicePlatform,status);
            json = UtilityService.UpdateIsEpayEnabledCheck(devicePlatform,status);
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
    //            json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
                                                                       
        }
        finally
        {
                logger.logDebug("setStatus-Finish");
        }
        return json;
    }
    @GET
    @Path("/getPropertyNolRequestDetails")    
    @Produces("application/json;charset=utf-8")
    public String getPropertyNolRequestDetails(
                                                    @QueryParam("loginId")String loginId, 
                                                    @QueryParam("requestId")Long requestId
                                                    
                                             )
    {
        String json="";
        try
        {
            logger.logDebug("getPropertyMaintenanceRequestDetails|loginId:%s|requestId:%s",loginId,requestId);    
            json = RequestService.getPropertyNolRequestDetails(loginId,requestId);
        }
        catch(Exception e)
        {
            logger.LogException("getPropertyNolRequestDetails|Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        
        return json;

    }
    
    @GET
    @Path("/getRenewRequestDetails")    
    @Produces("application/json;charset=utf-8")
    public String getRenewRequestDetails(
                                                    @QueryParam("loginId")String loginId, 
                                                    @QueryParam("requestId")Long requestId
                                                    
                                             )
    {
        String json="";
        try
        {
            logger.logDebug("getRenewRequestDetails|loginId:%s|requestId:%s",loginId,requestId);    
            json = RequestService.getRenewRequestDetails(loginId,requestId);
        }
        catch(Exception e)
        {
            logger.LogException("getRenewRequestDetails|Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        
        return json;

    }
        
        
    private String hasErrorCancelContractRequest(CancelRequestInputParameters inputObj,DateFormat df   )throws Exception
    {
        StringBuilder errorJson = null;
        boolean hasError = false;
        if( 
            inputObj.getEvacuationDate() == null || inputObj.getEvacuationDate().trim().length() <= 0
          )
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.EVACUATION_DATE_REQUIRED).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( "msg.error.evacuationDateRequired", "en" )
                                                           ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( "msg.error.evacuationDateRequired", "ar" )
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        else
        {
            Contract c  =  EntityManager.getBroker().findById(Contract.class, inputObj.getContractId());
            
            Date startDate = df.parse ( df.format( c.getStartDate() ) );
            Date evacDate = df.parse (  inputObj.getEvacuationDate()  );
            if (  startDate.compareTo( evacDate )> 0 ) 
            {
                if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
                errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.EVACUATION_DATE_INVALID_RANGE).append( "," );
                errorJson=errorJson.append( errorMsgEn ).append(
                                                                 ResourceWrapper.getInstance().getProperty( "msg.error.evacDateMustBeInContractRange", "en" )
                                                               ).
                                                                append("\",");
                errorJson=errorJson.append( errorMsgAr ).append(
                                                                 ResourceWrapper.getInstance().getProperty( "msg.error.evacDateMustBeInContractRange", "ar" )
                                                               ).
                                                               append("\"");
                
                errorJson = errorJson.append("},");
                hasError = true;
            }
        }
        if( 
            inputObj.getDescription() == null || 
            inputObj.getDescription().trim().length() <= 0 
          )
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.APPOINTMENT_DESCRIPTION_REQUIRED ).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( "msg.error.descriptionRequired", "en" )
                                                           ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( "msg.error.descriptionRequired", "ar" )
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
    }
    @POST
    @Path("/persistCancelContractRequest")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String persistCancelContractRequest(String inputParameters)
    {
        String json="";
        try
        {
            logger.logInfo("persistCancelContractRequest|inputParameters:%s",inputParameters);    
            Gson jsonObj =  new GsonBuilder().create();
            CancelRequestInputParameters inputObj = jsonObj.fromJson(inputParameters,CancelRequestInputParameters.class);
            DateFormat df = new SimpleDateFormat( "dd/MM/yyyy" );
            String validationResult = hasErrorCancelContractRequest(inputObj, df);
            if(validationResult != null && validationResult.trim().length()>0) return validationResult;
            HashMap<String, Object> argMap = new HashMap<String, Object>(0);
            argMap.put("requestDescription", inputObj.getDescription());
            argMap.put("applicantId", String.valueOf( inputObj.getCreatedByPersonId()));
            argMap.put("requestId", String.valueOf( inputObj.getRequestId()));
            RequestView requestView = evacuationServiceObj.addEvacuationRequest(
                                                                                inputObj.getCreatedBy(), 
                                                                                inputObj.getContractId(), 
                                                                                df.parse(inputObj.getEvacuationDate()), 
                                                                                argMap
                                                                               );
                                            
            
            json=getRequestSavedSuccessJSON(requestView);
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStrequestListackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("persistCancelContractRequest-Finish");
        }
        return json;
    }    
    
    @GET
    @Path("/getCancelContractRequestDetails")    
    @Produces("application/json;charset=utf-8")
    public String getCancelContractRequestDetails(
                                                    @QueryParam("loginId")String loginId, 
                                                    @QueryParam("requestId")Long requestId,
                                                    @QueryParam("contractId")Long contractId
                                                    
                                             )
    {
        String json="";
        try
        {
            logger.logInfo("getCancelContractRequestDetails|loginId:%s|requestId:%s|contractId:%s",loginId,requestId,contractId);    
            json = RequestService.getCancelContractRequestDetails(loginId,requestId,contractId);
        }
        catch(Exception e)
        {
            logger.LogException("getCancelContractRequestDetails|Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        
        return json;

    }
    
    @POST
    @Path("/persistTransferContractRequest")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String persistTransferContractRequest(String inputParameters)
    {
        String json="";
        try
        {
            logger.logInfo("persistTransferContractRequest|inputParameters:%s",inputParameters);    
            Gson jsonObj =  new GsonBuilder().create();
            TransferRequestInputParameters inputObj = jsonObj.fromJson(inputParameters,TransferRequestInputParameters.class);
           
            String validationResult = hasErrorTransferContractRequest(inputObj, true);
            if(validationResult != null && validationResult.trim().length()>0) return validationResult;
            RequestView requestView = requestServiceObj.persistTransferContractRequest(inputObj);
            json=getRequestSavedSuccessJSON(requestView);
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStrequestListackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("persistTransferContractRequest-Finish");
        }
        return json;
    }    

    private String hasErrorTransferContractRequest(
                                                    TransferRequestInputParameters inputParameter,
                                                    boolean finalPersist
                                                  )
    {
        DateFormat df = new SimpleDateFormat( "dd/MM/yyyy" );
        StringBuilder errorJson= new StringBuilder("{\"errors\": [");
        boolean hasError = false;
        if( inputParameter.getEvacuationDate() == null ||inputParameter.getEvacuationDate().trim().length()<=0 )
        {
            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.EVACUATION_DATE_REQUIRED).append( "," );
            errorJson=errorJson.append( errorMsgEn ).append(
                                                             ResourceWrapper.getInstance().getProperty( "msg.error.evacuationDateRequired", "en" )
                                                           ).
                                                            append("\",");
            errorJson=errorJson.append( errorMsgAr ).append(
                                                             ResourceWrapper.getInstance().getProperty( "msg.error.evacuationDateRequired", "ar" )
                                                           ).
                                                           append("\"");
            
            errorJson = errorJson.append("},");
            hasError = true;
        }
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
    }
    
    @POST
    @Path("/validateAndGetPaymentsForTransferRequest")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String validateAndGetPaymentsForTransferRequest(
                                                            String inputParameters
                                                           )
    {
        String json="";
        try
        {
            logger.logDebug("validateAndGetPaymentsForTransferRequest|inputParameters:%s|",inputParameters);    
            Gson jsonObj =  new GsonBuilder().create();
            TransferRequestInputParameters inputObj = jsonObj.fromJson(inputParameters,TransferRequestInputParameters.class);
            
            String validationResult = hasErrorTransferContractRequest(inputObj, false);
            if(validationResult != null && validationResult.trim().length()>0) return validationResult;
            else
            {
                json = UtilityService.getDefaultPaymentsJSONForTransferContract(inputObj.getRequestId(),inputObj.getContractId(),"0",true);
            }
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        
        return json;
    } 
    
    @GET
    @Path("/getTransferContractRequestDetails")    
    @Produces("application/json;charset=utf-8")
    public String getTransferContractRequestDetails(
                                                      @QueryParam("loginId")String loginId, 
                                                      @QueryParam("requestId")Long requestId,
                                                      @QueryParam("contractId")Long contractId,
                                                      @DefaultValue("ar") @QueryParam("locale")String locale
                                                    )
    {
        String json="";
        try
        {
            logger.logInfo("getTransferContractRequestDetails|loginId:%s|requestId:%s|contractId:%s|locale:%s",loginId,requestId,contractId,locale);    
            json = RequestService.getTransferContractRequestDetails(loginId,requestId,contractId,locale);
        }
        catch(Exception e)
        {
            logger.LogException("getTransferContractRequestDetails    |Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        
        return json;

    }    
    
    @GET
    @Path("/getContract")    
    @Produces("application/json;charset=utf-8")
    public String getContract(
                                                      
                                                      
                                                      @QueryParam("contractId")Long contractId
                                                      
                                                    )
    {
        Contract contract = EntityManager.getBroker().findById(Contract.class, contractId);
        
        return contract.getContractNumber();
    }
    @GET
    @Path("/testUpdateContract")    
    
    public String testUpdateContract(
                                                      
                                                      @QueryParam("contractNumber")String contractNumber,
                                                      @QueryParam("contractId")Long contractId
                                                      
                                                    )
    {
        try
        {
         ApplicationContext.getContext().getTxnContext().beginTransaction();            
        Contract contract = EntityManager.getBroker().findById(Contract.class, contractId);
        contract.setContractNumber(contractNumber);
        EntityManager.getBroker().update(contract);
            ApplicationContext.getContext().getTxnContext().commit();
        }catch(Exception e)
        {
            ApplicationContext.getContext().getTxnContext().rollback();
            logger.LogException("Error", e);
            return "failure";
        }finally
        {
            ApplicationContext.getContext().getTxnContext().release();}
        return "";
    }
    @GET
    @Path("/testGetContract")    
    
    public String testGetContract(
                                                      
                                                      
                                                      @QueryParam("contractId")Long contractId
                                                      
                                                    )
    {
        Contract contract = EntityManager.getBroker().findById(Contract.class, contractId);
        
        return  contract.getContractNumber();
    }
    
    
    @GET
    @Path("/getReplaceChequePayments")    
    @Produces("application/json;charset=utf-8")
    public String getReplaceChequePayments(
                                                    @QueryParam("loginId")String loginId, 
                                                    @DefaultValue("10")@QueryParam("recordsPerPage")String recordsPerPage,
                                                    @DefaultValue("1") @QueryParam("currentPage")String currentPage,
                                                    @DefaultValue("-1")@QueryParam("contractId")Long contractId,
                                                    @DefaultValue("ae") @QueryParam("locale")String locale
                                             )
    {
        String result = "0";
        try
        {
            logger.logInfo("getReplaceChequePayments|loginId:%s|contractId:%s|locale:%s", loginId, contractId,locale);
            Calendar paymentStartFrom = GregorianCalendar.getInstance();
            paymentStartFrom.add(Calendar.DATE, 1);
            DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            
             result = PropertyService.getPaymentSchedulesList(
                                                                    locale, 
                                                                    loginId, 
                                                                    recordsPerPage, 
                                                                    currentPage, 
                                                                    contractId, 
                                                                    "-1", 
                                                                    -1l, 
                                                                    "-1", 
                                                                    -1l, 
                                                                    "-1", 
                                                                    Constant.PAYMENT_SCHEDULE_STATUS_COLLECTED_ID,
                                                                    df.format(paymentStartFrom.getTime()), 
                                                                    "-1", 
                                                                    -1l
                                                                 );
        } catch (Exception e) {
        logger.LogException("getReplaceChequePayments:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    
    
    private String hasErrorReplaceChequeRequest(
                                                    ReplaceChequeInputParameters inputParameter,
                                                    boolean finalPersist
                                                  )
    {
//        DateFormat df = new SimpleDateFormat( "dd/MM/yyyy" );
        StringBuilder errorJson= new StringBuilder("{\"errors\": [");
        boolean hasError = false;
//        if( inputParameter.getEvacuationDate() == null ||inputParameter.getEvacuationDate().trim().length()<=0 )
//        {
//            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
//            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.EVACUATION_DATE_REQUIRED).append( "," );
//            errorJson=errorJson.append( errorMsgEn ).append(
//                                                             ResourceWrapper.getInstance().getProperty( "msg.error.evacuationDateRequired", "en" )
//                                                           ).
//                                                            append("\",");
//            errorJson=errorJson.append( errorMsgAr ).append(
//                                                             ResourceWrapper.getInstance().getProperty( "msg.error.evacuationDateRequired", "ar" )
//                                                           ).
//                                                           append("\"");
//            
//            errorJson = errorJson.append("},");
//            hasError = true;
//        }
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
    }
        
    @POST
    @Path("/validateAndGetPaymentsForReplaceCheque")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String validateAndGetPaymentsForReplaceCheque(
                                                            String inputParameters
                                                        )
    {
        String json="";
        try
        {
            logger.logDebug("validateAndGetPaymentsForReplaceCheque|inputParameters:%s|",inputParameters);    
            Gson jsonObj =  new GsonBuilder().create();
            ReplaceChequeInputParameters inputObj = jsonObj.fromJson(inputParameters,ReplaceChequeInputParameters.class);
            
            String validationResult = hasErrorReplaceChequeRequest(inputObj, false);
            if(validationResult != null && validationResult.trim().length()>0) return validationResult;
            else
            {
                json = UtilityService.getDefaultPaymentsJSONForReplaceCheque(
                                                                                inputObj.getContractId(),
                                                                                Constant.REQUEST_TYPE_REPLACE_CHEQUE,
                                                                                inputObj.getPaymentSchduleIds(),
                                                                                true
                                                                            );
            }
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        
        return json;
    } 

    
    @POST
    @Path("/persistReplaceChequeRequest")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String persistReplaceChequeRequest(String inputParameters)
    {
        String json="";
        try
        {
            logger.logInfo("persistReplaceChequeRequest|inputParameters:%s",inputParameters);    
            Gson jsonObj =  new GsonBuilder().create();
            ReplaceChequeInputParameters inputObj = jsonObj.fromJson(inputParameters,ReplaceChequeInputParameters.class);
           
            String validationResult = hasErrorReplaceChequeRequest(inputObj, true);
            if(validationResult != null && validationResult.trim().length()>0) return validationResult;
            RequestView requestView = requestServiceObj.persistReplaceChequeRequest(inputObj,Constant.REQUEST_TYPE_REPLACE_CHEQUE);
            json=getRequestSavedSuccessJSON(requestView);
//            json = "{"+
//                  "\"result\": ["+
//                   " {"+
//                      "\"requestNumber\": \"RQ-16-0068003\","+
//                      "\"requestId\": \"64876\","+
//                      "\"statusEn\": \"New\","+
//                      "\"statusAr\": \"New\","+
//                      "\"msgEn\": \"Request has been successfully saved with number:RQ-16-0068003\","+
//                      "\"msgAr\":\"Request has been successfully saved with number:RQ-16-0068003\" " +
//                    "}"+
//                  "]"+
//                "}";

            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStrequestListackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("persistReplaceChequeRequest-Finish");
        }
        return json;
    }    
    @GET
    @Path("/getBounceChequePayments")    
    @Produces("application/json;charset=utf-8")
    public String getBounceChequePayments(
                                                    @QueryParam("loginId")String loginId, 
                                                    @DefaultValue("10")@QueryParam("recordsPerPage")String recordsPerPage,
                                                    @DefaultValue("1") @QueryParam("currentPage")String currentPage,
                                                    @DefaultValue("-1")@QueryParam("contractId")Long contractId,
                                                    @DefaultValue("ae") @QueryParam("locale")String locale
                                             )
    {
        String result = "0";
        try
        {
            logger.logInfo("getBounceChequePayments|loginId:%s|contractId:%s|locale:%s", loginId, contractId,locale);
            
             result = PropertyService.getPaymentSchedulesList(
                                                                    locale, 
                                                                    loginId, 
                                                                    recordsPerPage, 
                                                                    currentPage, 
                                                                    contractId, 
                                                                    "-1", 
                                                                    -1l, 
                                                                    "-1", 
                                                                    -1l, 
                                                                    "-1", 
                                                                    Constant.PAYMENT_SCHEDULE_STATUS_BOUNCED_ID,
                                                                    "-1" ,
                                                                    "-1", 
                                                                    -1l
                                                                 );
        } catch (Exception e) {
        logger.LogException("getBounceChequePayments:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    
    @POST
    @Path("/validateAndGetPaymentsForBounceCheque")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String validateAndGetPaymentsForBounceCheque(
                                                            String inputParameters
                                                        )
    {
        String json="";
        try
        {
            logger.logDebug("validateAndGetPaymentsForBounceCheque|inputParameters:%s|",inputParameters);    
            Gson jsonObj =  new GsonBuilder().create();
            ReplaceChequeInputParameters inputObj = jsonObj.fromJson(inputParameters,ReplaceChequeInputParameters.class);
            
            String validationResult = hasErrorReplaceChequeRequest(inputObj, false);
            if(validationResult != null && validationResult.trim().length()>0) return validationResult;
            else
            {
                json = UtilityService.getDefaultPaymentsJSONForReplaceCheque(
                                                                            inputObj.getContractId(),
                                                                             Constant.REQUEST_TYPE_PAY_BOUNCE_CHEQUE,
                                                                             inputObj.getPaymentSchduleIds(),
                                                                             true
                                                                             );
            }
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        
        return json;
    } 

    
    private String hasErrorBounceChequeRequest(
                                                    ReplaceChequeInputParameters inputParameter,
                                                    boolean finalPersist
                                                  )
    {
    //        DateFormat df = new SimpleDateFormat( "dd/MM/yyyy" );
        StringBuilder errorJson= new StringBuilder("{\"errors\": [");
        boolean hasError = false;
    //        if( inputParameter.getEvacuationDate() == null ||inputParameter.getEvacuationDate().trim().length()<=0 )
    //        {
    //            if( errorJson == null )errorJson= new StringBuilder("{\"errors\": [");
    //            errorJson=errorJson.append( errorCode ).append( Constant.MobileWebServiceErrorCodes.EVACUATION_DATE_REQUIRED).append( "," );
    //            errorJson=errorJson.append( errorMsgEn ).append(
    //                                                             ResourceWrapper.getInstance().getProperty( "msg.error.evacuationDateRequired", "en" )
    //                                                           ).
    //                                                            append("\",");
    //            errorJson=errorJson.append( errorMsgAr ).append(
    //                                                             ResourceWrapper.getInstance().getProperty( "msg.error.evacuationDateRequired", "ar" )
    //                                                           ).
    //                                                           append("\"");
    //
    //            errorJson = errorJson.append("},");
    //            hasError = true;
    //        }
        if( hasError )
        {
            return errorJson.toString().substring(0,errorJson.length()-1)+"]}";
        }
        else
         return "";
    }
    @POST
    @Path("/persistBounceChequeRequest")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String persistBounceChequeRequest(String inputParameters)
    {
        String json="";
        try
        {
            logger.logInfo("persistBounceChequeRequest|inputParameters:%s",inputParameters);    
            Gson jsonObj =  new GsonBuilder().create();
            ReplaceChequeInputParameters inputObj = jsonObj.fromJson(inputParameters,ReplaceChequeInputParameters.class);
           
            String validationResult = hasErrorBounceChequeRequest(inputObj, true);
            if(validationResult != null && validationResult.trim().length()>0) return validationResult;
            RequestView requestView = requestServiceObj.persistReplaceChequeRequest(inputObj,Constant.REQUEST_TYPE_PAY_BOUNCE_CHEQUE);
            json=getRequestSavedSuccessJSON(requestView);
    //            json = "{"+
    //                  "\"result\": ["+
    //                   " {"+
    //                      "\"requestNumber\": \"RQ-16-0068003\","+
    //                      "\"requestId\": \"64876\","+
    //                      "\"statusEn\": \"New\","+
    //                      "\"statusAr\": \"New\","+
    //                      "\"msgEn\": \"Request has been successfully saved with number:RQ-16-0068003\","+
    //                      "\"msgAr\":\"Request has been successfully saved with number:RQ-16-0068003\" " +
    //                    "}"+
    //                  "]"+
    //                "}";

            
        }
        catch(Exception e)
        {
            logger.LogException("Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStrequestListackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        finally
        {
            logger.logDebug("persistBounceChequeRequest-Finish");
        }
        return json;
    }    
    
    
    @GET
    @Path("/getBounceChequeRequestDetails")    
    @Produces("application/json;charset=utf-8")
    public String getBounceChequeRequestDetails(
                                                      @QueryParam("loginId")String loginId, 
                                                      @QueryParam("requestId")Long requestId,
                                                      
                                                      @DefaultValue("ar") @QueryParam("locale")String locale
                                                    )
    {
        String json="";
        try
        {
            logger.logInfo("getBounceChequeRequestDetails|loginId:%s|requestId:%s|locale:%s",loginId,requestId,locale);    
            json = RequestService.getBounceChequeRequestDetails(loginId,requestId,locale);
        }
        catch(Exception e)
        {
            logger.LogException("getBounceChequeRequestDetails    |Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        
        return json;

    }    
    
    @GET
    @Path("/getReplaceChequeRequestDetails")    
    @Produces("application/json;charset=utf-8")
    public String getReplaceChequeRequestDetails(
                                                      @QueryParam("loginId")String loginId, 
                                                      @QueryParam("requestId")Long requestId,
                                                      
                                                      @DefaultValue("ar") @QueryParam("locale")String locale
                                                    )
    {
        String json="";
        try
        {
            logger.logInfo("getReplaceChequeRequestDetails|loginId:%s|requestId:%s|locale:%s",loginId,requestId,locale);    
            json = RequestService.getReplaceChequeRequestDetails(loginId,requestId,locale);
        }
        catch(Exception e)
        {
            logger.LogException("getReplaceChequeRequestDetails    |Error occured", e);
            //json="{\"errors\": [\"stackTrace\":"+Utility.getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
        }
        
        return json;

    }  

    @GET
    @Path("/getUserDashboard")
    @Produces("application/json;charset=utf-8")
    public String getUserDashboard(
                                                    @QueryParam("loginId")String loginId,
                                                    @QueryParam("locale")String locale
                                  )
    {
        
        String json= "0";
        try
        {
            logger.logInfo("getUserDashboard|loginId:%s|locale:%s",loginId,locale);    
            json = UtilityService.getUserDashboard(loginId,locale);
                                  
                                  
        } catch (Exception e) 
        {
            logger.LogException("getUserDashboard:", e);
            //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
            json="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"en")+"\""+
                                             ",\"errorMsgAr\":\""+ ResourceWrapper.getInstance().getProperty(Constant.MobileWebServiceErrorCodes.COMMON_ERROR_MSG,"ar")+"\"}]}";
           
        }
        return json;
    }    
    

    @GET
    @Path("/getCountries")
    @Produces("application/json;charset=utf-8")
    public String getCountries()
    {
        
        String json= "0";
        try
        {
            logger.logInfo("getCountries|");    
            json = UtilityService.getCountries();
                                  
                                  
        } catch (Exception e) 
        {
            logger.LogException("getCountries:", e);
            json="{\"message\": {\"msgCode\":\"000\",\"msgEn\":\"An error has occured please try again later\""+
                                                                 ",\"msgAr\":\"An error has occured please try again later\"}}";
           
        }
        return json;
    }    
    
    

    @GET
    @Path("/getUserDetails")
    @Produces("application/json;charset=utf-8")
    public String getUserDetails(  @QueryParam("loginId")String loginId )
    {
        
        String json= "0";
        try
        {
            logger.logInfo("userDetails|loginId:%s",loginId);    
//            UserDetails userDetail = UtilityService.getUserDetailsFromLoginId(loginId);
            json = getPersonFromLoginId( loginId );
        } catch (Exception e) 
        {
            logger.LogException("getUserDetails:", e);
            json="{\"message\": {\"msgCode\":\"000\",\"msgEn\":\"An error has occured please try again later\""+
                                                                 ",\"msgAr\":\"An error has occured please try again later\"}}";
           
        }
        return json;
    }    
    
    
        
}

 

