package com.emitac.Utils;

public class Utility {
    
    public static String getStackTrace(Exception exception) 
    {
           
           
           StringBuilder sb = new StringBuilder(""); 
           String lineSeparator = System.getProperty("line.separator");
           
           sb.append(lineSeparator);
           sb.append("Caused by:: ").append(exception.getCause()).append(lineSeparator);
           lineSeparator += "\t";
           
           for (StackTraceElement st : exception.getStackTrace()) {
               sb.append(st.toString() + lineSeparator);
           }
           
           return sb.toString();
    
       }
    
}
