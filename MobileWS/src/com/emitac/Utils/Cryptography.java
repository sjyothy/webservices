package com.emitac.Utils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class Cryptography 
{
        
       private static final String IsoAnsiLatinI = "ISO-8859-1"; // ISO ANSI LATIN I
       

       private static final String initializingVector = "AMAF@DG#";
       private static final String CipherMode = "DESede/CBC/PKCS5Padding";
       private byte[] key;
    
       
       public Cryptography()
       {
           this.key = ("AMAF#Key$M0BiLE!WS@DG_1@").getBytes();
       }
       
    
       public String encrpyt(String msg)throws Exception
       {
           
           byte[] plaintext =msg.getBytes();
           
           System.out.println("Length of msg(bytes) :"+plaintext.length );
           
           Cipher cipher = getCipher(Cipher.ENCRYPT_MODE);
            // Encrypt the data
           byte[] encrypted = cipher.doFinal(plaintext);
           
           // Convert to String
           return toHexLiteral(encrypted);
       }
       public static String toHexLiteral(byte[] input)
       {
           
         String hexString = "";

         for (int i=0; i<input.length; i++) 
         {
             String temp = Integer.toHexString(input[i] & 0xFF);
             hexString += (temp.length()<2) ? ("0" + temp) : temp;
         }
         return hexString;
       }

       
       public String decrypt(String input) throws Exception
       {
       
           String retVal;
           try 
           {

               Cipher cipher = getCipher(Cipher.DECRYPT_MODE);
               retVal = new String(cipher.doFinal(fromHEXLiteral(input)));
               
           } catch (Exception e) {

                   throw e;
           }
                   
           return retVal;
       }

    private Cipher getCipher( int cipherMode) throws Exception 
    {
           Cipher cipher = Cipher.getInstance(CipherMode);
           //            Create an initialization vector (necessary for CBC mode)
           IvParameterSpec IvParameters = new IvParameterSpec( initializingVector.getBytes() );
               // Create a DESede key spec from the key
           DESedeKeySpec spec = new DESedeKeySpec(this.key);

           // Get the secret key factor for generating DESede keys
           SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");

           // Generate a DESede SecretKey object
           SecretKey theKey = keyFactory.generateSecret(spec);

           cipher.init(cipherMode, theKey, IvParameters);
           return cipher;
    }
       
       public static byte[] fromHEXLiteral(String input) 
       {
           
           input.toLowerCase();
           byte[] bts = new byte[input.length() / 2];
           
           for (int i=0; i<bts.length; i++) {
               
              bts[i] = (byte) Integer.parseInt(input.substring(2*i, 2*i+2), 16);
           }
           return bts;
       }

       // This main function is added for testing purpose. It will be removed later
       public static void main(String[] arr) {
           
           Cryptography c = new Cryptography();
           try
           {
//                   String enc =  c.encrpyt("{ \"userDetails\":{ \"loginId\":\"YazanAtis1\", \"password\":\"Zaqwsxzaq\", \"userName\":\"YazanAti\",\"dateOfBirth\":\"23/09/1900\", \"gender\":\"m\",\"emiratesId\":\"098765432112344\",\"cellNumber\":\"009711234567\", \"email\":\"a12@a.com\", \"nationality\":\"20\" },\"roles\":[ { \"roleId\":\"JOBSEEKER\" } ] }");
                   String enc= c.encrpyt("madya123;i70788i");
                   System.out.println(enc);
//                   String dec = c.decrypt("9a7c7f5ae26c2e692d4a9afd3fd26d11");
////               
//                   System.out.println(dec);
//                    dec = dec;
           }
           catch(Exception e)
           {
               e.printStackTrace();
           }
       }
}
