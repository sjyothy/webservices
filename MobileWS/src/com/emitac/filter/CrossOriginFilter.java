package com.emitac.filter;

import java.io.IOException;

import com.avanza.core.util.Logger;

import com.avanza.core.web.config.LocaleInfo;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CrossOriginFilter implements Filter{
        
        private static final Logger logger = Logger.getLogger(CrossOriginFilter.class);

        @Override
        public void init(FilterConfig filterConfig) throws ServletException {
            // TODO Implement this method
        }
        public void destroy() {}
        public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
                    //logger.logDebug("||||| On request filter |||||");
                    
                    try{
                        applyFilters(request, response, filterChain);
                        
                    }
                    catch(Exception e)
                    {
                        logger.LogException("Error Occured:",e) ;
                    }
        }
        
        private void applyFilters(ServletRequest request, ServletResponse response,     FilterChain filterChain) throws ServletException, IOException {
            
            if (request instanceof HttpServletRequest) {
                    
                    HttpServletRequest req = (HttpServletRequest) request;
                    HttpServletResponse res = (HttpServletResponse) response;
                    processRequest(req, res, filterChain);
            }
        }
         
        private void processRequest(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {
                            
                            addCustomHeaders(request, response);
                            filterChain.doFilter(request, response);                    
        }   
        
        private void addCustomHeaders(HttpServletRequest servRequest, HttpServletResponse httpServletResponse) {
        
                    httpServletResponse.setHeader("pragma", "no-cache");
                    httpServletResponse.setHeader("cache-control", "no-cache");
                    httpServletResponse.setHeader("cache-control", "no-store");
                    httpServletResponse.setHeader("Cache-Control", "private"); // HTTP 1.1
                    httpServletResponse.setHeader("Cache-Control", "max-stale=0"); // HTTP 1.1 
                    httpServletResponse.setHeader("expires", "0");
                    httpServletResponse.setHeader("Access-Control-Allow-Origin", "*");
        
            }
}
