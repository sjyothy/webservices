package com.emitac.listener;

import com.avanza.core.main.ApplicationLoader;

import java.util.logging.Logger;
import com.avanza.core.main.ApplicationLoader;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ApplicationListener implements ServletContextListener {
   
    com.avanza.core.util.Logger logger = com.avanza.core.util.Logger.getLogger(ApplicationListener.class);
            
    public ApplicationListener() {
    }
    
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try
        {
            
            logger.logInfo("ApplicationListener - Start");    
            if( servletContextEvent == null ) 
            {
                logger.logInfo("ApplicationListener - ServletContextEvent is null;");        
            }
            else if( servletContextEvent.getServletContext() == null ) 
            {
                logger.logInfo("ApplicationListener - ServletContextEvent.getServletContext() is null;");        
            }
            
            String path=servletContextEvent.getServletContext().getRealPath("/")+ "resources\\av-conf.xml";
            logger.logInfo("ApplicationListener-Path:"+path);    
            
            ApplicationLoader.load(path);   
            logger.logInfo("ApplicationListener-Finish:");    
        }
        catch(Exception e)
        {
           logger.LogException("Error Occured", e);    
        }
                
                
        
                
         
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ApplicationLoader.destroy();
        
    }
    
}
