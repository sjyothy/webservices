package com.avanza.pims.report.criteria;

import com.avanza.core.constants.CoreConstants;
import com.avanza.core.data.ApplicationContext;
import com.avanza.core.web.WebContext;
import com.avanza.core.web.config.LocaleInfo;

public abstract class AbstractReportCriteria implements IReportCriteria {
	
	private String reportFileName;
	private String reportProcessorClassName;
	private String reportGeneratedBy;
	private String dateFormat;
	
	  public  String getDateFormat()
		{
	    	WebContext webContext =  ApplicationContext.getContext().get(WebContext.class);
			LocaleInfo localeInfo = webContext.getAttribute(CoreConstants.CurrentLocale);
			return localeInfo.getDateFormat();
			
		}  
	public AbstractReportCriteria(){}
	public AbstractReportCriteria(String reportFileName, String reportProcessorClassName) {
		this.reportFileName = reportFileName;
		this.reportProcessorClassName = reportProcessorClassName;
	}
	
	public AbstractReportCriteria(String reportFileName, String reportProcessorClassName, String reportGeneratedBy) {
		this.reportFileName = reportFileName;
		this.reportProcessorClassName = reportProcessorClassName;
		this.reportGeneratedBy = reportGeneratedBy;
	}

	public String getReportFileName() {
		return reportFileName;
	}

	public void setReportFileName(String reportFileName) {
		this.reportFileName = reportFileName;
	}

	public String getReportProcessorClassName() {
		return reportProcessorClassName;
	}

	public void setReportProcessorClassName(String reportProcessorClassName) {
		this.reportProcessorClassName = reportProcessorClassName;
	}

	public String getReportGeneratedBy() {
		return reportGeneratedBy;
	}

	public void setReportGeneratedBy(String reportGeneratedBy) {
		this.reportGeneratedBy = reportGeneratedBy;
	}
	
	
	
}
