package com.avanza.pims.report.criteria;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;


public class StatementOfAccountCriteria extends AbstractReportCriteria
{
	private Long personId;
	private Long inheritanceBeneficiaryId;
	private Long inheritancefileId;
	private Long endowmentId;
	private Long masrafId;
	private Date fromDate;
	private Date toDate;
	private String formattedFromDate;
	private String formattedToDate;
	private String showSubMasrafTrx;
	
	private List<String> costCenters = new ArrayList<String>();
	DateFormat formatterWithOutTime = new SimpleDateFormat("dd/MM/yyyy");
	DateFormat formatterReportDisplay = new SimpleDateFormat("MMM, dd yyyy");

	//For mobile webservice statement of account
	public StatementOfAccountCriteria()
	{


	}

	public StatementOfAccountCriteria(String reportFileName,String reportProcessorClassName,String reportGeneratedBy)
	{
		super(reportFileName, reportProcessorClassName,reportGeneratedBy);

	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public Long getInheritanceBeneficiaryId() {
		return inheritanceBeneficiaryId;
	}

	public void setInheritanceBeneficiaryId(Long inheritanceBeneficiaryId) {
		this.inheritanceBeneficiaryId = inheritanceBeneficiaryId;
	}

	public Long getInheritancefileId() {
		return inheritancefileId;
	}

	public void setInheritancefileId(Long inheritancefileId) {
		this.inheritancefileId = inheritancefileId;
	}

	public List<String> getCostCenters() {
		return costCenters;
	}

	public void setCostCenters(List<String> costCenters) {
		this.costCenters = costCenters;
	}

	public Long getEndowmentId() {
		return endowmentId;
	}

	public void setEndowmentId(Long endowmentId) {
		this.endowmentId = endowmentId;
	}

	public Long getMasrafId() {
		return masrafId;
	}

	public void setMasrafId(Long masrafId) {
		this.masrafId = masrafId;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		
		this.fromDate = fromDate;
		if( this.fromDate != null )
		{
			formattedFromDate = formatterWithOutTime.format(this.fromDate)+" 00:00:00";
		}
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
		if( this.toDate  != null )
		{
			formattedToDate = formatterWithOutTime.format(this.toDate )+" 23:59:59";
		}
	}

	public String getFormattedFromDateForReportDisplay() {
		if(this.fromDate != null )
		{
			java.util.Calendar cal  = GregorianCalendar.getInstance();
			cal.setTime(this.fromDate );
			cal.add(java.util.Calendar.DATE, -1);
		return formatterReportDisplay.format(  cal.getTime() );
		}
		return "";
	}

	public String getFormattedToDateForReportDisplay() {
		return formatterReportDisplay.format(this.toDate );
	}
	public String getFormattedFromDate() {
		return formattedFromDate;
	}

	public String getFormattedToDate() {
		return formattedToDate;
	}

	public String getShowSubMasrafTrx() {
		return showSubMasrafTrx;
	}

	public void setShowSubMasrafTrx(String showSubMasrafTrx) {
		this.showSubMasrafTrx = showSubMasrafTrx;
	}
	
	
}

