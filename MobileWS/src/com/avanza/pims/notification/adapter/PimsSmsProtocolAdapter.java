package com.avanza.pims.notification.adapter;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.avanza.core.util.Logger;
import com.avanza.core.util.configuration.ConfigSection;
import com.avanza.core.util.configuration.NameValueConfigSesction;
import com.avanza.notificationservice.adapter.AdapterStatus;
import com.avanza.notificationservice.adapter.NotificationAdapterBase;
import com.avanza.notificationservice.adapter.ProtocolAdapter;
import com.avanza.notificationservice.notification.Notification;
import com.avanza.notificationservice.notification.NotificationState;

/**
 * 
 * @author Anil Verani
 *
 */
public class PimsSmsProtocolAdapter extends NotificationAdapterBase implements ProtocolAdapter 
{
	private static Logger logger;	
	private ConfigSection adapterConfig; 
	
	public PimsSmsProtocolAdapter() 
	{
		logger = Logger.getLogger( PimsSmsProtocolAdapter.class );		
		adapterConfig = new NameValueConfigSesction();
	}
	
	public void initialize(ConfigSection adapterConfig) 
	{
		final String METHOD_NAME = "initialize()";
		
		logger.logInfo( METHOD_NAME + "  --- STARTED ---- " );
		
		this.adapterConfig = adapterConfig;		
		super.setStatus( AdapterStatus.Running );
		
		logger.logInfo( METHOD_NAME + " --- COMPLETED ---- " );
	}

	public void notify(Notification notification) 
	{
		final String METHOD_NAME = "notify()";
		
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED ---- " );
			
			String smsXMLRequest = new String( createSmsXmlRequest( notification ).getBytes(), "UTF-8" );
			String smsServiceURL = adapterConfig.getTextValue( "smsServiceURL" ) + smsXMLRequest;
			smsServiceURL = smsServiceURL.replaceAll( " ", "%20" );
			URL url = new URL( smsServiceURL );
			HttpURLConnection httpURLConnection = ( HttpURLConnection ) url.openConnection();
			httpURLConnection.setRequestMethod( "GET" );
			httpURLConnection.setAllowUserInteraction( false );
			httpURLConnection.setDoInput( true );
			InputStream inputStream = httpURLConnection.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader( inputStream, "UTF-8" );
			BufferedReader bufferedReader = new BufferedReader( inputStreamReader );			
			String smsXMLResponse = "";
			String tempStr = bufferedReader.readLine();
			while (tempStr != null) 
			{
				smsXMLResponse += tempStr;
				tempStr = bufferedReader.readLine();
			}			
			parseSmsXmlResponse( notification, smsXMLResponse );
			
			logger.logInfo( METHOD_NAME + " --- COMPLETED ---- " );
		}
		catch ( Exception exception )
		{
			logger.LogException( METHOD_NAME + " --- EXCEPTION --- ", exception);
			
			notification.setNotificationState( NotificationState.Failed );			
			notification.setStatusText( "Exception while sending SMS" ); 
			notificationUpdate.update( notification );
		}	
	}

	private void parseSmsXmlResponse(Notification notification, String smsXMLResponse) 
	{
		final String METHOD_NAME = "parseSmsXmlResponse()";
		
		logger.logInfo( METHOD_NAME + " --- STARTED ---- " );
		
		logger.logInfo( METHOD_NAME + " --- SMS Service XML Response --- " + smsXMLResponse );
		
		String[] smsXMLResponseArr = smsXMLResponse.split("</HTML>");
		
		notification.setNotificationState( NotificationState.Sent );
		notification.setStatusText( "SMS Sent Successfully" );
		
		String statusStartTag = "<STATUS>";
		String statusEndTag = "</STATUS>";
		
		for ( String smsResponse : smsXMLResponseArr )
		{
			int indexOfStatusStartTag = smsResponse.indexOf( statusStartTag );
			int indexOfStatusEndTag = smsResponse.indexOf( statusEndTag );			
			String statusValue = smsResponse.substring(indexOfStatusStartTag + statusStartTag.length(), indexOfStatusEndTag);
			
			if ( statusValue.equalsIgnoreCase("NACK") )
			{
				notification.setNotificationState( NotificationState.Failed );
				notification.setStatusText( "SMS Could Not Be Sent : " + smsResponse );
				logger.logInfo( "SMS Could Not Be Sent : " + smsResponse );				
				break;
			}
		}
		
		notificationUpdate.update( notification );
		
		logger.logInfo( METHOD_NAME + " --- COMPLETED ---- " );
	}

	private String createSmsXmlRequest(Notification notification) throws Exception
	{
		StringBuilder stringBuilder = new StringBuilder();
		
		stringBuilder.append("?MSG=");
		stringBuilder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		stringBuilder.append("<Request>");
		stringBuilder.append("<Header>");
		stringBuilder.append("<FusionInfo>");
		stringBuilder.append("<Process ID=\"0\" Name=\"\"/>");
		stringBuilder.append("<Activity ID=\"0\" Name=\"\"/>");
		stringBuilder.append("<TransactionID>0</TransactionID>");
		stringBuilder.append("</FusionInfo>");
		stringBuilder.append("<DepartmentInfo>");
		stringBuilder.append("<Sender>");
		stringBuilder.append("<Id>10260005</Id>");
		stringBuilder.append("</Sender>");
		stringBuilder.append("<Receiver>");
		stringBuilder.append("<Id>10260003</Id>");
		stringBuilder.append("</Receiver>");
		stringBuilder.append("<Transaction>");
		stringBuilder.append("<Type>1</Type>");
		stringBuilder.append("<ProcessType>SySingle_PR</ProcessType>");
		stringBuilder.append("<Date/>");
		stringBuilder.append("<ReferenceNumber/>");
		stringBuilder.append("</Transaction>");
		stringBuilder.append("</DepartmentInfo>");
		stringBuilder.append("</Header>");
		stringBuilder.append("<Body>");
		stringBuilder.append("<bulk_msg>");
		stringBuilder.append("<user_id>" + adapterConfig.getTextValue("smsServiceUserId").trim() + "</user_id>");
		stringBuilder.append("<password>" + new PasswordEncrypter( PasswordEncrypter.DES_ENCRYPTION_SCHEME ).decrypt( adapterConfig.getTextValue("smsServicePassword").trim().replaceAll("EQUAL", "=") ) + "</password>");
		stringBuilder.append("<dept_id>" + "" + "</dept_id>");
		stringBuilder.append("<message>");
		stringBuilder.append("<title>" + "" + "</title>");
		stringBuilder.append("<count>" + notification.getRecipientTo().size() + "</count>");
		stringBuilder.append("<body1>" + "|*UCS2|" + getArabicEncoding( notification.getBody().trim().replaceAll("'", "''") ) + "</body1>");
		
		for ( int msgId = 0; msgId < notification.getRecipientTo().size(); msgId++ )
		{
			stringBuilder.append("<values>");
			stringBuilder.append("<msg_id>" + msgId + "</msg_id>");
			stringBuilder.append("<mobile_no>" + notification.getRecipientTo().get( msgId ).getAddress().trim().replaceAll("-", "") + "</mobile_no>");
			stringBuilder.append("<param id=\"a1\"></param>");
			stringBuilder.append("<param id=\"a2\"></param>");
			stringBuilder.append("</values>");
		}
			
		stringBuilder.append("</message>");
		stringBuilder.append("</bulk_msg>");
		stringBuilder.append("</Body>");
		stringBuilder.append("</Request>");
		
		return stringBuilder.toString();
	}
	
	private String getArabicEncoding(String msg)
	{
		final String METHOD_NAME = "getArabicEncoding()";
		String msgEncoding = "";
		
		try
		{
			logger.logInfo( METHOD_NAME + " --- STARTED ---- " );
			
			byte[] byteMessage = msg.trim().getBytes("UTF-16");
			String strByteToHex;
			for(int ndx = byteMessage.length-1 ;ndx>=0;ndx--)
			{
				strByteToHex = byteToHex(byteMessage[ndx]);				
				msgEncoding = strByteToHex + msgEncoding ;				
			}
			
			logger.logInfo( METHOD_NAME + " --- COMPLETED ---- " );
		}
		catch(Exception ex)
		{
			logger.LogException( " --- EXCEPTION --- ", ex );
		}
	
		if(msgEncoding.startsWith("feff"))
		 	 return msgEncoding.substring(4); 
		else			
			return  msgEncoding;
	}
	
	private String byteToHex(byte bytes) 
	{
		char hexDigit[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		char[] array = { hexDigit[(bytes >> 4) & 0x0f], hexDigit[bytes & 0x0f] };
		String strMessage = new String(array);
		return strMessage;
	}
	
	public void reInitialize() 
	{	
		final String METHOD_NAME = "reInitialize()";		
		logger.logInfo( METHOD_NAME + " --- STARTED ---- " );		
		initialize( adapterConfig );		
		logger.logInfo( METHOD_NAME + " --- COMPLETED ---- " );
	}

	public void notify2(Notification notification) 
	{	
		final String METHOD_NAME = "notify2()";
		logger.logInfo( METHOD_NAME + " --- STARTED ---- " );
		logger.logInfo( METHOD_NAME + " --- COMPLETED ---- " );
	}
	
	public void dispose() 
	{
		final String METHOD_NAME = "dispose()";
		logger.logInfo( METHOD_NAME + " --- STARTED ---- " );
		logger.logInfo( METHOD_NAME + " --- COMPLETED ---- " );
	}
}
