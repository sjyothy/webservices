package com.emitac;

import com.avanza.core.data.HibernateDataBroker;

import com.avanza.core.util.Logger;


import com.google.gson.Gson;

import com.google.gson.GsonBuilder;

import com.google.gson.reflect.TypeToken;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;

import java.lang.reflect.Type;

import java.sql.Blob;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.commons.codec.binary.Base64;

import org.hibernate.Hibernate;

public class FileDownloadServlet extends HttpServlet {
    private static final String CONTENT_TYPE = 
        "text/html; charset=windows-1252";
    private Logger logger = null;
    private static final int DEFAULT_BUFFER_SIZE = 10240;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        logger = Logger.getLogger(FileDownloadServlet.class);
    }

    public void doGet(HttpServletRequest request, 
                      HttpServletResponse response) throws ServletException, 
                                                           IOException {
        logger.logInfo("doGet");
        response.setContentType(CONTENT_TYPE);
        BufferedInputStream input = null;
        BufferedOutputStream output = null;
        byte[] buffer = null;
        byte[] base64EncodedStringByteArray = null;
        byte[] decodedFileByteArray = null;
        String base64EncodedString = "";
        long documentId;
        try {
            if (request.getParameter("documentId") != null) {
                logger.logInfo("documentId:%s", 
                               request.getParameter("documentId"));
                documentId = 
                        Long.parseLong(request.getParameter("documentId").toString());
                String documentJSON = 
                    Service.callUrl("documentDetails?documentId=" + 
                                    documentId);
                logger.logInfo("documentJSON:%s", documentJSON);
                Gson jsonObj = new GsonBuilder().create();


                DocumentView documentView = 
                    jsonObj.fromJson(documentJSON, DocumentView.class);

                logger.logInfo("documentView.getDocumentId():%s|documentView.getDocumentFileName():%s", 
                               documentView.getDocumentId(), 
                               documentView.getDocumentFileName());
                base64EncodedString = 
                        Service.callUrl("documentContents?documentId=" + 
                                        documentId);
                base64EncodedStringByteArray = base64EncodedString.getBytes();
                decodedFileByteArray = 
                        Base64.decodeBase64(base64EncodedStringByteArray);
                Blob file = Hibernate.createBlob(decodedFileByteArray);
                input = 
                        new BufferedInputStream(file.getBinaryStream(), DEFAULT_BUFFER_SIZE);
//                response.setHeader("Content-disposition", 
//                                   "attachment; filename=\"" + 
//                                   documentView.getDocumentFileName() + "\"");
                response.setContentType(documentView.getContentType());
                response.setHeader("Content-disposition", 
                                   "inline; filename=\"" + 
                                   documentView.getDocumentFileName() + "\"");
                output = 
                        new BufferedOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE);
                buffer = new byte[DEFAULT_BUFFER_SIZE];
                for (int length; (length = input.read(buffer)) != -1; ) {
                    output.write(buffer, 0, length);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        finally {
            output.flush();
            close(output);
            close(input);
            buffer = null;
            base64EncodedStringByteArray = null;
            decodedFileByteArray = null;
            base64EncodedString = null;

        }

    }

    private static void close(Closeable resource) {
        if (resource != null) {
            try {
                resource.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
