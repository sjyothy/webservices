package com.emitac;

import com.avanza.core.util.Logger;

import com.emitac.FileDownloadServlet;
import com.emitac.Service;

import com.google.gson.Gson;

import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import java.net.URLEncoder;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.DefaultFileItemFactory;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.io.IOUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

public class PersistSocialResearch extends HttpServlet {
    
    private static Logger     logger = Logger.getLogger(PersistSocialResearch.class);
    

    public void init(ServletConfig config) throws ServletException 
    {
    
    }

    public void doPost(
                        HttpServletRequest request,   HttpServletResponse response
                      ) throws ServletException, IOException 
    {

        try 
        {
            String jsonResponse = "";
            logger.logInfo(
                            "doPost|inputParameters:%s|", request.getParameter("inputParameters")
                          );

            
            String inputParameters = request.getParameter("inputParameters");
            
            String responseMessage = Service.callUrlJSON(
                                                          "persistSocialResearch",inputParameters
                                                        );

            logger.logInfo(
                            "doPost|responseMessage:%s|", responseMessage
                          );
            PrintWriter out = response.getWriter();
            logger.logInfo("Final Response JSON:%s", responseMessage);
            out.print(responseMessage);
        } catch (Exception e) {
            logger.LogException("Exception occured", e);
            System.out.println(e);
        }
    }


}
