package com.emitac;


import com.emitac.*;

import javax.ws.rs.Path;
import com.avanza.core.util.Logger;

import java.io.File;

import java.net.URLEncoder;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import javax.servlet.http.HttpServletResponse;


import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import javax.ws.rs.core.MediaType;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;


@Path("Service")

public class Service {
    public Service() {
    }
    
    private static final Logger logger = Logger.getLogger(Service.class);
    
    //production url
//    public static final String url = "http://192.168.2.80:9001/AMAFMobileWebServiceOne/resources/Service/";
//    public static final String url = "http://192.168.2.80:9001/AMAFMobileWebServiceTwo/resources/Service/";
 //test url
         public static final String url = "http://192.168.2.80:9001/AMAFMobileWebServiceTest/resources/Service/" ;

     
     
    //    public static final String url = "http://localhost:6666/AMAFMobileWSApp-MobileWS-context-root/resources/Service/";
    public static void main(String[] args) throws Exception 
    {
       Service ws = new Service();
        String response ="";  
        response = ws.testService ();
      System.out.println("RESPONSE FROM WS:"+response);
    }
//
//    public  void multiPartFileUploader() 
//    {
//            String charset = "UTF-8";
//            try 
//            {
//                String queryString =        
//    //
//                                                        "documentTitle="+URLEncoder.encode( "test ploading 123456", "UTF-8")+
//                                                        "&associatedObjectId=43183" +
//                                                        "&procedureTypeId=memsNoObjectionLetter" +
//                                                        "&loggedInUser=pims_admin" +
//                                                        "&comments="+URLEncoder.encode( "test ploading comments", "UTF-8")
//                                                        ;
//    //                String requestURL = "http://localhost:7777/MobileClient/upload?" +queryString;
//                    String requestURL = "http://esvapps.amaf.ae/AMAFMobileWSAppClient/upload?" +queryString;
//    
//                logger.logInfo(requestURL);
//                com.emitac.MultipartUtility multipart = new com.emitac.MultipartUtility( requestURL, charset);
//                
//    //                multipart.addFormField("associatedObjectId", "43183");
//    //                multipart.addFormField("procedureTypeId", "memsNoObjectionLetter");
//    //                multipart.addFormField("loggedInUser", "pims_admin");
//    //                multipart.addFormField("comments", "testing file upload");
//                
//              File uploadFile1 = new File("C:\\Users\\danish_farooq\\Desktop\\Amaf Network.png");
//    //           File uploadFile2 = new File("C:\\Users\\danish_farooq\\Desktop\\Requests JSON.txt");
//            
//                multipart.addFilePart("fileUpload", uploadFile1);
//    //                multipart.addFilePart("fileUpload", uploadFile2);
//                List<String> response = multipart.finish();
//                 
//                System.out.println("SERVER REPLIED:");
//                 
//                for (String line : response) 
//                {
//                    System.out.println(line);
//                }
//            } catch (Exception ex) 
//            {
//                System.err.println(ex);
//            }
//        }
    
    public String testService() throws Exception {

        String result = "0";
        try 
        {
            String json="{\"tenantId\":\"34218\",\"orderBy\":\"contracts.contractId\"}";
            result=leaseContractList(json)            ;
//            result =callUrlJSON("persistNormalDisbursementRequest",json);
            logger.logInfo(result);
     
        } catch (Exception e) {
            logger.LogException("testService:", e);
    //            result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
             result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                                  ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";

        }
        return result;

    }
    
    
       
       public String getStackTrace(Exception exception) 
       {
              
              
              StringBuilder sb = new StringBuilder(""); 
              String lineSeparator = System.getProperty("line.separator");
              
              sb.append(lineSeparator);
              sb.append("Caused by:: ").append(exception.getCause()).append(lineSeparator);
              lineSeparator += "\t";
              
              for (StackTraceElement st : exception.getStackTrace()) {
                  sb.append(st.toString() + lineSeparator);
              }
              
              return sb.toString();
       
          }
       
       public static String callUrlJSON(String urlMethod,String postInfo) throws RuntimeException, Exception 
       {

           CloseableHttpClient httpclient = HttpClients.createDefault();
           String callUrl = url +urlMethod;
           logger.logInfo("Url To Call:" + callUrl);
           HttpPost request = new HttpPost(callUrl);
           request.setHeader("Accept-Charset", "UTF-8");
           request.addHeader("content-type", "application/json");
           request.addHeader("charset", "utf-8");
           
           StringEntity params = new StringEntity(postInfo,"UTF-8");
           request.setEntity(params);

           CloseableHttpResponse response1 = httpclient.execute(request);
           try {

               HttpEntity entity = response1.getEntity();
               if (entity != null) {
                   return EntityUtils.toString(entity, HTTP.UTF_8).replace("&lt;","<").replace("&gt;",">");
               }


           } finally {
               response1.close();
           }
           return "";

       }
       
       public static String callUrl(String urlMethod) throws RuntimeException, 
                                                       Exception 
       {

           CloseableHttpClient httpclient = HttpClients.createDefault();
           String callUrl = url +urlMethod;
           logger.logInfo("Url To Call:" + callUrl);
           HttpGet request = new HttpGet(callUrl);
           request.setHeader("Accept-Charset", "iso-8859-1, unicode-1-1;q=0.8");
    //        request.addHeader("content-type", "application/x-www-form-urlencoded");
           CloseableHttpResponse response1 = httpclient.execute(request);
           try {

               HttpEntity entity = response1.getEntity();
               if (entity != null) {
                   return EntityUtils.toString(entity, HTTP.UTF_8).replace("&lt;","<").replace("&gt;",">");
               }


           } finally {
               response1.close();
           }
           return "";

       }

    public String testServiceAnother() throws Exception {

        String result = "0";
        try 
        {
            result =callUrl("newsDetails?newsId="+1);
            result ="124";
            logger.logInfo(result);
     
        } catch (Exception e) {
            logger.LogException("testService:", e);
            result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                                 ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }


    public String testServiceInputParameter(Integer newsId) throws Exception {

        String result = "0";
        try 
        {
            
            result =callUrl("newsDetails?newsId="+newsId);
            logger.logInfo(result);
     
        } catch (Exception e) {
            logger.LogException("testServiceInputParameter:", e);
    //            result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
              result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                      ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
            
        }
        return result;

    }
    
    @GET
    @Path("/authenticate")
    @Produces("application/json;charset=utf-8")
    public String authenticate( @QueryParam("inputData") String inputData) throws Exception {

        String result = "0";
        try 
        {
            
            result =callUrl("authenticate?inputData="+inputData);
            
        } catch (Exception e) {
            logger.LogException("authenticate:", e);
    //            result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
    result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                      ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";            
        }
        return result;
    }
    @GET
    @Path("/newsDetails")
    @Produces("application/json;charset=utf-8")
    public String newsDetails(@QueryParam("newsId")  Integer newsId) throws Exception {

        String result = "0";
        try 
        {
            
            result =callUrl("newsDetails?newsId="+newsId);
            logger.logInfo(result);
     
        } catch (Exception e) {
            logger.LogException("newsDetails:", e);
    //            result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
    result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                      ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";            
        }
        return result;

    }

    @GET
    @Path("/newsList")
    @Produces("application/json;charset=utf-8")
    public String newsList(
                                @QueryParam("recordsPerPage")  String recordsPerPage,
                                @QueryParam("currentPage")  String currentPage,
                                @QueryParam("orderBy")  String orderBy, 
                                @QueryParam("ascending")  String ascending 
                          )
                              
                              
    {

                String result = "0";
                try 
                {
                    
                    result =callUrl(
                                    "newsList?" +
                                    "recordsPerPage="+recordsPerPage+
                                    "&currentPage="+currentPage+
                                    "&orderBy="+orderBy+
                                    "&ascending="+ascending
                                    );
                    logger.logInfo(result);
             
                } catch (Exception e) {
                    logger.LogException("newsDetails:", e);
    //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
    result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                      ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
                }
                return result;

    }

    @GET
    @Path("/filesList")
    @Produces("application/json;charset=utf-8")
    public String filesList(
                                @QueryParam("loginId")                              String loginId, 
                                @DefaultValue ("10") @QueryParam("recordsPerPage")  String recordsPerPage,
                                @DefaultValue ("1")  @QueryParam("currentPage")     String currentPage,
                                @DefaultValue ("fileNumber") @QueryParam("orderBy") String orderBy, 
                                @DefaultValue ("1")  @QueryParam("ascending")       String ascending,
                                @DefaultValue ("0")  @QueryParam("isResearcher")    String isResearcher, 
                                @DefaultValue ("-1") @QueryParam("fromDate")        String fromDate, 
                                @DefaultValue ("-1") @QueryParam("toDate")          String toDate, 
                                @DefaultValue ("-1") @QueryParam("fileName")        String fileName, 
                                @DefaultValue ("-1") @QueryParam("beneficiaryName") String beneficiaryName, 
                                @DefaultValue ("-1") @QueryParam("fileType")        String fileType, 
                                @DefaultValue ("-1") @QueryParam("fileNumber")      String fileNumber,
                                @DefaultValue ("-1") @QueryParam("inheritanceFileId")      String inheritanceFileId
                                
                          )
                              
                              
    {

                String result = "0";
                try 
                {
                    
                    logger.logInfo("filesList:fileNumber:%s|inheritanceFileId:%s", fileNumber,inheritanceFileId);
                    inheritanceFileId=  inheritanceFileId !=null ?inheritanceFileId:"-1";
                    result =callUrl(
                                    "filesList?" +
                                    "loginId="+loginId+
                                    "&recordsPerPage="+recordsPerPage+
                                    "&currentPage="+currentPage+
                                    "&orderBy="+orderBy+
                                    "&ascending="+ascending+
                                    "&isResearcher="+isResearcher+
                                    "&fromDate="+fromDate+
                                    "&toDate="+toDate+
                                    "&fileName="+fileName+
                                    "&beneficiaryName="+beneficiaryName+
                                    "&fileType="+fileType+
                                    "&fileNumber="+fileNumber+
                                    "&inheritanceFileId="+inheritanceFileId
                    
                                    );
//                    logger.logInfo(result);
             
                } catch (Exception e) {
                    logger.LogException("filesList:", e);
    //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
                     result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                      ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
                }
                return result;

    }
    @GET
    @Path("/contactDetails")
    @Produces("application/json;charset=utf-8")
    public String contactDetails() 
    {

        String result = "0";
        try 
        {
            
            result =callUrl("contactDetails");
            logger.logInfo(result);
     
        } catch (Exception e) {
            logger.LogException("contactDetails:", e);
    //            result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
    result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                      ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";            
        }
        return result;

    }

    @GET
    @Path("/aboutAMAF")
    @Produces("application/json;charset=utf-8")
    public String aboutAMAF() 
    {

        String result = "0";
        try 
        {
            
            result="{\"aboutAMAF\":" +
                            "[" +
                                "   {" +
                                        "\"en\":\"http://esvapps.amaf.ae/AMAFMobileWSAppClient/about_amaf_en2.html\","+
                                        "\"ar\":\"http://esvapps.amaf.ae/AMAFMobileWSAppClient/about_amaf_ar.html\"" +
                                      
                                    "}" +
                            "]" +
                  "}";
    //            result= "{\"aboutAMAFEn\": \"http://esvapps.amaf.ae/AMAFMobileWSAppClient/aboutAMAF.html\"" +
            
    //            "}";

     
        } catch (Exception e) 
        {
            logger.LogException("aboutAMAF:", e);
            result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                      ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";            
        }
        return result;

    }
    @GET
    @Path("/beneficiariesList")
    @Produces("application/json;charset=utf-8")
    public String beneficiariesList(
                                        @QueryParam("fileId")                                Long fileId,
                                        @QueryParam("loginId")                               String loginId,
                                        @DefaultValue ("10") @QueryParam("recordsPerPage")   String recordsPerPage,
                                        @DefaultValue ("1") @QueryParam("currentPage")       String currentPage,
                                        @DefaultValue ("beneficiaryName") @QueryParam("orderBy")           String orderBy, 
                                        @DefaultValue ("1") @QueryParam("ascending")         String ascending ,
                                        @DefaultValue ("-1") @QueryParam("beneficiaryName")  String beneficiaryName, 
                                        @DefaultValue ("-1") @QueryParam("incomeCategory")   String incomeCategory, 
                                        @DefaultValue ("-1") @QueryParam("isStudent")        String isStudent,
                                        @DefaultValue ("-1") @QueryParam("minorStatus")      String minorStatus,
                                        @DefaultValue ("-1") @QueryParam("beneficiaryId")    Long beneficiaryId
                                            
                                  )
                              
                              
    {

                String result = "0";
                try 
                {
                    logger.logInfo("beneficiariesList|fileId:%s|loginId:%s|recordsPerPage:%s|currentPage:%s|orderBy:%s|ascending:%s|incomeCategory:%s|isStudent:%s|minorStatus:%s|beneficiaryId:%s|",
                                                     fileId,loginId,recordsPerPage,currentPage,orderBy,ascending,beneficiaryName,incomeCategory,isStudent,minorStatus,beneficiaryId
                                  );
                    result =callUrl(
                                    "beneficiariesList?" +
                                    "fileId="+fileId+
                                    "&loginId="+loginId+
                                    "&recordsPerPage="+recordsPerPage+
                                    "&currentPage="+currentPage+
                                    "&orderBy="+orderBy+
                                    "&ascending="+ascending+
                                    "&beneficiaryName="+beneficiaryName+
                                    "&incomeCategory="+incomeCategory+
                                    "&isStudent="+isStudent+
                                    "&minorStatus="+minorStatus
    
                    
                                    );
                    logger.logInfo(result);
             
                } catch (Exception e) {
                    logger.LogException("beneficiariesList:", e);
    //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
    result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                      ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
                }
                return result;

    }
    @GET
    @Path("/assetsList")
    @Produces("application/json;charset=utf-8")
    public String assetsList(
                                @QueryParam("fileId")        Long fileId,
                                @QueryParam("loginId")       String loginId,
                                @DefaultValue ("10") @QueryParam("recordsPerPage")String recordsPerPage,
                                @DefaultValue ("1") @QueryParam("currentPage")   String currentPage,
                                @DefaultValue ("assetId") @QueryParam("orderBy")       String orderBy, 
                                @DefaultValue ("1") @QueryParam("ascending")     String ascending 
                            )
                              
                              
    {
                String result = "0";
                try 
                {
                    
                    result =callUrl(
                                    "assetsList?" +
                                    "fileId="+fileId+
                                    "&loginId="+loginId+
                                    "&recordsPerPage="+recordsPerPage+
                                    "&currentPage="+currentPage+
                                    "&orderBy="+orderBy+
                                    "&ascending="+ascending
                                    );
                    logger.logInfo(result);
             
                } catch (Exception e) {
                    logger.LogException("assetsList:", e);
    //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
    result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                      ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
                }
                return result;

    }


    @GET
    @Path("/disbursementReasonSuperTypes")
    @Produces("application/json;charset=utf-8")
    public String disbursementReasonSuperTypes()
    {
        String result = "0";
        try
        {
        
            result =callUrl("disbursementReasonSuperTypes");
            //logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("disbursementReasonSuperTypes:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }

    @GET
    @Path("/disbursementReason")
    @Produces("application/json;charset=utf-8")
    public String disbursementReason(@QueryParam("mainReasonId")     Long mainReasonId)
    {
        String result = "0";
        try
        {
        
            result =callUrl("disbursementReason?mainReasonId="+mainReasonId);
            //logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("disbursementReason:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/disbursementReasonSubType")
    @Produces("application/json;charset=utf-8")
    public String disbursementReasonSubType(@QueryParam("reasonId") Long reasonId)
    {
        String result = "0";
        try
        {
        
            result =callUrl("disbursementReasonSubType?reasonId="+reasonId);
            //logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("disbursementReasonSubType:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }

    @GET
    @Path("/persistMEMSNolRequest")
    @Produces("application/json;charset=utf-8")
    public String persistMEMSNolRequest(@QueryParam("inputParameters") String inputParameters)
    {
        String result = "0";
        try
        {
        
            result =callUrlJSON("persistMEMSNolRequest",inputParameters.trim());
            logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("persistMEMSNolRequest:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/persistNormalDisbursementRequest")
    @Produces("application/json;charset=utf-8")
    public String persistNormalDisbursementRequest(@QueryParam("inputParameters") String inputParameters)
    {
        String result = "0";
        try
        {
        
            result =callUrlJSON("persistNormalDisbursementRequest",inputParameters.trim());
            logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("persistNormalDisbursementRequest:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }

    @GET
    @Path("/beneficiaryDetailsByFileIdLoginId")
    @Produces("application/json;charset=utf-8")
    public String beneficiaryDetailsByFileIdLoginId( @QueryParam("fileId")   Long fileId,
                                                     @QueryParam("loginId") String loginId
                                                    )
    {
        String result = "0";
        try
        {
        
            result =callUrl("beneficiaryDetailsByFileIdLoginId?fileId="+fileId+"&loginId="+loginId);
            logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("beneficiaryDetailsByFileIdLoginId:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/memsNOLTypes")
    @Produces("application/json;charset=utf-8")
    public String memsNOLTypes()
    {
        String result = "0";
        try
        {
            result =callUrl("memsNOLTypes");
            logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("memsNOLTypes:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/requestList")
    @Produces("application/json;charset=utf-8")
    public String requestList(
                             @QueryParam("personId") String personId,
                             @DefaultValue ("-1")  @QueryParam("fromDate") String fromDate, 
                             @DefaultValue ("-1")  @QueryParam("toDate") String toDate, 
                             @DefaultValue ("-1")  @QueryParam("requestNumber") String requestNumber, 
                             @DefaultValue ("-1")  @QueryParam("requestType") String requestType,
                             @DefaultValue ("10")  @QueryParam("recordsPerPage") String recordsPerPage,
                             @DefaultValue ("1")  @QueryParam("currentPage") String currentPage,
                             @DefaultValue ("requestId")  @QueryParam("orderBy") String orderBy, 
                             @DefaultValue ("0")  @QueryParam("ascending") String ascending 
                       )
    {

             String result = "0";
             try 
             {
                 
                 result =callUrl(
                                 "requestList?" +
                                 "&personId="+personId+
                                 "&fromDate="+fromDate+
                                 "&toDate="+toDate+
                                 "&requestNumber="+requestNumber+
                                 "&requestType="+requestType+
                                 "&recordsPerPage="+recordsPerPage+
                                 "&currentPage="+currentPage+
                                 "&orderBy="+orderBy+
                                 "&ascending="+ascending
                                 );
                 logger.logInfo(result);
          
             } catch (Exception e) {
                 logger.LogException("requestList:", e);
    //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
    result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                   ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
             }
             return result;

    }
    @GET
    @Path("/requestDetails")
    @Produces("application/json;charset=utf-8")
    public String requestDetails(  @QueryParam("requestId") Long requestId )
    {
        String result = "0";
        try
        {
        
            result =callUrl("requestDetails?requestId="+requestId);
            logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("requestDetails:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/requestTypes")
    @Produces("application/json;charset=utf-8")
    public String requestTypes()
    {
        String result = "0";
        try
        {
        
            result =callUrl("allRequestTypes");
            //logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("requestTypes:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/serviceProcedures")
    @Produces("application/json;charset=utf-8")
    public String serviceProcedures(       
                                    @QueryParam("categoryId") Long categoryId,
                                    @QueryParam("typeId")Long typeId 
                                   )
    {
        String result = "0";
        try
        {
        
            result =callUrl("serviceProcedures?categoryId="+categoryId+"&typeId="+typeId );
            //logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("serviceProcedures:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }

    @GET
    @Path("/servieProcedureTypes")
    @Produces("application/json;charset=utf-8")
    public String servieProcedureTypes(  )
    {
            String result = "0";
            try
            {
            
                result =callUrl("servieProcedureTypes" );
                //logger.logInfo(result);
            
            } catch (Exception e) {
            logger.LogException("servieProcedureTypes:", e);
            //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
            result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
            ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
            }
            return result;

    }
        
    @GET
    @Path("/servieCategories")
    @Produces("application/json;charset=utf-8")
    public String servieCategories(  )
    {
        String result = "0";
        try
        {
        
            result =callUrl("servieCategories" );
            //logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("servieCategories:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    
    @GET
    @Path("/balanceSummary")
    @Produces("application/json;charset=utf-8")
    public String balanceSummary(       
                                    @QueryParam("personId") Long personId,
                                    @QueryParam("inheritanceFileId") Long inheritanceFileId,
                                    @QueryParam("trxDuration") int trxDuration,
                                    @QueryParam("statementDate")  String statementDate
                                   )
    {
        String result = "0";
        try
        {
        
            result =callUrl("balanceSummary?personId="+personId+"&inheritanceFileId="+inheritanceFileId+"&trxDuration="+trxDuration+"&statementDate="+statementDate );
            //logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("balanceSummary:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }

    
    @GET
    @Path("/relatedPersonsForBalanceSummary")
    @Produces("application/json;charset=utf-8")
    public String relatedPersonsForBalanceSummary(       
                                                        @QueryParam("personId") Long personId
                                                )
    {
        String result = "0";
        try
        {
        
            result =callUrl("relatedPersonsForBalanceSummary?personId="+personId);
            //logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("relatedPersonsForBalanceSummary:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/registerUser")
    @Produces("application/json;charset=utf-8")
    public String registerUser(@QueryParam("registrationData")String registrationData) {

        String result = "0";
        try 
        {
            result =callUrl("registerUser?registrationData="+registrationData);
    //            result =callUrlJSON("registerUser","registrationData="+registrationData);
            
        } catch (Exception e) {
            logger.LogException("registerUser:", e);
    //            result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
    result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                      ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";            
        }
        return result;
    }
    
    @GET
    @Path("/activateUser")
    @Produces("application/json;charset=utf-8")
    public String activateUser(       
                               @QueryParam("loginId") String loginId,
                               @QueryParam("emiratesId") String emiratesId,
                               @QueryParam("pin") String pin
                              )
    {
        String result = "0";
        try
        {
        
    //            System.out.println("activateUser Start");
            result =callUrl("activateUser?loginId="+loginId+"&emiratesId="+emiratesId+"&pin="+pin);
    //            result =callUrlJSON("activateUser","loginId="+loginId+"&pin="+pin);
    //            System.out.println("activateUser result:"+result);
        
        } catch (Exception e) {
        logger.LogException("activateUser:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }

    @GET
    @Path("/regenerateNewPINToActivateLogin")
    @Produces("application/json;charset=utf-8")
    public String regenerateNewPINToActivateLogin(@QueryParam("loginId") String loginId, @QueryParam("emiratesId") String emiratesId )
    {
        String result = "0";
        try
        {
            result =callUrl("regenerateNewPINToActivateLogin?loginId="+loginId+"&emiratesId="+emiratesId);
        
        } catch (Exception e) {
        logger.LogException("regenerateNewPINToActivateLogin:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;
    }
    
    @GET
    @Path("/getPINForLoginId")
    @Produces("application/json;charset=utf-8")
    public String getPINForLoginId(@QueryParam("loginId") String loginId )
    {
        String result = "0";
        try
        {
            result =callUrl("getPINForLoginId?loginId="+loginId);
        
        } catch (Exception e) {
        logger.LogException("getPINForLoginId:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/changePassword")
    @Produces("application/json;charset=utf-8")
    public String changePassword(@QueryParam("inputData")String inputData )
    {
        String result = "0";
        try
        {
            result =callUrl("changePassword?inputData="+inputData);
        
        } catch (Exception e) {
        logger.LogException("changePassword:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    
    @GET
    @Path("/changePasswordFromPortal")
    @Produces("application/json;charset=utf-8")
    public String changePasswordFromPortal(@QueryParam("inputData")String inputData )
    {
        String result = "0";
        try
        {
            result =callUrl("changePasswordFromPortal?inputData="+inputData);
        
        } catch (Exception e) {
        logger.LogException("changePassword:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/persistAppointment")
    @Produces("application/json;charset=utf-8")
    public String persistAppointment(@QueryParam("inputParameters")String inputParameters)
    {
        String result = "0";
        try
        {
        
            result =callUrlJSON("persistAppointment",inputParameters.trim());
            logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("persistAppointment:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
        
    @GET
    @Path("/appointmentList")
    @Produces("application/json;charset=utf-8")
    public String appointmentList(
                                  @QueryParam("loginId")      String loginId,
                                  @QueryParam("filters")      String filters, 
                                  @DefaultValue("10") @QueryParam("recordsPerPage")      String recordsPerPage,
                                  @DefaultValue("1")  @QueryParam("currentPage")      String currentPage,
                                  @DefaultValue("appointmentDate") @QueryParam("orderBy")      String orderBy, 
                                  @DefaultValue("0") @QueryParam("ascending")      String ascending 
                                  )
                  
                  
    {
            String result = "0";
            try 
            {
                
                result =callUrl(
                                "appointmentList?" +
                                "loginId="+loginId+
                                "&filters="+filters+
                                "&recordsPerPage="+recordsPerPage+
                                "&currentPage="+currentPage+
                                "&orderBy="+orderBy+
                                "&ascending="+ascending
                                );
                logger.logInfo(result);
            
            } catch (Exception e) {
                logger.LogException("appointmentList:", e);
            //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
                 result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                  ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
            }
            return result;

    }
    
    @GET
    @Path("/relatedAmafEmployeeForLoginId")
    @Produces("application/json;charset=utf-8")
    public  String relatedAmafEmployeeForLoginId(@QueryParam("loginId")String loginId )
    {
            String result = "0";
            try 
            {
                
                result =callUrl(
                                "relatedAmafEmployeeForLoginId?loginId=" +loginId
                                );
                logger.logInfo(result);
            } 
            catch (Exception e) 
            {
                logger.LogException("relatedAmafEmployeeForLoginId:", e);
            
                 result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                  ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
            }
            return result;

    }
    @GET
    @Path("/deleteAppointment")
    @Produces("application/json;charset=utf-8")
    public String deleteAppointment(
                                        @QueryParam("appointmentId")String appointmentId,
                                        @QueryParam("loginId")String loginId
                                   )
    {
            String result = "0";
            try 
            {
                
                result =callUrl(
                                "deleteAppointment?" +
                                "appointmentId="+appointmentId+
                                "&loginId="+loginId
                                );
                logger.logInfo(result);
            } 
            catch (Exception e) 
            {
                logger.LogException("deleteAppointment:", e);
            
                 result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                  ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
            }
            return result;

    }
    
    
    @GET
    @Path("/chatList")
    @Produces("application/json;charset=utf-8")
    public String chatList(
                           @QueryParam("loginId") String loginId,
                           @QueryParam("inheritanceFileId") String inheritanceFileId, 
                           @QueryParam("filters") String filters,
                           @DefaultValue("10") @QueryParam("recordsPerPage") String recordsPerPage,
                           @DefaultValue("1") @QueryParam("currentPage") String currentPage,
                           @DefaultValue("chatId") @QueryParam("orderBy") String orderBy, 
                           @DefaultValue("0") @QueryParam("ascending") String ascending 
                           )
                  
                  
    {
            String result = "0";
            try 
            {
                
                result =callUrl(
                                "chatList?" +
                                "loginId="+loginId+
                                "&inheritanceFileId="+inheritanceFileId+
                                "&filters="+filters+
                                "&recordsPerPage="+recordsPerPage+
                                "&currentPage="+currentPage+
                                "&orderBy="+orderBy+
                                "&ascending="+ascending
                                );
                logger.logInfo(result);
            
            } catch (Exception e) {
                logger.LogException("chatList:", e);
            //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
                 result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                  ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
            }
            return result;

    }
    @GET
    @Path("/getUserHappinessIndex")
    @Produces("application/json;charset=utf-8")
    public String getUserHappinessIndex()
                  
                  
    {
            String result = "0";
            try 
            {
                
                result =callUrl(
                                "getUserHappinessIndex"
                                );
                logger.logInfo(result);
            
            } catch (Exception e) {
                logger.LogException("getUserHappinessIndex:", e);
            //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
                 result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                  ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
            }
            return result;

    }
    
    @GET
    @Path("/persistUserHappinessForRequestId")
    @Produces("application/json;charset=utf-8")
    public String persistUserHappinessForRequestId(
                                            @QueryParam("userId")      String userId,
                                            @QueryParam("requestId")   String requestId, 
                                            @QueryParam("happinessId") String happinessId
                                      )
                      
                      
        {
                String result = "0";
                try 
                {
                    
                    result =callUrl(
                                    "persistUserHappinessForRequestId?" +
                                    "userId="+userId+
                                    "&requestId="+requestId+
                                    "&happinessId="+happinessId
                                    );
                    logger.logInfo(result);
                
                } catch (Exception e) {
                    logger.LogException("persistUserHappinessForRequestId:", e);
                //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
                     result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                      ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
                }
                return result;

        }
    
    @GET
    @Path("/persistUserFeedback")
    @Produces("application/json;charset=utf-8")    
    public String persistUserFeedback(@QueryParam("inputParamters")String inputParameters)
    
    {
        String result = "0";
        try
        {
        
            result =callUrlJSON("persistUserFeedback",inputParameters.trim());
            logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("persistUserFeedback:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/getRelations")
    @Produces("application/json;charset=utf-8")    
    public String getRelations(  )
    {
        String result = "0";
        try
        {
        
            result =callUrl("getRelations" );
            
        
        } catch (Exception e) {
        logger.LogException("getRelations:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }

    @GET
    @Path("/getAccomodationFormDetails")
    @Produces("application/json;charset=utf-8")    
    public String getAccomodationFormDetails(
                                             @QueryParam("researcherId") String researcherId,  
                                             @QueryParam("beneficiaryId") Long beneficiaryId, 
                                             @QueryParam("socialResearchId") Long socialResearchId
                                            )
    {
        String result = "0";
        try
        {
        
            result =callUrl("getAccomodationFormDetails?" +
                            "researcherId="+researcherId+
                            "&beneficiaryId="+beneficiaryId+
                            "&socialResearchId="+socialResearchId
                            
                          );
            
            //logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("getAccomodationFormDetails:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    
    @GET
    @Path("/getHealthFormDetails")
    @Produces("application/json;charset=utf-8")    
    public String getHealthFormDetails(
                                            @QueryParam("researcherId") String researcherId,  
                                            @QueryParam("beneficiaryId")  Long beneficiaryId, 
                                            @QueryParam("socialResearchId") Long socialResearchId
                                            )
    {
        String result = "0";
        try
        {
        
            result =callUrl("getHealthFormDetails?researcherId="+researcherId+"&beneficiaryId="+beneficiaryId+"&socialResearchId="+socialResearchId);
            //logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("getHealthFormDetails:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    
    @GET
    @Path("/getEducationFormDetails")
    @Produces("application/json;charset=utf-8")    
    public String getEducationFormDetails(
                                            @QueryParam("researcherId") String researcherId,  
                                            @QueryParam("beneficiaryId")  Long beneficiaryId, 
                                            @QueryParam("socialResearchId") Long socialResearchId
                                            )
    {
        String result = "0";
        try
        {
        
            result =callUrl("getEducationFormDetails?researcherId="+researcherId+"&beneficiaryId="+beneficiaryId+"&socialResearchId="+socialResearchId);
            //logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("getEducationFormDetails:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
   
    @GET
    @Path("/getFinancialFormDetails")
    @Produces("application/json;charset=utf-8")    
    public String getFinancialFormDetails(
                                            @QueryParam("researcherId") String researcherId,  
                                            @QueryParam("beneficiaryId")  Long beneficiaryId, 
                                            @QueryParam("socialResearchId") Long socialResearchId
                                          )
    {
        String result = "0";
        try
        {
        
            result =callUrl("getFinancialFormDetails?researcherId="+researcherId+"&beneficiaryId="+beneficiaryId+"&socialResearchId="+socialResearchId);
            //logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("getFinancialFormDetails:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
        
    @GET
    @Path("/getSocialFormDetails")
    @Produces("application/json;charset=utf-8")    
    public String getSocialFormDetails(
                                            @QueryParam("researcherId") String researcherId,  
                                            @QueryParam("beneficiaryId")  Long beneficiaryId, 
                                            @QueryParam("socialResearchId") Long socialResearchId
                                      )
    {
        String result = "0";
        try
        {
        
            result =callUrl("getSocialFormDetails?researcherId="+researcherId+"&beneficiaryId="+beneficiaryId+"&socialResearchId="+socialResearchId);
            //logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("getSocialFormDetails:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    
    @GET
    @Path("/persistSocialResearch")
    @Produces("application/json;charset=utf-8")    
    public String persistSocialResearch(@QueryParam("inputParameters")String inputParameters)
    {
        String result = "0";
        try
        {
        
            result =callUrlJSON("persistSocialResearch",inputParameters.trim());
    //            logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("persistSocialResearch:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    
    @GET
    @Path("/deleteLocation")
    @Produces("application/json;charset=utf-8")    
    public String deleteLocation(  
                                 @QueryParam("locationId")   String locationId, 
                                 @QueryParam("loginId")  String loginId
                                )
    {
        
        String result = "0";
        try
        {
        
            result =callUrl("deleteLocation?locationId="+locationId+"&loginId="+loginId);
            //logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("deleteLocation:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;
    }
    
    
    @GET
    @Path("/persistLocation")
    @Produces("application/json;charset=utf-8")    
    public String persistLocation(@QueryParam("inputParameters")String inputParameters)
    {
        String result = "0";
        try
        {
            
            
            result =callUrlJSON("persistLocation",inputParameters.trim());
    //            logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("persistLocation:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    
    @GET
    @Path("/getLocationFormDetails")
    @Produces("application/json;charset=utf-8")    
    public String getLocationFormDetails(
                                        @QueryParam("researcherId") String researcherId,  
                                        @QueryParam("beneficiaryId")     String beneficiaryId, 
                                        @QueryParam("socialResearchId") String socialResearchId,
                                        @QueryParam("inheritanceFileId") String inheritanceFileId
                                        )
    {
        String result = "0";
        try
        {
        
            result =callUrl("getLocationFormDetails?" +
                            "researcherId="+researcherId+"&beneficiaryId="+beneficiaryId+"&socialResearchId="+socialResearchId+"&inheritanceFileId="+inheritanceFileId);
            
        
        } catch (Exception e) {
        logger.LogException("getLocationFormDetails:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/donationProgramStatus")
    @Produces("application/json;charset=utf-8")    
    public String donationProgramStatus()
    {
        String result = "0";
        try
        {
            result =callUrl("donationProgramStatus");
        } catch (Exception e) {
        logger.LogException("donationProgramStatus:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/donationProgramObjective")
    @Produces("application/json;charset=utf-8")    
    public String donationProgramObjective()
    {
        String result = "0";
        try
        {
            result =callUrl("donationProgramObjective");
        } catch (Exception e) {
        logger.LogException("donationProgramObjective:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/donationProgramDetails")
    @Produces("application/json;charset=utf-8")    
    public String donationProgramDetails( @QueryParam("donationProgramId") Long donationProgramId )
    {
        String result = "0";
        try
        {
            result =callUrl("donationProgramDetails?donationProgramId="+donationProgramId);
        } catch (Exception e) {
        logger.LogException("donationProgramDetails:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;
    }

    @GET
    @Path("/donationProgramsList")
    @Produces("application/json;charset=utf-8")        
    public String donationProgramsList(
                                @QueryParam("loginId") String loginId, 
                                @QueryParam("deviceId") String deviceId, 
                                @DefaultValue("10") @QueryParam("recordsPerPage") String recordsPerPage,
                                @DefaultValue("1") @QueryParam("currentPage") String currentPage,
                                @DefaultValue("endowmentProgramId") @QueryParam("orderBy") String orderBy, 
                                @DefaultValue("1") @QueryParam("ascending") String ascending,
                                @DefaultValue("-1") @QueryParam("fromStartDate") String fromStartDate, 
                                @DefaultValue("-1")@QueryParam("toStartDate")  String toStartDate, 
                                @DefaultValue("-1")@QueryParam("fromEndDate")  String fromEndDate, 
                                @DefaultValue("-1")@QueryParam("toEndDate")  String toEndDate, 
                                @DefaultValue("-1")@QueryParam("programName") String programName, 
                                @DefaultValue("-1")@QueryParam("objective") String objective, 
                                @DefaultValue("-1")@QueryParam("status") String status
                                
                          )
                              
                              
    {

                String result = "0";
                try 
                {
                    
                    result =callUrl(
                                    "donationProgramsList?" +
                                    "loginId="+loginId+
                                    "deviceId="+deviceId+
                                    "&recordsPerPage="+recordsPerPage+
                                    "&currentPage="+currentPage+
                                    "&orderBy="+orderBy+
                                    "&ascending="+ascending+
                                    "&fromStartDate="+fromStartDate+
                                    "&toStartDate="+toStartDate+
                                    "&fromEndDate="+fromEndDate+
                                    "&toEndDate="+toEndDate+
                                    "&programName="+programName+
                                    "&objective="+objective+
                                    "&status="+status
                                    );
                    logger.logDebug(result);
             
                } catch (Exception e) {
                    logger.LogException("donationProgramsList:", e);
    //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
                     result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                      ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
                }
                return result;

    }
    @GET
    @Path("/persistDonationRequest")
    @Produces("application/json;charset=utf-8")        
    public String persistDonationRequest(@QueryParam("inputParameters")String inputParameters)
    {
        String result = "0";
        try
        {
        
            result =callUrlJSON("persistDonationRequest",inputParameters.trim());
            logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("persistDonationRequest:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/getSMSAmounts")
    @Produces("application/json;charset=utf-8")        
    public String getSMSAmounts(
                                @QueryParam("donationProgramId") Long donationProgramId
                               )
    {
        String result = "0";
        try
        {
        
            result =callUrl("getSMSAmounts?donationProgramId="+donationProgramId);
            
        
        } catch (Exception e) {
        logger.LogException("getSMSAmounts:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/myContributions")
    @Produces("application/json;charset=utf-8")        
    public String myContributions(
                                   @QueryParam("loginId") String loginId,
                                   @QueryParam("deviceId") String deviceId
                                 )
    {
        String result = "0";
        try
        {
        
            result =callUrl("getMyContributions?loginId="+loginId+"&deviceId="+deviceId);
            
        
        } catch (Exception e) {
        logger.LogException("myContributions:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }

    @GET
    @Path("/myContributionsInDonationProgram")
    @Produces("application/json;charset=utf-8")     
    public String myContributionsInDonationProgram(
                                                    @QueryParam("loginId")String loginId,
                                                    @QueryParam("deviceId")String deviceId,
                                                    @QueryParam("donationProgramId")String donationProgramId
                                                  )
    {
        String result = "0";
        try
        {
            //?loginId=pims_admin&deviceId=1234567890&donationProgramId=1
            result =callUrl("getMyContributionsInDonationProgram?loginId="+loginId+"&deviceId="+deviceId+"&donationProgramId="+donationProgramId);
            
        
        } catch (Exception e) {
        logger.LogException("getMyContributionsInDonationProgram:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    
    @GET
    @Path("/getGeneralFormDetails")
    @Produces("application/json;charset=utf-8")     
    public String getGeneralFormDetails(    @QueryParam("researcherId")String researcherId,  
                                            @QueryParam("beneficiaryId") Long beneficiaryId, 
                                            @QueryParam("socialResearchId")Long socialResearchId
                                            )
    {
        String result = "0";
        try
        {
            result =callUrl("getGeneralFormDetails?researcherId="+researcherId+"&beneficiaryId="+beneficiaryId+"&socialResearchId="+socialResearchId);
        } catch (Exception e) {
        logger.LogException("getGeneralFormDetails:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/incomeCategories")
    @Produces("application/json;charset=utf-8")     
    public String incomeCategories()
    {
        String result = "0";
        try
        {
        
            result =callUrl("incomeCategory");
            
        
        } catch (Exception e) {
        logger.LogException("incomeCategories:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/socialResearchList")
    @Produces("application/json;charset=utf-8")     
    public String socialResearchList(
                                @QueryParam("loginId") String loginId, 
                                @DefaultValue("10")@QueryParam("recordsPerPage") String recordsPerPage,
                                @DefaultValue("1")@QueryParam("currentPage") String currentPage,
                                @DefaultValue("socialResearchId")@QueryParam("orderBy") String orderBy, 
                                @DefaultValue("0")@QueryParam("ascending") String ascending,
                                @DefaultValue("-1")@QueryParam("fromDate") String fromDate, 
                                @DefaultValue("-1")@QueryParam("toDate") String toDate, 
                                @DefaultValue("-1")@QueryParam("fileName") String fileName, 
                                @DefaultValue("-1")@QueryParam("fileNumber") String fileNumber,
                                @DefaultValue("-1")@QueryParam("costCenter") String costCenter,
                                @DefaultValue("-1")@QueryParam("socialResearchNumber") String socialResearchNumber
                                
                          )
                              
                              
    {

                String result = "0";
                try 
                {
                    
                    result =callUrl(
                                    "socialResearchList?" +
                                    "loginId="+loginId+
                                    "&recordsPerPage="+recordsPerPage+
                                    "&currentPage="+currentPage+
                                    "&orderBy="+orderBy+
                                    "&ascending="+ascending+
                                    "&fromDate="+fromDate+
                                    "&toDate="+toDate+
                                    "&fileName="+fileName+
                                    "&fileNumber="+fileNumber+
                                    "&costCenter="+costCenter+
                                    "&socialResearchNumber="+socialResearchNumber
                    
                                    );
                    logger.logInfo(result);
             
                } catch (Exception e) {
                    logger.LogException("socialResearchList:", e);
    //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
                     result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                      ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
                }
                return result;

    }
    @GET
    @Path("/minorStatus")
    @Produces("application/json;charset=utf-8")     
    public String minorStatus()
    {
        String result = "0";
        try
        {
        
            result =callUrl("minorStatus");
            
        
        } catch (Exception e) {
        logger.LogException("minorStatus:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/getMaintenanceSearchCriteria")
    @Produces("application/json;charset=utf-8")     
    public String getMaintenanceSearchCriteria()
    {
        String result = "0";
        try
        {
        
            result =callUrl("getMaintenanceSearchCriteria");
            
        
        } catch (Exception e) {
        logger.LogException("getMaintenanceSearchCriteria:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    } 
    @GET
    @Path("/minorMaintenanceList")
    @Produces("application/json;charset=utf-8")     
    public String minorMaintenanceList(
                                        @QueryParam("loginId") String loginId,
                                        @QueryParam("inheritanceFileId") String inheritanceFileId, 
                                        @DefaultValue("10")@QueryParam("recordsPerPage") String recordsPerPage,
                                        @DefaultValue("1")@QueryParam("currentPage") String currentPage,
                                        @DefaultValue("-1")@QueryParam("statusId") String statusId, 
                                        @DefaultValue("-1")@QueryParam("requestNumber") String requestNumber,
                                        @DefaultValue("-1")@QueryParam("maintenanceType") String maintenanceType, 
                                        @DefaultValue("-1")@QueryParam("maintenanceWorkTypes") String maintenanceWorkTypes
                                      )
                              
                              
    {

                String result = "0";
                try 
                {
                    
                    result =callUrl(
                                    "minorMaintenanceList?" +
                                    "loginId="+loginId+
                                    "&inheritanceFileId="+inheritanceFileId+
                                    "&recordsPerPage="+recordsPerPage+
                                    "&pageNumber="+currentPage+
                                    "&statusId="+statusId+
                                    "&requestNumber="+requestNumber+
                                    "&maintenanceType="+maintenanceType+
                                    "&maintenanceWorkTypes="+maintenanceWorkTypes
                                    );
                    //logger.logInfo(result);
             
                } catch (Exception e) {
                    logger.LogException("minorMaintenanceList:", e);
    //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
                     result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                      ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
                }
                return result;

    }
    @GET
    @Path("/persistMinorMaintenanceRequest")
    @Produces("application/json;charset=utf-8")             
    public String persistMinorMaintenanceRequest(@QueryParam("inputParameters")String inputParameters)
    {
        String result = "0";
        try
        {
            result =callUrlJSON("persistMinorMaintenanceRequest",inputParameters.trim());
    } catch (Exception e) {
        logger.LogException("persistMinorMaintenanceRequest:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }

    @GET
    @Path("/minorMaintenanceFormDetails")
    @Produces("application/json;charset=utf-8")                 
    public String minorMaintenanceFormDetails(
                                                    @DefaultValue("-1")@QueryParam("requestId")String requestId,
                                                    @QueryParam("loginId")String loginId
                                                  
                                               )
    {
            String result = "0";
            try 
            {
                
                result =callUrl(
                                "minorMaintenanceFormDetails?" +
                                "requestId="+requestId+
                                "&loginId="+loginId
                                );
                
            } 
            catch (Exception e) 
            {
                logger.LogException("minorMaintenanceFormDetails:", e);
            
                 result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                  ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
            }
            return result;

    }
    
    @GET
    @Path("/deleteMinorMaintenanceRequest")
    @Produces("application/json;charset=utf-8")                 
    public String deleteMinorMaintenanceRequest(
                                                  @QueryParam("requestId")  String requestId,
                                                  @QueryParam("loginId")  String loginId
                                               )
    {
            String result = "0";
            try 
            {
                result =callUrl(
                                "deleteMinorMaintenanceRequest?" +
                                "requestId="+requestId+
                                "&loginId="+loginId
                                );

            } 
            catch (Exception e) 
            {
                logger.LogException("deleteMinorMaintenanceRequest:", e);
            
                 result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                  ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
            }
            return result;

    }
    
    @GET
    @Path("/recommendationsList")
    @Produces("application/json;charset=utf-8")         
    public String recommendationsList(
                                        @QueryParam("loginId") String loginId,
                                        @QueryParam("socialResearchId") String socialResearchId,
                                        @QueryParam("inheritanceFileId") String inheritanceFileId,
                                        @DefaultValue("10")@QueryParam("recordsPerPage") String recordsPerPage,
                                        @DefaultValue("1")@QueryParam("pageNumber") String pageNumber
                                      )
                              
                              
    {

                String result = "0";
                try 
                {
                    
                    result =callUrl(
                                    "recommendationsList?" +
                                    "loginId="+loginId+
                                    "&socialResearchId="+socialResearchId+
                                    "&inheritanceFileId="+inheritanceFileId+
                                    "&recordsPerPage="+recordsPerPage+
                                    "&pageNumber="+pageNumber
                                    );
                    //logger.logInfo(result);
             
                } catch (Exception e) {
                    logger.LogException("recommendationsList:", e);
    //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
                     result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                      ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
                }
                return result;

    }
    @GET
    @Path("/persistRecommendation")
    @Produces("application/json;charset=utf-8") 
    public String persistRecommendation(@QueryParam("inputParameters")  String inputParameters )
    {
            String result = "0";
            try
            {
            
                result =callUrlJSON("persistRecommendation",inputParameters);

            
            } catch (Exception e) {
            logger.LogException("persistRecommendation:", e);
            //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
            result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
            ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
            }
            return result;

    }
    @GET
    @Path("/recommendationFormDetails")
    @Produces("application/json;charset=utf-8")     
    public String recommendationFormDetails(
                                               @QueryParam("loginId") String loginId,
                                               @QueryParam("recommendationId")  String recommendationId,
                                                @QueryParam("socialResearchId") String socialResearchId,
                                                @QueryParam("inheritanceFileId") String inheritanceFileId,
                                                @QueryParam("beneficiaryId") String beneficiaryId
                                              )
        {
                String result = "0";
                try 
                {
                    
                    result =callUrl(
                                    "recommendationFormDetails?" +
                                    "loginId="+loginId+
                                    "&recommendationId="+recommendationId+
                                    "&socialResearchId="+socialResearchId+
                                    "&inheritanceFileId="+inheritanceFileId+
                                    "&beneficiaryId="+beneficiaryId
                                    );
                    
                } 
                catch (Exception e) 
                {
                    logger.LogException("recommendationFormDetails:", e);
                
                     result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                      ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
                }
                return result;

    }
    
    @GET
    @Path("/deleteRecommendation")
    @Produces("application/json;charset=utf-8")    
    public String deleteRecommendation(
                                          @QueryParam("recommendationId") String recommendationId,
                                          @QueryParam("loginId") String loginId
                                      )
    {
            String result = "0";
            try 
            {
                result =callUrl(
                                "deleteRecommendation?" +
                                "recommendationId="+recommendationId+
                                "&loginId="+loginId
                                );

            } 
            catch (Exception e) 
            {
                logger.LogException("deleteRecommendation:", e);
            
                 result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                  ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
            }
            return result;

    }
    
    @GET
    @Path("/persistBeneficiaryDetails")
    @Produces("application/json;charset=utf-8")    
    public String persistBeneficiaryDetails( @QueryParam("inputParameters")String inputParameters )
    {
            String result = "0";
            try
            {
            
                result =callUrlJSON("persistBeneficiaryDetails",inputParameters);

            
            } catch (Exception e) {
            logger.LogException("persistBeneficiaryDetails:", e);
            //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
            result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
            ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
            }
            return result;

    }
    @GET
    @Path("/researchStats")
    @Produces("application/json;charset=utf-8")    
    public String researchStats(@QueryParam("loginId")String loginId)
    {
            String result = "0";
            try 
            {
                result =callUrl(
                                "researchStats?loginId=" +loginId
                                );

            } 
            catch (Exception e) 
            {
                logger.LogException("researchStats:", e);
            
                 result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                  ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
            }
            return result;

    }
    @GET
    @Path("/getComparisonList")
    @Produces("application/json;charset=utf-8")    
    public String getComparisonList(
                                        @QueryParam("aspect")String aspect,
                                        @QueryParam("records") String records,
                                        @QueryParam("loginId")String loginId,
                                        @QueryParam("inheritanceFileId")String inheritanceFileId,
                                        @QueryParam("beneficiaryId")String beneficiaryId
                                   )
    {
            String result = "0";
            try 
            {
                result =callUrl(
                                "getComparisonList?" +
                                "aspect="+aspect+
                                "&records="+records+
                                "&loginId="+loginId+
                                "&inheritanceFileId="+inheritanceFileId+
                                "&beneficiaryId="+beneficiaryId
                                );

            } 
            catch (Exception e) 
            {
                logger.LogException("getComparisonList:", e);
            
                 result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                  ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
            }
            return result;

    }
    @GET
    @Path("/compareAspects")
    @Produces("application/json;charset=utf-8")    
    public String compareAspects( @QueryParam("inputParameters")String inputParameters )
    {
        String result = "0";
        try
        {
            result =callUrlJSON("compareAspects",inputParameters);
        } catch (Exception e) {
        logger.LogException("compareAspects:", e);
        
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;
    }
    
    @GET
    @Path("/compareSavedAspects")
    @Produces("application/json;charset=utf-8")    
    public String compareSavedAspects( 
                                        @QueryParam("aspect")String aspect,
                                        @QueryParam("personId")Long   personId,
                                        @QueryParam("oldAspectId")Long   oldAspectId
                                     )
    {
        String result = "0";
        try
        {
            result =callUrl("compareSavedAspects?" +
                                "aspect="+aspect+
                                "&personId="+personId+
                                "&oldAspectId="+oldAspectId
                               );
        } catch (Exception e) {
        logger.LogException("compareSavedAspects:", e);
        
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;
    }        
    @GET
    @Path("/getbeneficiariesDetails")
    @Produces("application/json;charset=utf-8")    
    public String getbeneficiariesDetails(
                                        @QueryParam("fileId")Long fileId,
                                        @QueryParam("loginId")String loginId,
                                        @QueryParam("beneficiaryId")Long beneficiaryId
                                  )
                              
                              
    {

                String result = "0";
                try 
                {
                    logger.logInfo("beneficiariesList|fileId:%s|loginId:%s|beneficiaryId:%s|",
                                                     fileId,loginId,beneficiaryId
                                  );
                    result =callUrl(
                                    "beneficiariesList?" +
                                    "fileId="+fileId+
                                    "&loginId="+loginId+
                                    "&beneficiaryId="+beneficiaryId
                    
                                    );
                    logger.logInfo(result);
             
                } catch (Exception e) {
                    logger.LogException("getbeneficiariesDetails:", e);
    //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
    result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                      ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
                }
                return result;

    }
    @GET
    @Path("/fileTypes")
    @Produces("application/json;charset=utf-8")    
    public String fileTypes()
    {
        String result = "0";
        try
        {
        
            result =callUrl("fileTypes");
            //logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("fileTypes:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/getPrivacyPolicy")
    @Produces("application/json;charset=utf-8")    
    public String getPrivacyPolicy(@QueryParam("loginId")String loginId,@QueryParam("deviceId")String deviceId)
    {
        String result = "0";
        try
        {
            result =callUrl("getPrivacyPolicy?loginId="+loginId+"&deviceId="+deviceId);
        
        } catch (Exception e) {
        logger.LogException("getPrivacyPolicy:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;
    }
    @GET
    @Path("/getFAQS")
    @Produces("application/json;charset=utf-8")    
    public String getFAQS(@QueryParam("loginId")String loginId,@QueryParam("deviceId")String deviceId)
    {
        String result = "0";
        try
        {
            result =callUrl("getFAQS?loginId="+loginId+"&deviceId="+deviceId);
        
        } catch (Exception e) {
        logger.LogException("getFAQ:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;
    }
    @GET
    @Path("/forgetPassword")
    @Produces("application/json;charset=utf-8")    
    public String forgetPassword(@QueryParam("loginId")String loginId, @QueryParam("emiratesId")String emiratesId )
    {
        String result = "0";
        try
        {
            result =callUrl("forgetPassword?loginId="+loginId+"&emiratesId="+emiratesId);
        
        } catch (Exception e) {
        logger.LogException("forgetPassword:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;
    }
    
    @GET
    @Path("/getSocialResearchDetails")
    @Produces("application/json;charset=utf-8")    
    public String getSocialResearchDetails( @QueryParam("loginId")String loginId,
                                            @QueryParam("socialResearchId")String socialResearchId,
                                            @QueryParam("beneficiaryId")String beneficiaryId )
    {
        String result = "0";
        try
        {
            result =callUrl("getSocialResearchDetails?loginId="+loginId+"&socialResearchId="+socialResearchId+"&beneficiaryId="+beneficiaryId);
        
        } catch (Exception e) {
        logger.LogException("getSocialResearchDetails:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;
    }        

    @GET
    @Path("/getLeaseContractDetails")
    @Produces("application/json;charset=utf-8")    
    public String getLeaseContractDetails( @QueryParam("contractId")String contractId)
    {
        String result = "0";
        try
        {
            result =callUrl("getLeaseContractDetails?contractId="+contractId);
        
        } catch (Exception e) {
        logger.LogException("getLeaseContractDetails:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;
    }        

    @GET
    @Path("/leaseContractList")
    @Produces("application/json;charset=utf-8")        
    public String leaseContractList(  @QueryParam("filters")String filters )
                              
                              
    {
                String result = "0";
                try 
                {
                    logger.logInfo("leaseContractList|filters:%s",filters);
                    result =callUrl(
                                    "leaseContractList?" +
                                    "filters="+URLEncoder.encode(filters,"UTF-8")
                                    );
                    
             
                } catch (Exception e) {
                    logger.LogException("leaseContractList:", e);
    //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
                     result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                      ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
                }
                return result;

    }    
    @GET
    @Path("/propertyNOLTypes")
    @Produces("application/json;charset=utf-8")
    public String propertyNOLTypes(@QueryParam("loginId") String loginId)
    {
        String result = "0";
        try
        {
            result =callUrl("propertyNOLTypes?loginId="+loginId);
            logger.logInfo(result);
        
        } catch (Exception e) {
        logger.LogException("propertyNOLTypes:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    
    @POST
    @Path("/persistPropertyNolRequest")
    @Produces("application/json;charset=utf-8")        
    @Consumes(MediaType.APPLICATION_JSON)
    public String persistPropertyNolRequest(  String inputParameters)
                              
                              
    {
                String result = "0";
                try 
                {
                    logger.logInfo("persistPropertyNolRequest|inputParameters:%s",inputParameters);
                    result =callUrlJSON(
                                        "persistPropertyNolRequest" ,
                                        inputParameters
                                        );
                    
             
                } catch (Exception e) {
                    logger.LogException("persistPropertyNolRequest:", e);
    //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
                     result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
                      ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
                }
                return result;

    }    
    @GET
    @Path("/defaulPaymentsForPropertyNOLType")    
    @Produces("application/json;charset=utf-8")
    
    public String defaulPaymentsForPropertyNOLType(
                                                    @QueryParam("loginId")String loginId, 
                                                    @QueryParam("nolTypeId")Long nolTypeId, 
                                                    @QueryParam("contractId")Long contractId
                                                  )
    {
        String result = "0";
        try 
        {
            logger.logDebug("defaulPaymentsForPropertyNOLType|loginId:%s|nolTypeId:%s|contractId:%s",loginId,nolTypeId,contractId);    
            result =callUrl(
                                "defaulPaymentsForPropertyNOLType?"+
                                "loginId="+loginId+
                                "&nolTypeId="+nolTypeId+
                                "&contractId="+contractId
                                );
            
        
        } catch (Exception e) {
            logger.LogException("defaulPaymentsForPropertyNOLType:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
             result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
              ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
        }
        return result;
    }
    
    @GET
    @Path("/dataForRenewContract")    
    @Produces("application/json;charset=utf-8")
    
    public String dataForRenewContractrequest(
                                                    @QueryParam("loginId")String loginId, 
                                                    @QueryParam("contractId")Long contractId
                                    )
    {
        String result = "0";
        try 
        {
            logger.logDebug("dataForRenewContract|loginId:%s|contractId:%s",loginId,contractId);    
            result =callUrl(
                                "dataForRenewContract?"+
                                "loginId="+loginId+
                                "&contractId="+contractId
                                );
            
        
        } catch (Exception e) {
            logger.LogException("dataForRenewContract:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
             result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
              ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
        }
        return result;    
        
    }
    
    @GET
    @Path("/createRenewContractRequest")    
    @Produces("application/json;charset=utf-8")
    public String createRenewContractRequest(
                                                    @QueryParam("loginId")String loginId, 
                                                    @QueryParam("contractId")Long contractId,
                                                    @QueryParam("personId")Long personId,
                                                    @DefaultValue("1") @QueryParam("requestSource")String requestSource
                                             )
    {
        String result = "0";
        try 
        {
            logger.logDebug("createRenewContractRequest|loginId:%s|contractId:%s|requestSource:%s|personId:%s",loginId,contractId,requestSource,personId);    
            result =callUrl(
                                "createRenewContractRequest?"+
                                "loginId="+loginId+
                                "&contractId="+contractId+
                                "&requestSource="+requestSource+
                                "&personId="+personId
                           );
            
        
        } catch (Exception e) {
            logger.LogException("createRenewContractRequest:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
             result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
              ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
        }
        return result;    
        
    }
    
    @GET
    @Path("/getPropertyMaintenanceRequestDetails")    
    @Produces("application/json;charset=utf-8")    
    public String getPropertyMaintenanceRequestDetails(
                                                    @QueryParam("loginId")String loginId, 
                                                    @DefaultValue("-1")@QueryParam("contractId")Long contractId,
                                                    @DefaultValue("-1")@QueryParam("unitId")Long unitId,
                                                    @DefaultValue("-1")@QueryParam("propertyId")Long propertyId,
                                                    @DefaultValue("-1")@QueryParam("requestId")Long requestId
                                                    
                                             )
    {
        String result="0";
        try
        {
            logger.logDebug("getPropertyMaintenanceRequestDetails|loginId:%s|contractId:%s|unitId:%s|propertyId:%s|requestId:%s",
                            loginId,contractId, unitId ,propertyId,requestId);    
            result =callUrl(
                                "getPropertyMaintenanceRequestDetails?"+
                                "loginId="+loginId+
                                "&contractId="+contractId+
                                "&unitId="+unitId+
                                "&propertyId="+propertyId+
                                "&requestId="+requestId
                           );
            
        
        } catch (Exception e) {
            logger.LogException("getPropertyMaintenanceRequestDetails:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
             result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
              ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
        }
        return result;    
        
    }
    
    @POST
    @Path("/persistPropertyMaintenanceRequest")
    @Produces("application/json;charset=utf-8")             
    public String persistPropertyMaintenanceRequest(String inputParameters)
    {
        String result = "0";
        try
        {
            result =callUrlJSON("persistPropertyMaintenanceRequest",inputParameters.trim());
    } catch (Exception e) {
        logger.LogException("persistPropertyMaintenanceRequest:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/checkStatus")    
    @Produces("application/json;charset=utf-8")
    public String checkStatus( @DefaultValue ("-1")  @QueryParam("devicePlatform")String devicePlatform)
    {
        
            String result = "0";
            try
            {
                logger.logInfo("checkStatus|devicePlatform:%s", devicePlatform);
                result =callUrl("checkStatus?"+
                                "devicePlatform="+devicePlatform);
        } catch (Exception e) {
            logger.LogException("checkStatus:", e);
            //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
            result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
            ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
            }
        return result;

    }
    @GET
    @Path("/setStatus")    
    @Produces("application/json;charset=utf-8")
    public String setStatus( 
                                @DefaultValue ("-1")  @QueryParam("devicePlatform")String devicePlatform,
                                @DefaultValue ("0")   @QueryParam("status")String status
                             )
    {
        String result = "0";
        try
        {
            logger.logInfo("setStatus|devicePlatform:%s|status:%s", devicePlatform,status);
            result =callUrl("setStatus?"+
                            "devicePlatform="+devicePlatform+
                            "&status="+status
                            );
        } catch (Exception e) {
        logger.LogException("setStatus:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }   
    
    @GET
    @Path("/getPropertyNolRequestDetails")    
    @Produces("application/json;charset=utf-8")
    public String getPropertyNolRequestDetails(
                                                    @QueryParam("loginId")String loginId, 
                                                    @QueryParam("requestId")Long requestId
                                                    
                                             )
    {
        String result = "0";
        try
        {
            logger.logInfo("getPropertyNolRequestDetails|loginId:%s|loginId:%s", loginId,requestId);
            result =callUrl("getPropertyNolRequestDetails?"+
                            "loginId="+loginId+
                            "&requestId="+requestId
                            );
        } catch (Exception e) {
        logger.LogException("getPropertyNolRequestDetails:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @GET
    @Path("/getRenewRequestDetails")    
    @Produces("application/json;charset=utf-8")
    public String getRenewRequestDetails(
                                                    @QueryParam("loginId")String loginId, 
                                                    @QueryParam("requestId")Long requestId
                                                    
                                             )
    {
        String result = "0";
        try
        {
            logger.logInfo("getRenewRequestDetails|loginId:%s|loginId:%s", loginId,requestId);
            result =callUrl("getRenewRequestDetails?"+
                            "loginId="+loginId+
                            "&requestId="+requestId
                            );
        } catch (Exception e) {
        logger.LogException("getRenewRequestDetails:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    @POST
    @Path("/persistCancelContractRequest")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String persistCancelContractRequest(String inputParameters)
    {
        String result = "0";
        try
        {
            result =callUrlJSON("persistCancelContractRequest",inputParameters.trim());
        } catch (Exception e) 
        {
            logger.LogException("persistCancelContractRequest:", e);
            //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
            result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
            ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    
    @GET
    @Path("/getCancelContractRequestDetails")    
    @Produces("application/json;charset=utf-8")
    public String getCancelContractRequestDetails(
                                                    @QueryParam("loginId")String loginId, 
                                                    @QueryParam("requestId")Long requestId,
                                                    @QueryParam("contractId")Long contractId
                                              
                                             )
    {
        String result = "0";
        try
        {
            logger.logInfo("getCancelContractRequestDetails|loginId:%s|loginId:%s|contractId:%s", loginId,requestId, contractId);
            result =callUrl("getCancelContractRequestDetails?"+
                            "loginId="+loginId+
                            "&requestId="+requestId+
                            "&contractId="+contractId
                            );
        } catch (Exception e) {
        logger.LogException("getCancelContractRequestDetails:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    
    @POST
    @Path("/persistTransferContractRequest")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    public String persistTransferContractRequest(String inputParameters)
    {
        String result = "0";
        try
        {
            result =callUrlJSON("persistTransferContractRequest",inputParameters.trim());
        } catch (Exception e) 
        {
            logger.LogException("persistCancelContractRequest:", e);
            //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
            result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
            ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    
    @GET
    @Path("/getTransferContractRequestDetails")    
    @Produces("application/json;charset=utf-8")
    public String getTransferContractRequestDetails(
                                                    @QueryParam("loginId")String loginId, 
                                                    @QueryParam("requestId")Long requestId,
                                                    @QueryParam("contractId")Long contractId,
                                                    @DefaultValue("ae") @QueryParam("locale")String locale
                                              
                                             )
    {
        String result = "0";
        try
        {
            logger.logInfo("getTransferContractRequestDetails|loginId:%s|loginId:%s|contractId:%s|locale:%s", loginId,requestId, contractId,locale);
            result =callUrl("getTransferContractRequestDetails?"+
                            "loginId="+loginId+
                            "&requestId="+requestId+
                            "&contractId="+contractId+
                            "&locale="+locale
                            );
        } catch (Exception e) {
        logger.LogException("getTransferContractRequestDetails:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
        result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
        ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";
        }
        return result;

    }
    
    @POST
    @Path("/validateAndGetPaymentsForTransferRequest")    
    @Produces("application/json;charset=utf-8")
    @Consumes(MediaType.APPLICATION_JSON)
    
    public String validateAndGetPaymentsForTransferRequest(
                                                                String inputParameters
                                                        ) 
    {
        String result = "0";
        try 
        {
            logger.logDebug("validateAndGetPaymentsForTransferRequest|inputParameters:%s|",inputParameters);    
            result =callUrlJSON(
                                "validateAndGetPaymentsForTransferRequest",inputParameters
                                );
            
        
        } catch (Exception e) {
            logger.LogException("validateAndGetPaymentsForTransferRequest:", e);
        //                    result ="{\"errors\": [\"stackTrace\":"+getStackTrace(e)+"]}";
             result="{\"errors\": [{\"errorCode\":\"000\",\"errorMsgEn\":\"An error has occured please try again later\""+
              ",\"errorMsgAr\":\"An error has occured please try again later\"}]}";                    
        }
        return result;
    }
    
    
}
