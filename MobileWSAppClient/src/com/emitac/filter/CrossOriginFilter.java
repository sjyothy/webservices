package com.emitac.filter;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerResponse;

import com.avanza.core.util.Logger;
import com.sun.jersey.spi.container.ContainerResponseFilter;



public class CrossOriginFilter implements ContainerResponseFilter{
        
    private static final Logger logger = Logger.getLogger(CrossOriginFilter.class);

    @Override
    public ContainerResponse filter(ContainerRequest containerRequest, ContainerResponse containerResponse) {
        
        logger.logDebug("filter");
            containerResponse.getHttpHeaders().add("Access-Control-Allow-Origin", "*");
        containerResponse.getHttpHeaders().add("Access-Control-Expose-Headers", "Authorization");
            return containerResponse;
        
    }
}
