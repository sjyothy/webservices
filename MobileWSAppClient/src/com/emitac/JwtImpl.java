package com.emitac;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;

import com.auth0.jwt.exceptions.JWTCreationException;


import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import com.avanza.core.util.Logger;

import java.io.UnsupportedEncodingException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class JwtImpl {
    private static String secretKey;
    private static Algorithm algorithm;
    private static final Logger logger = Logger.getLogger(Service.class);
    private static JWTVerifier verifier = null;
    static
    {
        try
        {
            secretKey = "TESTTOKEN";
            algorithm = Algorithm.HMAC256(secretKey);    
            verifier  = JWT.require(algorithm).withIssuer("pims_admin").build();
            logger.logDebug(algorithm.toString());
        }
        catch (UnsupportedEncodingException exception){
                   //UTF-8 encoding not supported
                   logger.LogException("UTF-8 encoding not supported:", exception);
                   exception.printStackTrace();
                   
        }
        catch(Exception e)
        {
            logger.LogException("Error occured:", e);
            e.printStackTrace();
        }
    }
    
    
    
    public String createToken( String userId)
    {
        String token =null;
        try {
                long issueAt = System.currentTimeMillis()/1000;
               
                token = JWT.create()
                                    .withIssuer("pims_admin")
                   
                                    .withIssuedAt(new Date())
                                        
                                    //.withExpiresAt( new Date()+2)
                                    .withSubject(userId)
                                    
                                    .sign(algorithm);
        } catch (JWTCreationException exception){
            logger.LogException("Invalid Signing configuration:", exception);
            exception.printStackTrace();
        }
        catch (Exception exception){
                   logger.LogException("Exception:", exception);
                   exception.printStackTrace();
        }
        return token;
    }
    
    public boolean verifyToken( String token)
    {
        boolean result =true;
        try {
                DecodedJWT decodedToken = verifier.verify(token);
                System.out.println("decodedToken.getIssuer():"+decodedToken.getIssuer());
                System.out.println("decodedToken.getKeyId():"+decodedToken.getKeyId());
                System.out.println("decodedToken.getIssuedAt():"+decodedToken.getIssuedAt());
                System.out.println("decodedToken.getExpiresAt():"+decodedToken.getExpiresAt());
                System.out.println("decodedToken.getSubject():"+decodedToken.getSubject());
                Map<String, Claim> claims = decodedToken.getClaims();
                for(String key : claims.keySet())
                {
                    System.out.println("Key:"+key+"|Value:"+claims.get(key).asString());
                }
        } 
        catch (JWTVerificationException  exception){
            result = false;
            logger.LogException("Invalid signature/claims:", exception);
            exception.printStackTrace();
        }
        catch (Exception exception){
                    result = false;
                   logger.LogException("Exception:", exception);
                   exception.printStackTrace();
                   
        }
        return result;
    }
    
    // This main function is added for testing purpose. It will be removed later
    public static void main(String[] arr) {
        
        
        try
        {
            JwtImpl jwt = new JwtImpl();
            List<String> tokens = new ArrayList<String>();
            tokens.add(jwt.createToken("pims_admin"));
            tokens.add(jwt.createToken("malijtabi"));
            tokens.add("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ");
            for (String token: tokens) {
                System.out.println("Token:"+token);
            }
            
            for (String token: tokens) {
                boolean result = jwt.verifyToken(token);
                System.out.println("token:"+token+"|Authenticated:"+result);
            }
            System.out.println("Main Completed");
        }
        catch(Exception e)
        {
                System.out.println("Exception occured :"+e.getStackTrace());
        }
    }

    
    
    public void createJWTToken()
    {
        
        
    }
}


