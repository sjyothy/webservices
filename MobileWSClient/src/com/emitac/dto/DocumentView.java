package com.emitac.dto;

import java.io.Serializable;

import java.util.Date;


import com.google.gson.annotations.Expose;


public class DocumentView implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long documentId;

    private String documentTitle;

    private String documentFileName;


    private String contentType;

    private String comments;

    private Long sizeInBytes;
    private String externalId;

    private Long docTypeId;

    private String docTypeEn;

    private String docTypeAr;

    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    public Long getDocumentId() {
        return documentId;
    }

    public void setDocumentTitle(String documentTitle) {
        this.documentTitle = documentTitle;
    }

    public String getDocumentTitle() {
        return documentTitle;
    }

    public void setDocumentFileName(String documentFileName) {
        this.documentFileName = documentFileName;
    }

    public String getDocumentFileName() {
        return documentFileName;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentType() {
        return contentType;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getComments() {
        return comments;
    }

    public void setSizeInBytes(Long sizeInBytes) {
        this.sizeInBytes = sizeInBytes;
    }

    public Long getSizeInBytes() {
        return sizeInBytes;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setDocTypeId(Long docTypeId) {
        this.docTypeId = docTypeId;
    }

    public Long getDocTypeId() {
        return docTypeId;
    }

    public void setDocTypeEn(String docTypeEn) {
        this.docTypeEn = docTypeEn;
    }

    public String getDocTypeEn() {
        return docTypeEn;
    }

    public void setDocTypeAr(String docTypeAr) {
        this.docTypeAr = docTypeAr;
    }

    public String getDocTypeAr() {
        return docTypeAr;
    }
}
