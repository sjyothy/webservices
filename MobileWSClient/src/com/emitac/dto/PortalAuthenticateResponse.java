package com.emitac.dto;

import com.google.gson.annotations.Expose;

public class PortalAuthenticateResponse {

    @Expose
    public PortalAuthenticateResponse.UserDetails userDetails =new UserDetails();

    public void setUserDetails(PortalAuthenticateResponse.UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public PortalAuthenticateResponse.UserDetails getUserDetails() {
        return userDetails;
    }

    /**
     * <p>Java class for user details component of response Json.
     **/
    public static class UserDetails{
        @Expose
        private String loginId ;
        @Expose
        private String userName;
        @Expose
        private String dateOfBirth;
        @Expose
        private String emiratesId;
        @Expose
        private String gender;
        @Expose
        private String email;
        @Expose
        private String cellNumber;
        @Expose
        private String nationality;
        @Expose
        private String nationalityEn;
        @Expose
        private String nationalityAr;

        public void setLoginId(String loginId) {
            this.loginId = loginId;
        }

        public String getLoginId() {
            return loginId;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserName() {
            return userName;
        }

        public void setDateOfBirth(String dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
        }

        public String getDateOfBirth() {
            return dateOfBirth;
        }

        public void setEmiratesId(String emiratesId) {
            this.emiratesId = emiratesId;
        }

        public String getEmiratesId() {
            return emiratesId;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getGender() {
            return gender;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getEmail() {
            return email;
        }

        public void setCellNumber(String cellNumber) {
            this.cellNumber = cellNumber;
        }

        public String getCellNumber() {
            return cellNumber;
        }

        public void setNationality(String nationality) {
            this.nationality = nationality;
        }

        public String getNationality() {
            return nationality;
        }

        public void setNationalityEn(String nationalityEn) {
            this.nationalityEn = nationalityEn;
        }

        public String getNationalityEn() {
            return nationalityEn;
        }

        public void setNationalityAr(String nationalityAr) {
            this.nationalityAr = nationalityAr;
        }

        public String getNationalityAr() {
            return nationalityAr;
        }
    }
}
