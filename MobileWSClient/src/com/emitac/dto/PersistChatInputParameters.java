package com.emitac.dto;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class PersistChatInputParameters implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    @Expose
    private String chatId;
    @Expose
    private String msg;
    @Expose
    private String createdBy;
    @Expose
    private String receipientId;
    @Expose
    private Long inheritanceFileId;

    public Long getInheritanceFileId() {
        return inheritanceFileId;
    }

    public void setInheritanceFileId(Long inheritanceFileId) {
        this.inheritanceFileId = inheritanceFileId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getReceipientId() {
        return receipientId;
    }

    public void setReceipientId(String receipientId) {
        this.receipientId = receipientId;
    }


    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getChatId() {
        return chatId;
    }
}
