package com.emitac.filter;

import com.avanza.core.util.Logger;

import com.emitac.annotations.VerifyCallerKey;

import com.emitac.utils.Constants;

import java.io.IOException;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Priority;

import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;



@VerifyCallerKey
@Priority(Priorities.AUTHENTICATION)
@Provider
public class VerifyCallerKeyFilter implements ContainerRequestFilter {
    private static final Logger logger = Logger.getLogger(VerifyCallerKeyFilter.class);
    public VerifyCallerKeyFilter() {
        super();
    }


    @Override
    public void filter(ContainerRequestContext request) throws IOException {
        // TODO Implement this method
        
        try{
            verifyCallerKey(request);   
        }
        catch(Exception e)
        {
          logger.LogException("Error Occured:",e)    ;
          
        }
    }

    public static Map<String,String> getAuthorizationHeaderData(ContainerRequestContext request)    throws Exception{

        String authorizationHeader = request.getHeaderString(Constants.Headers.AUTHORIZATION_HEADER );
        if(authorizationHeader == null || authorizationHeader.trim().length() <= 0)
        return null;
        
        Map<String,String> authorizationMap = new HashMap<String,String>();
        //Bearer apikey= , token= 
        String headerValues[] = authorizationHeader.split(",");
        authorizationMap.put(
                                Constants.Headers.AUTHORIZATION_API_KEY, 
                                headerValues[0].split("=")[1].trim()
                            );
        //For Public Requests, JWT Token will not be included in Authorization Header
        if(headerValues.length>1){
            authorizationMap.put(
                                    Constants.Headers.AUTHORIZATION_TOKEN_KEY, 
                                    headerValues[1].split("=")[1].trim()
                                );
        }
        logger.logInfo(
                         "ApiKey:%s|JWTToken:%s",            
                         authorizationMap.get(Constants.Headers.AUTHORIZATION_API_KEY), 
                         authorizationMap.get(Constants.Headers.AUTHORIZATION_TOKEN_KEY)
                       
                       );
        return authorizationMap;
        
        
    }
    public static      void verifyCallerKey (ContainerRequestContext request)throws Exception{
            logger.logInfo("verifyCallerKey");
            Map<String,String> mapAuthorizationHeaderData=  getAuthorizationHeaderData(request);
            if(mapAuthorizationHeaderData==null)
            {
                abortRequest(request, Response.Status.UNAUTHORIZED,Constants.ErrorMessages.invalidAPIKeyJson);
            }
            else
            {
                String caller  = mapAuthorizationHeaderData.get(Constants.Headers.AUTHORIZATION_API_KEY);
                
                boolean isCallerHeaderEmpty =          caller == null ||  caller.trim().length() <= 0 ;
             
                boolean isCallerAuthenticated =        !isCallerHeaderEmpty  && 
                                                        (
                                                            caller.trim().equals(Constants.CallerKeys.PORTAL_CALLER_KEY)   ||
                                                            caller.trim().equals(Constants.CallerKeys.IOS_CALLER_KEY)      ||
                                                            caller.trim().equals(Constants.CallerKeys.ANDROID_CALLER_KEY)  ||
                                                            caller.trim().equals(Constants.CallerKeys.ESERVICES_CALLER_KEY)
                                                        );
                 if(
                     !isCallerAuthenticated
                   )
                 {
                     abortRequest(request, Response.Status.UNAUTHORIZED,Constants.ErrorMessages.invalidAPIKeyJson );
                 
                 }
            }
    }
    
    public static void abortRequest(ContainerRequestContext request,Response.Status responseStatus,String errorJson) throws Exception
    {
       
       request.abortWith(
                              Response.status(responseStatus).entity(errorJson).build()
                        );
    }
}
