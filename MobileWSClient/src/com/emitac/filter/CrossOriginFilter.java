package com.emitac.filter;

import com.avanza.core.util.Logger;

import java.io.IOException;

import javax.annotation.Priority;

import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;

import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.server.ContainerRequest;
import org.glassfish.jersey.server.ContainerResponse;

@Priority(Priorities.HEADER_DECORATOR)
@Provider
public class CrossOriginFilter implements ContainerResponseFilter{
        
    private static final Logger logger = Logger.getLogger(CrossOriginFilter.class);

    

    @Override
    public void filter(ContainerRequestContext request,
                       ContainerResponseContext response) throws IOException {

        logger.logDebug("CrossOriginFilter");
        response.getHeaders().add("Access-Control-Allow-Origin", "*");
        response.getHeaders().add("Access-Control-Allow-Methods", "POST,GET,HEAD,OPTIONS");
        response.getHeaders().add("Access-Control-Allow-Headers", "Content-Type, Authorization,Origin,Accept");
        response.getHeaders().add("Access-Control-Expose-Headers", "token");

    }
}
