package com.emitac.filter;

import com.avanza.core.util.Logger;

import com.emitac.annotations.CreateJWTToken;
import com.emitac.utils.Constants;
import com.emitac.utils.JwtImpl;

import java.io.IOException;

import java.util.Map;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

@CreateJWTToken
@Provider
public class CreateJWTTokenFilter  implements ContainerResponseFilter{
        
    private static final Logger logger = Logger.getLogger(CreateJWTTokenFilter.class);

    
    

    @Override
    public void filter(ContainerRequestContext request,ContainerResponseContext response) throws IOException {
        try{
                logger.logDebug("CreateJWTTokenFilter|Start");
                Map<String,String> mapAuthorizationHeaderData=  VerifyCallerKeyFilter.getAuthorizationHeaderData(request);
                if(mapAuthorizationHeaderData!= null)
                {
                    String apikey  = mapAuthorizationHeaderData.get(Constants.Headers.AUTHORIZATION_API_KEY);
                    JwtImpl jwt  = new JwtImpl();
                    logger.logDebug("CreateJWTTokenFilter|apiKey:%s",apikey);
                    if( response.hasEntity() )
                    {
                        String responseBody = (String) response.getEntity();    
                        if(responseBody.trim().replace(" ", "").indexOf("\"errors\"")< 0 )
                        {
                            
                            String token = jwt.createToken(apikey,responseBody);
                            response.getHeaders().add("token", token);
                        }
                    }
                }
            logger.logDebug("CreateJWTTokenFilter|Finish");
        }
        catch(Exception e)
        {
          logger.LogException("Error Occured:",e)    ;
        }
    }
}

