package com.emitac.filter;

import com.avanza.core.util.Logger;

import com.emitac.annotations.VerifyJWTToken;

import com.emitac.utils.Constants;
import com.emitac.utils.JwtImpl;

import java.io.IOException;

import java.util.Map;

import javax.annotation.Priority;

import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.Providers;

import oracle.oc4j.query.Callers;

import org.glassfish.jersey.server.ContainerRequest;

@Priority(Priorities.AUTHORIZATION)
@VerifyJWTToken
@Provider
public class VerifyJWTTokenFilter implements ContainerRequestFilter{
    private static final Logger logger = Logger.getLogger(VerifyJWTTokenFilter.class);
    
    
    public VerifyJWTTokenFilter() {
        super();
    }

    

    @Override
    public void filter(ContainerRequestContext request) throws IOException {
     
     try{
             logger.logDebug("VerifyJWTTokenFilter|Start");
             Map<String,String> mapAuthorizationHeaderData=  VerifyCallerKeyFilter.getAuthorizationHeaderData(request);
             if(mapAuthorizationHeaderData==null)
             {
                 logger.logDebug("mapAuthorizationHeaderData is null" );
                VerifyCallerKeyFilter.abortRequest(request, Response.Status.UNAUTHORIZED,Constants.ErrorMessages.invalidAPIKeyJson);
             }
            else
             {
                 String apikey  = mapAuthorizationHeaderData.get(Constants.Headers.AUTHORIZATION_API_KEY);
                 String jwtToken  = mapAuthorizationHeaderData.get(Constants.Headers.AUTHORIZATION_TOKEN_KEY);
                 logger.logDebug("apikey:%s|jwtToken:%s",apikey,jwtToken  );
                 JwtImpl jwt  = new JwtImpl();
                 if (
                        (jwtToken == null || jwtToken.trim().length() <= 0)   ||
                        !jwt.verifyToken(apikey,jwtToken)
                     )
                { 
                        logger.logDebug("jwtToken is null OR jwtToken is not verfied" );
                       VerifyCallerKeyFilter.abortRequest(request,Response.Status.UNAUTHORIZED,Constants.ErrorMessages.userNotAuthenticatedJson ); 
                }
             }
         logger.logDebug("VerifyJWTTokenFilter|Finish");
             
     }
     catch(Exception e)
     {
       logger.LogException("Error Occured:",e)    ;
     }
    }
     

}
