package com.emitac.application;

import com.emitac.filter.CreateJWTTokenFilter;
import com.emitac.filter.CrossOriginFilter;


import com.emitac.filter.VerifyCallerKeyFilter;

import com.emitac.filter.VerifyJWTTokenFilter;

import java.util.Set;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
@ApplicationPath("/")
public class Application extends ResourceConfig {
    

    public Application() {
        super();
        packages("com.emitac");
        register(CrossOriginFilter.class);
        register(VerifyCallerKeyFilter.class);
        register(VerifyJWTTokenFilter.class);
        register(CreateJWTTokenFilter.class);
    }
}
