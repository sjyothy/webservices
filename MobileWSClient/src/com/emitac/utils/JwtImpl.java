package com.emitac.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;

import com.auth0.jwt.exceptions.JWTCreationException;


import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import com.avanza.core.util.Logger;

import java.io.UnsupportedEncodingException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class JwtImpl {
    private static String secretKey;
    private static Algorithm algorithm;
    private static final Logger logger = Logger.getLogger(JwtImpl.class);
    private static JWTVerifier verifier = null;
    static
    {
        try
        {
            secretKey = "TOKENJWT";
            algorithm = Algorithm.HMAC256(secretKey);    
            
            logger.logDebug(algorithm.toString());
        }
        catch (UnsupportedEncodingException exception){
                   //UTF-8 encoding not supported
                   logger.LogException("UTF-8 encoding not supported:", exception);
                   exception.printStackTrace();
                   
        }
        catch(Exception e)
        {
            logger.LogException("Error occured:", e);
            
        }
    }
    
    
    
    public String createToken( 
                               String apiKey,
                               String userDetailsJson
                             )
    {
        String token =null;
        try {
                long issueAt = System.currentTimeMillis()/1000;
                Calendar dateNow = Calendar.getInstance();
                Calendar expiredOn = Calendar.getInstance();
                expiredOn.add(Calendar.MINUTE, 20);
//              expiredOn.add(Calendar.HOUR, 1);
              token = JWT.create()
                                    .withIssuer(Constants.JWTTokenIssuer)
                                    .withIssuedAt(dateNow.getTime())
                                    .withExpiresAt( expiredOn.getTime() )
                                    .withSubject(userDetailsJson)
                                    .sign(algorithm);
        } catch (JWTCreationException exception){
            logger.LogException("Exception:", exception);
            exception.printStackTrace();
        }
        catch (Exception exception){
                   logger.LogException("Exception:", exception);
                   exception.printStackTrace();
        }
        return token;
    }
    
    public boolean verifyToken( 
                                String apiKey,
                                String token
                              )
    {
        boolean result =true;
        try {
                verifier  = JWT.require(algorithm).withIssuer(Constants.JWTTokenIssuer).build();
            
                DecodedJWT decodedToken = verifier.verify(token);
                logger.logDebug("decodedToken.getIssuer():%s|decodedToken.getKeyId():%s|decodedToken.getIssuedAt():%s" +
                                "decodedToken.getExpiresAt():%s|decodedToken.getSubject():%s",
                                decodedToken.getIssuer(),decodedToken.getKeyId(),decodedToken.getIssuedAt(),decodedToken.getExpiresAt(),
                                decodedToken.getSubject()            
                                );
//                Map<String, Claim> claims = decodedToken.getClaims();
//                for(String key : claims.keySet())
//                {
//                    logger.logDebug("Key:%s|Value:%s",key,claims.get(key).asString());
////                    System.out.println("Key:"+key+"|Value:"+claims.get(key).asString());
//                }
            if(decodedToken.getExpiresAt().compareTo(new Date())<0){
                result=false;
            }
        } 
        catch (JWTVerificationException  exception){
            result = false;
            logger.LogException("Invalid signature/claims:", exception);
            
        }
        catch (Exception exception){
                    result = false;
                   logger.LogException("Exception:", exception);

                   
        }
        return result;
    }
    
    public String getUserDetailsFromToken( 
                                            String apiKey,
                                            String token
                                          )throws Exception
    {
        String result = null;
        try {
                verifier  = JWT.require(algorithm).withIssuer(Constants.JWTTokenIssuer).build();
                DecodedJWT decodedToken = verifier.verify(token);
                result = decodedToken.getSubject();
                logger.logDebug("result :%s",result );
                
        } 
        catch (JWTVerificationException  exception){
            result = null;
            logger.LogException("Invalid signature/claims:", exception);
            throw exception;
        }
        catch (Exception exception){
                    result = null;
                   logger.LogException("getUserDetailsFromToken|Exception:", exception);
                   throw exception;

                   
        }
        return result;
    }

    
    // This main function is added for testing purpose. It will be removed later
    public static void main(String[] arr) {
        
        
        try
        {
            JwtImpl jwt = new JwtImpl();
            List<String> tokens = new ArrayList<String>();
            tokens.add(jwt.createToken(Constants.CallerKeys.ESERVICES_CALLER_KEY,"{\n" + 
            "    \"message\": {\n" + 
            "        \"msgCode\": \"0\",\n" + 
            "        \"msgEn\": \"Authenticated Successfully\",\n" + 
            "        \"msgAr\": \"Authenticated Successfully\"\n" + 
            "    },\n" + 
            "    \"userDetails\": {\n" + 
            "        \"loginId\": \"minor\",\n" + 
            "        \"userName\": \"Abdullah Ahmed\",\n" + 
            "        \"dateOfBirth\": \"23/09/1984\",\n" + 
            "        \"emiratesId\": \"784198406587505\",\n" + 
            "        \"gender\": \"m\",\n" + 
            "        \"email\": \"farooq.danish2@gmail.com\",\n" + 
            "        \"cellNumber\": \"0551364549\",\n" + 
            "        \"nationality\": \"222\",\n" + 
            "        \"nationalityEn\": \"Pakistan\",\n" + 
            "        \"nationalityAr\": \"?????????\"\n" + 
            "    },\n" + 
            "    \"roles\": [\n" + 
            "        {\n" + 
            "            \"roleId\": \"MINOR\"\n" + 
            "        }\n" + 
            "    ]\n" + 
            "}"));
            for (String token: tokens) {
                System.out.println("Token:"+token);
            }
            for (String token: tokens) {
                boolean result = jwt.verifyToken(
                                                Constants.CallerKeys.ESERVICES_CALLER_KEY,
                                                token
                                                );
                System.out.println("token:"+token+"|Authenticated:"+result);
            }
            System.out.println("Main Completed");
        }
        catch(Exception e)
        {
                System.out.println("Exception occured :"+e.getStackTrace());
        }
    }

    
    

}


