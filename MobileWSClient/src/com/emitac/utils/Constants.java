package com.emitac.utils;

public class Constants {
    private Constants(){}
    
    public static String JWTTokenIssuer = "AMAFWebServices";
    public static class Headers
    {
            
        private Headers(){};
        public static final String AUTHORIZATION_HEADER = "Authorization";
        public static final String AUTHORIZATION_API_KEY = "apikey";
        public static final String AUTHORIZATION_TOKEN_KEY = "token";
        public static final String AUTHORIZATION_SCHEME  = "Scheme";
        public static final String AUTHORIZATION_SCHEME_BEARER = "Bearer ";
    }
    public static class CallerKeys
    {
            
        private CallerKeys(){};
        public static final String IOS_CALLER_KEY = "AIzaSyBBKwVFz8YXy4ivoT8lIg2evPHeWpCUcME";
        public static final String ANDROID_CALLER_KEY = "KIzsdNDYwVTe9EWo1qxdQ1lIg1vrYHUWufzhTQ";
        public static final String ESERVICES_CALLER_KEY = "MQzddTDHwVTe0POd1efhP1lIg3weYHUWufzhPO";
        public static final String PORTAL_CALLER_KEY = "MQzddTDHwVTe0POd1efhP1lIg3weYHUWufzhPT";
    }
    public static class ErrorMessages
    {
        private ErrorMessages(){};
        public static final String invalidAPIKeyJson=  "{\"errors\": [{\"errorCode\":\"10001\"," +
                                "\"errorMsgEn\":\"Invalid caller\" ,"+
                                "\"errorMsgAr\":\"Invalid caller\"" +
                                "}]}";
        public static final String userNotAuthenticatedJson  =  "{\"errors\": [{\"errorCode\":\"401\"," +
                                "\"errorMsgEn\":\"User not authenticated\" ,"+
                                "\"errorMsgAr\":\"User not authenticated\"" +
                                "}]}";    
        
    }
}
