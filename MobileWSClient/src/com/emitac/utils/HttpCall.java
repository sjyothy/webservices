package com.emitac.utils;

import com.avanza.core.util.Logger;

import java.net.URLDecoder;

import java.net.URLEncoder;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class HttpCall {

    static final public Logger logger = Logger.getLogger(HttpCall.class);
    
    //production url
//        public static final String url = "http://192.168.2.17:7012/AMAFMobileWebServiceOne/resources/";
        public static final String url = "http://192.168.2.17:7012/AMAFMobileWebServiceTwo/resources/";
    //test url

//         public static final String url = "http://192.168.2.17:7012/AMAFMobileWebServiceTest/resources/" ;
                                                
    //localhost
//            public static final String url=" http://127.0.0.1:7101/AMAFMobileWSApp-MobileWS-context-root/resources/";
    public static String Post(String urlMethod, String postInfo) throws RuntimeException, Exception
    {

        CloseableHttpClient httpclient = HttpClients.createDefault();
        String callUrl = url + urlMethod;
        logger.logInfo("Url To Call:" + callUrl);
        HttpPost request = new HttpPost(callUrl);
        request.setHeader("Accept-Charset", "UTF-8");
        request.addHeader("content-type", "application/json");
        request.addHeader("charset", "utf-8");
        
        StringEntity params = new StringEntity(
                                                    URLDecoder.decode(postInfo, "UTF-8")
                                                    , 
                                                "UTF-8");
        request.setEntity(params);

        CloseableHttpResponse response1 = httpclient.execute(request);
        try {

            HttpEntity entity = response1.getEntity();
            if (entity != null) {
                return EntityUtils.toString(entity, org.apache.http.protocol.HTTP.UTF_8).replace("&lt;",
                                                                                                 "<").replace("&gt;",
                                                                                                              ">");
            }


        } finally {
            response1.close();
        }
        return "";

    }

    public static String Get(String urlMethod) throws RuntimeException, Exception {

        CloseableHttpClient httpclient = HttpClients.createDefault();
        String callUrl = url +      URLDecoder.decode(urlMethod, "UTF-8");
        callUrl = callUrl.replace("{","%7B").replace("}","%7D").replace("\"","%22");
        logger.logInfo("Url To Call:" + callUrl);
        HttpGet request = new HttpGet(callUrl);
        request.setHeader("Accept-Charset", "iso-8859-1, unicode-1-1;q=0.8");
        //        request.addHeader("content-type", "application/x-www-form-urlencoded");
        CloseableHttpResponse response1 = httpclient.execute(request);
        try {

            HttpEntity entity = response1.getEntity();
            if (entity != null) {
                return EntityUtils.toString(entity, org.apache.http.protocol.HTTP.UTF_8).replace("&lt;",
                                                                                                 "<").replace("&gt;",
                                                                                                              ">");
            }


        } finally {
            response1.close();
        }
        return "";

    }
    // This main function is added for testing purpose. It will be removed later
    public static void main(String[] arr) {
        String response="";
        CloseableHttpResponse response1 = null;
        try {

                String urlMethod = "leaseContractList?filters={\"tenantId\":\"34219\",\"orderBy\":\"contractId\",\"status\":\"310010\"}";
                CloseableHttpClient httpclient = HttpClients.createDefault();
                String callUrl = url +  urlMethod;
            //URLDecoder.decode(urlMethod, "UTF-8");
                                 //URLEncoder.encode(urlMethod, "UTF-8");
                logger.logInfo("Url To Call:" + callUrl);
                HttpGet request = new HttpGet(callUrl);
                request.setHeader("Accept-Charset", "iso-8859-1, unicode-1-1;q=0.8");
                response1 = httpclient.execute(request);
                HttpEntity entity = response1.getEntity();
                if (entity != null) {
                     response=    EntityUtils.toString(entity, org.apache.http.protocol.HTTP.UTF_8).replace("&lt;",
                                                                                                         "<").replace("&gt;",
                                                                                                                      ">");
                }


            } 
            catch(Exception e)
            {
                e.printStackTrace();
            }
            finally{
                        System.out.println("response:"+response);
            }
            
            
    }
}
