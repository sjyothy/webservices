package com.emitac.services;

import com.avanza.core.util.Logger;

import com.emitac.services.FileDownloadServlet;
import com.emitac.services.Service;

import com.emitac.utils.HttpCall;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.DefaultFileItemFactory;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.io.IOUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

public class FileUploadServlet extends HttpServlet {
    private static final String CONTENT_TYPE = 
        "text/html; charset=windows-1252";
    private Logger logger = null;
    private boolean isMultipart;
    private String filePath;
    private static final int MEMORY_THRESHOLD = 1024 * 1024 * 3; // 3MB
    private static final int MAX_FILE_SIZE = 1024 * 1024 * 40; // 40MB
    private static final int MAX_REQUEST_SIZE = 1024 * 1024 * 50; // 50MB
    private File file;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        logger = Logger.getLogger(FileUploadServlet.class);
    }

    public void doPost(HttpServletRequest request, 
                       HttpServletResponse response) throws ServletException, 
                                                            IOException {

        try {
            logger.logInfo("Inside Servlet TWO:");

            logger.logInfo("doPost|associatedObjectId:%s|procedureTypeId:%s|loggedInUser:%s|comments:%s|documentTitle:%s", 
                           request.getParameter("associatedObjectId"), 
                           request.getParameter("procedureTypeId"), 
                           request.getParameter("loggedInUser"), 
                           request.getParameter("comments"), 
                           request.getParameter("documentTitle"));
            String associatedObjectId = 
                request.getParameter("associatedObjectId");
            String externalId = request.getParameter("procedureTypeId");
            String loggedInUser = request.getParameter("loggedInUser");
            String comments = request.getParameter("comments");
            String documentTitle = request.getParameter("documentTitle");
            response.setContentType("application/json;charset=utf-8");
            logger.logInfo("Checking if request Is Multipart");
            isMultipart = ServletFileUpload.isMultipartContent(request);
            logger.logInfo("Is Multipart:%s", isMultipart);
            response.setContentType("text/html");
            DiskFileItemFactory factory = new DiskFileItemFactory();
            // sets memory threshold - beyond which files are stored in disk
            factory.setSizeThreshold(MEMORY_THRESHOLD);
            // sets temporary location to store files
            factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
            ServletFileUpload fu = new ServletFileUpload(factory);
            List fileItems = fu.parseRequest(request);
            if (fileItems != null) {
                logger.logInfo("Size of file Items:%s", fileItems.size());
            } else {
                logger.logInfo("file Items is null");
            }
            // Process the uploaded file items
            Iterator i = fileItems.iterator();
            String jsonResponse = "{\"documents\":[";
            boolean hasItems = false;
            while (i.hasNext()) {
                FileItem fi = (FileItem)i.next();
                if (!fi.isFormField()) {
                    logger.logInfo("inside file iterator::");
                    // Get the uploaded file parameters
                    String fieldName = fi.getFieldName();
                    logger.logInfo("fieldName:%s", fieldName);
                    String fileName = fi.getName();
                    logger.logInfo("fileName:%s", fileName);
                    String contentType = fi.getContentType();
                    boolean isInMemory = fi.isInMemory();
                    String sizeInBytes = String.valueOf(fi.getSize());

                    InputStream is = fi.getInputStream();
                    byte[] bytes = IOUtils.toByteArray(is);
                    byte[] encodedFileByteArray = Base64.encodeBase64(bytes);
                    String base64EncodedString = 
                        new String(encodedFileByteArray);
                    logger.logInfo("before callWebService");
                    String responseWebservice = 
                        callWebService(base64EncodedString, associatedObjectId, 
                                       loggedInUser, fileName, contentType, 
                                       comments, sizeInBytes, externalId, 
                                       documentTitle);
                    logger.logInfo("Response from WebService:%s", 
                                   responseWebservice);
                    if (responseWebservice.contains("{\"errors\": [")) {
                        jsonResponse = responseWebservice;
                        break;
                    } else {
                        jsonResponse += responseWebservice + ",";
                        hasItems = true;
                    }
                }

            }
            if (hasItems) {
                jsonResponse = 
                        jsonResponse.substring(0, jsonResponse.length() - 1) + 
                        "]}";
            }
            PrintWriter out = response.getWriter();
            logger.logInfo("Final Response JSON:%s", jsonResponse);
            out.print(jsonResponse);
        } catch (Exception e) {
            logger.LogException("Exception occured", e);
            System.out.println(e);
        }
    }


    public String callWebService(String base64EncodedString, 
                                 String associatedObjectId, 
                                 String loggedInUser, String documentFileName, 
                                 String contentType, String comments, 
                                 String sizeInBytes, String externalId, 
                                 String documentTitle) throws Exception {
//        final String webMethod = "uploadDocument";
        List<NameValuePair> postParameters = new ArrayList<NameValuePair>();
        postParameters.add(new BasicNameValuePair("base64EncodedString", 
                                                  base64EncodedString));
        postParameters.add(new BasicNameValuePair("associatedObjectId", 
                                                  associatedObjectId));
        postParameters.add(new BasicNameValuePair("loggedInUser", 
                                                  loggedInUser));
        postParameters.add(new BasicNameValuePair("documentFileName", 
                                                  documentFileName));
        postParameters.add(new BasicNameValuePair("contentType", contentType));
        postParameters.add(new BasicNameValuePair("comments", comments));
        postParameters.add(new BasicNameValuePair("externalId", externalId));
        postParameters.add(new BasicNameValuePair("sizeInBytes", sizeInBytes));
        postParameters.add(new BasicNameValuePair("documentTitle", 
                                                  documentTitle));

        CloseableHttpClient httpclient = HttpClients.createDefault();
        String callUrl = HttpCall.url + "uploadDocument";
        logger.logInfo("Url To Call:" + callUrl);
        HttpPost request = new HttpPost(callUrl);
        request.setHeader("Accept-Charset", "iso-8859-1, unicode-1-1;q=0.8");
        //        request.addHeader("content-type", "application/json");
        request.setEntity(new UrlEncodedFormEntity(postParameters));
        CloseableHttpResponse response1 = httpclient.execute(request);
        try {

            HttpEntity entity = response1.getEntity();
            if (entity != null) {
                return EntityUtils.toString(entity, HTTP.UTF_8).replace("&lt;", 
                                                                        "<").replace("&gt;", 
                                                                                     ">");
            }


        } finally {
            response1.close();
        }
        return "";

    }
}
